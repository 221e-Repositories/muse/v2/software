﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Pit221e.Core;
using Pit221e.Utilities;
using Pit221e._03b1Muse;
using System.IO;
using System.Diagnostics;

using System.Runtime.InteropServices;

namespace Dataset_ToyApp
{
    public partial class Form1 : Form
    {
        List<KeyValuePair<string, string>> portInfo_;
        List<KeyValuePair<string, string>> macInfo_;

        List<_03b1MuseSerialConnection> connectionList_;

        public List<StreamWriter> fileList_;

        private static int acquisitionIdx;

        private int acquisitionMode = 0; // 0: Streaming, 1: Log

        private BackgroundWorker bgWorker;

        // [DllImport("kernel32.dll", SetLastError = true)]
        // [return: MarshalAs(UnmanagedType.Bool)]
        // static extern bool AllocConsole();

        private void initializeBackgroundWorker()
        {
            bgWorker = new BackgroundWorker();
            bgWorker.WorkerReportsProgress = true;

            bgWorker.DoWork += (sender, e) => ((MethodInvoker)e.Argument).Invoke();
            bgWorker.ProgressChanged += (sender, e) =>
            {
                // toolProgressBar.Style = ProgressBarStyle.Continuous;
                // toolProgressBar.Value = e.ProgressPercentage;
            };
            bgWorker.RunWorkerCompleted += (sender, e) =>
            {
                // if (toolProgressBar.Style == ProgressBarStyle.Marquee)
                //     toolProgressBar.Style = ProgressBarStyle.Blocks;
            };
        }

        public Form1()
        {
            InitializeComponent();
            initializeBackgroundWorker();

            portInfo_ = new List<KeyValuePair<string, string>>();
            macInfo_ = new List<KeyValuePair<string, string>>();

            connectionList_ = new List<_03b1MuseSerialConnection>();
            fileList_ = new List<StreamWriter>();

            acquisitionIdx = 0;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // AllocConsole();

            cmbBox_Frequency.SelectedIndex = 1;

            // Search available devices and load them within combobox
            Pit221e.Utilities.Tool.SearchDevices(portInfo_, macInfo_);
            UpdateAvailableDevicesList();
        }

        private void UpdateAvailableDevicesList()
        {
            // Load available devices within combobox
            foreach (KeyValuePair<string, string> var in portInfo_)
                cmbBox_AvailableDevices.Items.Add(var.Value);
            cmbBox_AvailableDevices.SelectedIndex = 0;
        }

        private void btn_Connect_Click(object sender, EventArgs e)
        {
            // this.Invoke(new MethodInvoker(delegate {
            this.BeginInvoke(new MethodInvoker(delegate {
                _03b1MuseSerialConnection tmpConnection = new _03b1MuseSerialConnection(portInfo_[cmbBox_AvailableDevices.SelectedIndex].Key, true);

                // Connect selected device
                if (tmpConnection.Status == true)
                {
                    selectedConnection = tmpConnection;

                    connectionList_.Add(tmpConnection);
                    lst_ConnectedDevices.Items.Add(portInfo_[cmbBox_AvailableDevices.SelectedIndex].Value);
                }
            }));
        }

        private void btn_DisconnectAll_Click(object sender, EventArgs e)
        {
            if (selectedConnection != null)
                selectedConnection.Disconnect();

            // Disconnect all devices without shutdown them
            // foreach (_03b1MuseSerialConnection var in connectionList_)
            //     var.Disconnect();
            selectedConnection = null;
            lst_ConnectedDevices.Clear();
        }

        private void btn_Save_Click(object sender, EventArgs e)
        {
            /*
            // Setup folder for saving data
            FolderBrowserDialog folderDlg = new FolderBrowserDialog();

            if (folderDlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string folderPath = folderDlg.SelectedPath;

                // Setup one file for each connected sensor
                // int idx = 0;
                // foreach (ListViewItem var in lst_ConnectedDevices.Items)
                // {
                    string n = string.Format("{0:dd-MM-yyyy hhmmss tt}", DateTime.Now);
                    string fileName = folderPath + "\\" + n + ".txt";
                    StreamWriter tmpStream = new StreamWriter(fileName);
                    tmpStream.WriteLine(fileName);
                    tmpStream.WriteLine("Timestamp\tGyr_X\tGyr_Y\tGyr_Z\tAcc_X\tAcc_Y\tAcc_Z\tMag_X\tMag_Y\tMag_Z\tqW\tqX\tqY\tqZ");
                    fileList_.Add(tmpStream);
                //     idx++;
                // }
            }
            */
        }

        private void btn_Start_Click(object sender, EventArgs e)
        {
            // Start transmission
            foreach (_03b1MuseSerialConnection var in connectionList_)
            {
                selectedConnection = var;
            // if (selectedConnection != null) //  connectionList_.Count > 0)
            // {
                bool trStatus = selectedConnection.StatusTX;
                if (selectedConnection.Status == true)
                    selectedConnection.StartRawAndQuaternionStreaming(selectedFrequency);
                trStatus = selectedConnection.StatusTX;
                acquisitionIdx++;
            // }

                if (acquisitionIdx > 0)
                {
                    acquisitionMode = 0;
                    btn_StartLog.Enabled = false;
                    btn_Refresh.Enabled = false;
                    btn_ReadFile.Enabled = false;
                    btn_EraseMemory.Enabled = false;
                }
            }
        }

        private void btn_StartLog_Click(object sender, EventArgs e)
        {
            foreach (_03b1MuseSerialConnection var in connectionList_)
            {
                selectedConnection = var;
                // if (selectedConnection != null)
                // {
                selectedConnection.SetConfigurationParams((int)_03b1MuseHW.Gyr_fs.FS_4000, (int)_03b1MuseHW.Acc_fs.FS_16, (int)_03b1MuseHW.Mag_fs.FS_4, (int)_03b1MuseHW.Acc_HDR_fs.FS_100, (int)_03b1MuseHW.Log_mode.LOG_SYNC, 100);
                selectedConnection.StoreConfigurationParams();

                bool trStatus = selectedConnection.StatusTX;
                if (selectedConnection.Status == true)
                    selectedConnection.StartLog();
                trStatus = selectedConnection.StatusTX;

                acquisitionIdx++;
            
                if (acquisitionIdx > 0)
                {
                    acquisitionMode = 1;
                    btn_StartStreaming.Enabled = false;
                    btn_Refresh.Enabled = false;
                    btn_ReadFile.Enabled = false;
                    btn_EraseMemory.Enabled = false;
                }
            }
        }

        private void btn_Stop_Click(object sender, EventArgs e)
        {
            // Stop transmission
            foreach (_03b1MuseSerialConnection var in connectionList_)
            {
                var.StopTransmission();
            }
        //        // Stop transmission and save data

        //        // Setup one file for each connected sensor
        //        // int idx = 0;
        //        // foreach (ListViewItem var in lst_ConnectedDevices.Items)
        //        // {
        //        if (acquisitionMode == 0)
        //    {
        //        // Streaming
        //        string n = string.Format("{0:dd-MM-yyyy hhmmss tt}", DateTime.Now);
        //        string fileName = n + ".txt";
        //        // StreamWriter tmpStream = new StreamWriter(fileName);
        //        // tmpStream.WriteLine(fileName);
        //        // tmpStream.WriteLine("Timestamp\tGyr_X\tGyr_Y\tGyr_Z\tAcc_X\tAcc_Y\tAcc_Z\tMag_X\tMag_Y\tMag_Z\tqW\tqX\tqY\tqZ");
        //        // fileList_.Add(tmpStream);
        //        // }

        //        if (selectedConnection != null)
        //            selectedConnection.StopTransmission(fileName);
        //        // idx++;
        //        // }

        //        // Save data
        //        // if (acquisitionMode == 0)
        //        // {
        //        // idx = 0;
        //        // foreach (_03b1MuseSerialConnection var in connectionList_)
        //        // {
        //        /*
        //            string line = "";
        //            connectionList_[0].MessageList.ForEach(delegate (_03b1MuseMessage data)
        //            {
        //                line = data.TimestampTX + "\t" +
        //                       data.Gyroscope[0] + "\t" +
        //                       data.Gyroscope[1] + "\t" +
        //                       data.Gyroscope[2] + "\t" +
        //                       data.Accelerometer[0] + "\t" +
        //                       data.Accelerometer[1] + "\t" +
        //                       data.Accelerometer[2] + "\t" +
        //                       data.Magnetometer[0] + "\t" +
        //                       data.Magnetometer[1] + "\t" +
        //                       data.Magnetometer[2] + "\t" +
        //                       data.Quaternion[0] + "\t" +
        //                       data.Quaternion[1] + "\t" +
        //                       data.Quaternion[2] + "\t" +
        //                       data.Quaternion[3] + "\t";
        //                line = line.Replace(',', '.');
        //                fileList_[0].WriteLine(line);
        //            });
        //        */
        //        // idx++;
        //        }
        //        else
        //        {
        //            if (selectedConnection != null)
        //                selectedConnection.StopTransmission();
        //        }
        //// <}

            acquisitionIdx = 0;
            acquisitionMode = 0;
            btn_StartStreaming.Enabled = true;
            btn_StartLog.Enabled = true;
            btn_Refresh.Enabled = true;
            btn_ReadFile.Enabled = true;
            btn_EraseMemory.Enabled = true;
        }

        private void btn_Refresh_Click(object sender, EventArgs e)
        {
            // selectedConnection = connectionList_[0];

            try
            {
                if (selectedConnection != null)
                {
                    this.BeginInvoke(new MethodInvoker(delegate
                    {

                        cmb_AvailableFiles.Items.Clear();

                        List<DateTime> availableFiles = selectedConnection.GetFiles();
                        if (availableFiles.Count > 0)
                        {
                            foreach (var fl in availableFiles)
                                cmb_AvailableFiles.Items.Add(fl);

                            cmb_AvailableFiles.SelectedIndex = 0;
                        }
                    }));
                }
            }
            catch 
            { }
        }

        private void btn_ReadFile_Click(object sender, EventArgs e)
        {
            // selectedConnection = connectionList_[0];

            this.BeginInvoke(new MethodInvoker(delegate {
                if (selectedConnection != null && index >= 0)
                    selectedConnection.ReadFile(selectedFileIndex + 1, (cmb_AvailableFiles.SelectedItem.ToString().Replace("/", "-") + ".txt").Replace(":", ""));
            }));
        }

        private void btn_EraseMemory_Click(object sender, EventArgs e)
        {
            // selectedConnection = connectionList_[0];

            this.BeginInvoke(new MethodInvoker(delegate {
                if (selectedConnection != null && index >= 0)
                    selectedConnection.EraseMemory();
            }));
        }

        private int index = -1;
        private _03b1MuseSerialConnection selectedConnection;
        private void lst_ConnectedDevices_SelectedIndexChanged(object sender, EventArgs e)
        {
            // selectedConnection = connectionList_[0];

            // if (selectedConnection != null)
            //     selectedConnection = connectionList_[lst_ConnectedDevices.SelectedIndices[0]];
            
            index = -1;
            foreach (ListViewItem var in lst_ConnectedDevices.Items)
            {
                if (var.Selected == true)
                    index = var.Index;
            }

            if (selectedConnection != null  && index >= 0)
                selectedConnection = connectionList_[index];
        }

        private int selectedFileIndex;
        private void cmb_AvailableFiles_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedFileIndex = cmb_AvailableFiles.SelectedIndex;
        }

        private UInt16 selectedFrequency;
        private void cmbBox_Frequency_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (cmbBox_Frequency.SelectedIndex)
            {
                case 0:
                    selectedFrequency = 25;
                    break;
                case 1:
                    selectedFrequency = 50;
                    break;
                case 2:
                    selectedFrequency = 100;
                    break;
                case 3:
                    selectedFrequency = 150;
                    break;
                default:
                    selectedFrequency = 50;
                    break;
            }
        }
    }
}
