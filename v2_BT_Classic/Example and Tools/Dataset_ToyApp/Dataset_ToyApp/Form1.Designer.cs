﻿namespace Dataset_ToyApp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbBox_AvailableDevices = new System.Windows.Forms.ComboBox();
            this.lst_ConnectedDevices = new System.Windows.Forms.ListView();
            this.btn_Connect = new System.Windows.Forms.Button();
            this.btn_Disconnect = new System.Windows.Forms.Button();
            this.btn_StartStreaming = new System.Windows.Forms.Button();
            this.cmbBox_Frequency = new System.Windows.Forms.ComboBox();
            this.btn_Stop = new System.Windows.Forms.Button();
            this.btn_StartLog = new System.Windows.Forms.Button();
            this.cmb_AvailableFiles = new System.Windows.Forms.ComboBox();
            this.btn_ReadFile = new System.Windows.Forms.Button();
            this.btn_EraseMemory = new System.Windows.Forms.Button();
            this.btn_Refresh = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // cmbBox_AvailableDevices
            // 
            this.cmbBox_AvailableDevices.FormattingEnabled = true;
            this.cmbBox_AvailableDevices.IntegralHeight = false;
            this.cmbBox_AvailableDevices.ItemHeight = 16;
            this.cmbBox_AvailableDevices.Location = new System.Drawing.Point(30, 38);
            this.cmbBox_AvailableDevices.Name = "cmbBox_AvailableDevices";
            this.cmbBox_AvailableDevices.Size = new System.Drawing.Size(233, 24);
            this.cmbBox_AvailableDevices.TabIndex = 0;
            // 
            // lst_ConnectedDevices
            // 
            this.lst_ConnectedDevices.HideSelection = false;
            this.lst_ConnectedDevices.Location = new System.Drawing.Point(30, 72);
            this.lst_ConnectedDevices.MultiSelect = false;
            this.lst_ConnectedDevices.Name = "lst_ConnectedDevices";
            this.lst_ConnectedDevices.Size = new System.Drawing.Size(233, 66);
            this.lst_ConnectedDevices.TabIndex = 1;
            this.lst_ConnectedDevices.UseCompatibleStateImageBehavior = false;
            this.lst_ConnectedDevices.View = System.Windows.Forms.View.List;
            this.lst_ConnectedDevices.SelectedIndexChanged += new System.EventHandler(this.lst_ConnectedDevices_SelectedIndexChanged);
            // 
            // btn_Connect
            // 
            this.btn_Connect.Location = new System.Drawing.Point(269, 34);
            this.btn_Connect.Name = "btn_Connect";
            this.btn_Connect.Size = new System.Drawing.Size(151, 33);
            this.btn_Connect.TabIndex = 2;
            this.btn_Connect.Text = "Connect";
            this.btn_Connect.UseVisualStyleBackColor = true;
            this.btn_Connect.Click += new System.EventHandler(this.btn_Connect_Click);
            // 
            // btn_Disconnect
            // 
            this.btn_Disconnect.Location = new System.Drawing.Point(269, 70);
            this.btn_Disconnect.Name = "btn_Disconnect";
            this.btn_Disconnect.Size = new System.Drawing.Size(151, 32);
            this.btn_Disconnect.TabIndex = 4;
            this.btn_Disconnect.Text = "Disconnect";
            this.btn_Disconnect.UseVisualStyleBackColor = true;
            this.btn_Disconnect.Click += new System.EventHandler(this.btn_DisconnectAll_Click);
            // 
            // btn_StartStreaming
            // 
            this.btn_StartStreaming.Location = new System.Drawing.Point(587, 34);
            this.btn_StartStreaming.Name = "btn_StartStreaming";
            this.btn_StartStreaming.Size = new System.Drawing.Size(151, 32);
            this.btn_StartStreaming.TabIndex = 6;
            this.btn_StartStreaming.Text = "Start Streaming";
            this.btn_StartStreaming.UseVisualStyleBackColor = true;
            this.btn_StartStreaming.Click += new System.EventHandler(this.btn_Start_Click);
            // 
            // cmbBox_Frequency
            // 
            this.cmbBox_Frequency.FormattingEnabled = true;
            this.cmbBox_Frequency.Items.AddRange(new object[] {
            "25",
            "50",
            "100",
            "150"});
            this.cmbBox_Frequency.Location = new System.Drawing.Point(467, 39);
            this.cmbBox_Frequency.Name = "cmbBox_Frequency";
            this.cmbBox_Frequency.Size = new System.Drawing.Size(114, 24);
            this.cmbBox_Frequency.TabIndex = 8;
            this.cmbBox_Frequency.SelectedIndexChanged += new System.EventHandler(this.cmbBox_Frequency_SelectedIndexChanged);
            // 
            // btn_Stop
            // 
            this.btn_Stop.Location = new System.Drawing.Point(587, 106);
            this.btn_Stop.Name = "btn_Stop";
            this.btn_Stop.Size = new System.Drawing.Size(151, 32);
            this.btn_Stop.TabIndex = 9;
            this.btn_Stop.Text = "Stop";
            this.btn_Stop.UseVisualStyleBackColor = true;
            this.btn_Stop.Click += new System.EventHandler(this.btn_Stop_Click);
            // 
            // btn_StartLog
            // 
            this.btn_StartLog.Location = new System.Drawing.Point(587, 70);
            this.btn_StartLog.Name = "btn_StartLog";
            this.btn_StartLog.Size = new System.Drawing.Size(151, 32);
            this.btn_StartLog.TabIndex = 10;
            this.btn_StartLog.Text = "Start Log";
            this.btn_StartLog.UseVisualStyleBackColor = true;
            this.btn_StartLog.Click += new System.EventHandler(this.btn_StartLog_Click);
            // 
            // cmb_AvailableFiles
            // 
            this.cmb_AvailableFiles.FormattingEnabled = true;
            this.cmb_AvailableFiles.Location = new System.Drawing.Point(30, 183);
            this.cmb_AvailableFiles.Name = "cmb_AvailableFiles";
            this.cmb_AvailableFiles.Size = new System.Drawing.Size(233, 24);
            this.cmb_AvailableFiles.TabIndex = 11;
            this.cmb_AvailableFiles.SelectedIndexChanged += new System.EventHandler(this.cmb_AvailableFiles_SelectedIndexChanged);
            // 
            // btn_ReadFile
            // 
            this.btn_ReadFile.Location = new System.Drawing.Point(587, 178);
            this.btn_ReadFile.Name = "btn_ReadFile";
            this.btn_ReadFile.Size = new System.Drawing.Size(151, 33);
            this.btn_ReadFile.TabIndex = 12;
            this.btn_ReadFile.Text = "Read File";
            this.btn_ReadFile.UseVisualStyleBackColor = true;
            this.btn_ReadFile.Click += new System.EventHandler(this.btn_ReadFile_Click);
            // 
            // btn_EraseMemory
            // 
            this.btn_EraseMemory.Location = new System.Drawing.Point(587, 215);
            this.btn_EraseMemory.Name = "btn_EraseMemory";
            this.btn_EraseMemory.Size = new System.Drawing.Size(151, 33);
            this.btn_EraseMemory.TabIndex = 14;
            this.btn_EraseMemory.Text = "Erase Memory";
            this.btn_EraseMemory.UseVisualStyleBackColor = true;
            this.btn_EraseMemory.Click += new System.EventHandler(this.btn_EraseMemory_Click);
            // 
            // btn_Refresh
            // 
            this.btn_Refresh.Location = new System.Drawing.Point(269, 178);
            this.btn_Refresh.Name = "btn_Refresh";
            this.btn_Refresh.Size = new System.Drawing.Size(151, 33);
            this.btn_Refresh.TabIndex = 15;
            this.btn_Refresh.Text = "Refresh";
            this.btn_Refresh.UseVisualStyleBackColor = true;
            this.btn_Refresh.Click += new System.EventHandler(this.btn_Refresh_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 17);
            this.label1.TabIndex = 16;
            this.label1.Text = "Connection:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(464, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 17);
            this.label2.TabIndex = 17;
            this.label2.Text = "Acquisition:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(27, 155);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 17);
            this.label3.TabIndex = 18;
            this.label3.Text = "Memory:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(761, 266);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_Refresh);
            this.Controls.Add(this.btn_EraseMemory);
            this.Controls.Add(this.btn_ReadFile);
            this.Controls.Add(this.cmb_AvailableFiles);
            this.Controls.Add(this.btn_StartLog);
            this.Controls.Add(this.btn_Stop);
            this.Controls.Add(this.cmbBox_Frequency);
            this.Controls.Add(this.btn_StartStreaming);
            this.Controls.Add(this.btn_Disconnect);
            this.Controls.Add(this.btn_Connect);
            this.Controls.Add(this.lst_ConnectedDevices);
            this.Controls.Add(this.cmbBox_AvailableDevices);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbBox_AvailableDevices;
        private System.Windows.Forms.ListView lst_ConnectedDevices;
        private System.Windows.Forms.Button btn_Connect;
        private System.Windows.Forms.Button btn_Disconnect;
        private System.Windows.Forms.Button btn_StartStreaming;
        private System.Windows.Forms.ComboBox cmbBox_Frequency;
        private System.Windows.Forms.Button btn_Stop;
        private System.Windows.Forms.Button btn_StartLog;
        private System.Windows.Forms.ComboBox cmb_AvailableFiles;
        private System.Windows.Forms.Button btn_ReadFile;
        private System.Windows.Forms.Button btn_EraseMemory;
        private System.Windows.Forms.Button btn_Refresh;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}

