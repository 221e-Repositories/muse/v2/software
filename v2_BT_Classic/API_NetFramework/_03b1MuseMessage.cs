﻿//-----------------------------------------------------------------
//
// 221e srl CONFIDENTIAL
// 
// File Name:  _03b1Message.cs
// File Type:  Visual C# Source file
// Author:     Roberto Bortoletto
// Company:    221e srl
//             Copyright (c) 2017 All Rights Reserved
//-----------------------------------------------------------------

using System;

namespace Pit221e._03b1Muse
{
    /// <summary> _03b1MuseMessage class. </summary>
    /// All communication between Muse sensors and PC ultimately results in _03b1MuseMessage instances being sent by the sensor to the PC.
    /// 
    /// A _03b1MuseMessage is a general-purpose container for Muse data. It includes a number of information that allow for uniquely distinguishes a message from another such as
    /// identifier, dimension and timestamp. It manages the representation of the data received by <see cref ="Gyroscope">Gyroscope</see>, 
    /// <see cref ="Accelerometer">Accelerometer</see>, <see cref ="HDRAccelerometer">HDRAccelerometer</see>, <see cref ="Magnetometer">Magnetometer</see>, 
    /// <see cref ="Barometer">Barometer</see> and <see cref ="Thermometer">Thermometer</see> as well as the output of the embedded orientation estimation filter
    /// in <see cref ="Quaternion">Quaternion</see> form. Each message includes also the <see cref ="LogMode">LogMode</see> and <see cref ="LogFrequency">LogFrequency</see>
    /// with which it has been recorded in the case of log acquisition.
    /// 
    /// You would not usually interact with the _03b1MuseMessage class directly. Instead, _03b1MuseSerialConnection constructs, such as data contracts,
    /// message contracts, and operation contracts, are used to describe incoming and outcoming messages. However, in some advanced scenarios you can program
    /// using the _03b1MuseMessage class directly. For example, you might want to use the _03b1MuseMessage class:
    /// 
    /// - When you need an alternative way of using incoming (from sensor to PC) message contents instead of deserializing into the predefined objects;
    /// 
    /// - When you need to deal with messages in a general way regardless of message contents.
    public class _03b1MuseMessage
    {
        private int id_;
        private int dimension_;
        private double timestampTX_;
        private double timestampRX_;
        private UInt64 timestamp_;

        private float[] gyro_;
        private float[] acc_;
        private float[] accHDR_;
        private int[] magn_;

        private int[] therm_;
        private int[] baro_;

        private float[] quat_;

        private int logMode_;
        private int logFreq_;

        #region CONSTRUCTOR

        /// <summary> Constructor. Initializes a new instance of the class. </summary>
        /// \hideinitializer
        public _03b1MuseMessage()
        {
            id_ = 0;
            dimension_ = 0;
            timestampTX_ = 0;
            timestampRX_ = 0;

            gyro_ = new float[3];
            acc_ = new float[3];
            accHDR_ = new float[3];
            magn_ = new int[3];

            therm_ = new int[3];
            baro_ = new int[3];

            quat_ = new float[4];

            logMode_ = 0;
            logFreq_ = 0;
        }

        #endregion

        #region PROPERTIES

        /// <summary> The property ID gets or sets the message identifier. </summary>
        /// \hideinitializer
        public int ID
        {
            get { return id_; }
            set { id_ = value; }
        }
        /// <summary> The property Dimension [num of bytes] gets or sets the message dimension </summary>
        /// \hideinitializer
        public int Dimension
        {
            get { return dimension_; }
            set { dimension_ = value; }
        }
        /// <summary> The property TimestampTX gets or sets the message transmission timestamp. </summary>
        /// \hideinitializer
        public double TimestampTX
        {
            get { return timestampTX_; }
            set { timestampTX_ = value; }
        }

        /// <summary> The property TimestampRX gets or sets the message reception timestamp. </summary>
        /// \hideinitializer
        public double TimestampRX
        {
            get { return timestampRX_; }
            set { timestampRX_ = value; }
        }
    
        public UInt64 Timestamp
        {
            get { return timestamp_; }
            set { timestamp_ = value; }
        }

        /// <summary> The property Gyroscope gets or sets the x,y,z gyroscope [dps] data. </summary>
        /// \hideinitializer
        public float[] Gyroscope
        {
            get { return gyro_; }
            set
            {
                // gyro_ = value;
                gyro_[0] = value[0];
                gyro_[1] = value[1];
                gyro_[2] = value[2];
            }
        }
        /// <summary> The property Accelerometer gets or sets the x,y,z accelerometer [g] data. </summary>
        /// \hideinitializer
        public float[] Accelerometer
        {
            get { return acc_; }
            set
            {
                // acc_ = value;
                acc_[0] = value[0];
                acc_[1] = value[1];
                acc_[2] = value[2];
            }
        }
        /// <summary> The property HDRAccelerometer gets or sets the x,y,z High Dynamic Range (HDR) accelerometer [g] data. </summary>
        /// \hideinitializer
        public float[] HDRAccelerometer
        {
            get { return accHDR_; }
            set
            {
                // accHDR_ = value;
                accHDR_[0] = value[0];
                accHDR_[1] = value[1];
                accHDR_[2] = value[2];
            }
        }
        /// <summary> The property Magnetometer gets or sets the x,y,z magnetometer [Gauss] data. </summary>
        /// \hideinitializer
        public int[] Magnetometer
        {
            get { return magn_; }
            set
            {
                // magn_ = value;
                magn_[0] = value[0];
                magn_[1] = value[1];
                magn_[2] = value[2];
            }
        }
        /// <summary> The property Thermometer gets or sets the thermometer [°C] data. </summary>
        /// \hideinitializer
        public int[] Thermometer
        {
            get { return therm_; }
            set { therm_ = value; }
        }
        /// <summary> The property Barometer gets or sets the barometer [hPa] data. </summary>
        /// \hideinitializer
        public int[] Barometer
        {
            get { return baro_; }
            set { baro_ = value; }
        }
        /// <summary> The property Quaternion gets or sets the w,x,y,z quaternion components. </summary>
        /// \hideinitializer
        public float[] Quaternion
        {
            get { return quat_; }
            set
            {
                // quat_ = value;
                quat_[0] = value[0];
                quat_[1] = value[1];
                quat_[2] = value[2];
                quat_[3] = value[3];
            }
        }

        /// <summary> The property LogMode gets or sets the device log mode. </summary>
        /// \hideinitializer
        public int LogMode
        {
            get { return logMode_; }
            set { logMode_ = value; }
        }

        /// <summary> The property LogFrequency gets or sets the device log frequency. </summary>
        /// \hideinitializer
        public int LogFrequency
        {
            get { return logFreq_; }
            set { logFreq_ = value; }
        }

        #endregion
    }
}