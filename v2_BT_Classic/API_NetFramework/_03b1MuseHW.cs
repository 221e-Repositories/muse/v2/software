﻿//-----------------------------------------------------------------
//
// 221e srl CONFIDENTIAL
// 
// File Name:  _03b1hw.cs
// File Type:  Visual C# Source file
// Author:     Roberto Bortoletto
// Company:    221e srl
//             Copyright (c) 2017 All Rights Reserved
//-----------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;

namespace Pit221e._03b1Muse
{
    public class Dictionaries
    {
        public static Dictionary<int, int> GetGyrFS()
        {
            Dictionary<int, int> GyrFSValues = new Dictionary<int, int> {
                {0,500},
                {8,1000},
                {16,2000},
                {24,4000}
            };
            return GyrFSValues;
        }

        public static Dictionary<int, int> GetAxlFS()
        {
            Dictionary<int, int> AxlFSvalue = new Dictionary<int, int> {
                {0,2},
                {8,4},
                {16,6},
                {24,8},
                {32,16}
            };
            return AxlFSvalue;
        }

        public static Dictionary<int, int> GetAxlHDRFS()
        {
            Dictionary<int, int> AxlHDRFSvalue = new Dictionary<int, int> {
                {0,100},
                {16,200},
                {48,400}
            };
            return AxlHDRFSvalue;
        }

        public static Dictionary<int, int> GetMagFS()
        {
            Dictionary<int, int> MagFSvalue = new Dictionary<int, int> {
                {0,2},
                {32,4},
                {64,8},
                {96,12}
            };
            return MagFSvalue;
        }

        public static Dictionary<int, string> GetLogMode()
        {
            Dictionary<int, string> LogModeValue = new Dictionary<int, string> {
                {0,"NONE"},
                {1,"QUATERNION"},
                {2,"HDR"},
                {3,"RAW"},
                {4,"RAW_AND_QUATERNION"},
                {5,"HIGH_RESOLUTION"},
                {6,"SYNC"}
            };
            return LogModeValue;
        }
    }

    /// <summary> _03b1MuseHW class. Serial Communication Protocol over Bluetooth Classic. </summary>
    public class _03b1MuseHW
    {
        #region FREQUENCIES and CLOCK

        /// <summary> Maximum Streaming Frequency [Hz]. </summary>
        public const uint MAX_STREAM_FREQUENCY = 150;
        /// <summary> Maximum Logging Frequency [Hz]. </summary>
        public const uint MAX_LOG_FREQUENCY = 500;

        #endregion

        #region MESSAGE DIMENSIONS
 
        /// <summary> Quaternion Message Size [Byte]. </summary>
        public const byte QUATERNION_MESSAGE_SIZE = 18;
        /// <summary> Raw Data Message Size [Byte]. </summary>
        public const byte RAW_DATA_MESSAGE_SIZE = 54;
        /// <summary> HDR Data Message Size [Byte]. </summary>
        public const byte HDR_DATA_MESSAGE_SIZE = 36;

        #endregion

        #region OPERATING MODES AND PARAMETERS - ENUM TYPES

        /// <summary> Device Operating Modes. </summary>
        /// \hideinitializer
        public enum op_mode
        {
            STREAM = 0,
            LOG = 1,
            TYME_SYNC = 2,
            // FOTA = 3
        }

        /// <summary> Device Sleep Modes (default OFF). </summary>
        /// \hideinitializer
        public enum sleep_mode
        {
            DISABLED = 0,
            ENABLED = 1
        }

        /// <summary> Streaming modes. </summary>
        /// \hideinitializer
        public enum streaming_type
        {
            STREAM_HDR = 0,
            STREAM_QUATERNION = 1,
            STREAM_RAW_AND_QUATERNION = 2   // Default
        }

        /// <summary> Log modes. </summary>
        /// \hideinitializer
        public enum Log_mode
        {
            LOG_NONE = 0,
            LOG_QUATERNION = 1,
            LOG_HDR = 2,
            LOG_RAW = 3,
            LOG_RAW_AND_QUATERNION = 4,
            LOG_HIGH_RES = 5,
            LOG_SYNC = 6
        }

        /// <summary> Gyroscope Full Scale parameter. </summary>
        /// \hideinitializer
        internal static Dictionary<int, int> gyr_fs_value = new Dictionary<int, int> {
            {0,500},
            {8,1000},
            {16,2000},
            {24,4000}
        };

        public enum Gyr_fs
        {
            FS_500 = 0,
            FS_1000 = 8,
            FS_2000 = 16,
            FS_4000 = 24
        }

        /// <summary> Accelerometer Full Scale parameter. </summary>
        /// \hideinitializer
        internal static Dictionary<int, int> acc_fs_value = new Dictionary<int, int> {
            {0,2},
            {8,4},
            {16,6},
            {24,8},
            {32,16}
        };

        public enum Acc_fs
        {
            FS_2 = 0,
            FS_4 = 8,
            FS_6 = 16,
            FS_8 = 24,
            FS_16 = 32
        }

        /// <summary> Accelerometer (HDR) Full Scale parameter. </summary>
        /// \hideinitializer
        internal static Dictionary<int, int> acc_hdr_fs_value = new Dictionary<int, int> {
            {0,100},
            {16,200},
            {48,400}
        };

        public enum Acc_HDR_fs
        {
            FS_100 = 0,
            FS_200 = 16,
            FS_400 = 48
        }

        /// <summary> Magnetometer Full Scale parameter. </summary>
        /// \hideinitializer
        internal static Dictionary<int, int> mag_fs_value = new Dictionary<int, int> {
            {0,2},
            {32,4},
            {64,8},
            {96,12}
        };

        public enum Mag_fs
        {
            FS_2 = 0,
            FS_4 = 32,
            FS_8 = 64,
            FS_12 = 96
        }

        public static Dictionary<int, int> GetGyrFS()
        {
            Dictionary<int, int> GyrFSValues = new Dictionary<int, int> {
                {0,500},
                {8,1000},
                {16,2000},
                {24,4000}
            };
            return GyrFSValues;
        }

        public static Dictionary<int, int> GetAxlFS()
        {
            Dictionary<int, int> AxlFSvalue = new Dictionary<int, int> {
                {0,2},
                {8,4},
                {16,6},
                {24,8},
                {32,16}
            };
            return AxlFSvalue;
        }

        public static Dictionary<int, int> GetAxlHDRFS()
        {
            Dictionary<int, int> AxlHDRFSvalue = new Dictionary<int, int> {
                {0,100},
                {16,200},
                {48,400}
            };
            return AxlHDRFSvalue;
        }

        public static Dictionary<int, int> GetMagFS()
        {
            Dictionary<int, int> MagFSvalue = new Dictionary<int, int> {
                {0,2},
                {32,4},
                {64,8},
                {96,12}
            };
            return MagFSvalue;
        }

        public static Dictionary<int, string> GetLogMode()
        {
            Dictionary<int, string> LogModeValue = new Dictionary<int, string> {
                {0,"NONE"},
                {1,"QUATERNION"},
                {2,"HDR"},
                {3,"RAW"},
                {4,"RAW_AND_QUATERNION"},
                {5,"HIGH_RESOLUTION"},
                {6,"SYNC"}
            };
            return LogModeValue;
        }

        #endregion

        #region CALIBRATION TIMES / FILTER FREQUENCES

        internal const uint GYRO_CALIB_TIME = 10000;    // Duration of the gyroscope offset estimation in ms

        internal const uint MAGN_CALIB_TIME = 15000;    // Duration of magnetometer calibration time in ms

        internal const uint DIP_CALIB_TIME = 10000;     // Duration of the magnetic dip angle estimation in ms

        public const float REFERENCE_EPOCH = 1530000000; //26 June 2018, 8:00:00

        #endregion

        #region CALIBRATION / CONFIGURATION COMMANDS 

        /// <summary> Gyroscope offset calibration [perform and set] </summary>
        /// \hideinitializer
        public static readonly string CalibrateGyroscope = "?!GYRCALIB!?";
        /// <summary> Gyroscope offset calibration [get] </summary>
        /// \hideinitializer
        public static readonly string GetGyroscopeOffset = "?!GETGBIAS!?";

        /// <summary> Accelerometer calibration [set] </summary>
        /// \hideinitializer
        public static readonly string SetAccelerometerCalibParams = "?!SETACCAL!?";
        /// <summary> Accelerometer calibration [get] </summary>
        /// \hideinitializer
        public static readonly string GetAccelerometerCalibParams = "?!GETACCAL!?";
        
        /// <summary> Magnetometer calibration [set] </summary>
        /// \hideinitializer
        public static readonly string SetMagnetometerCalibParams = "?!SETCALIB!?";
        /// <summary> Magnetometer calibration [get] </summary>
        /// \hideinitializer
        public static readonly string GetMagnetometerCalibParams = "?!GETCALIB!?";

        /// <summary> OPTIONAL - Magnetic Inclination Angle [perform and set] </summary>
        /// \hideinitializer
        public static readonly string ComputeMagneticInclinationAngle = "?!DIPCALIB!?";
        /// <summary> OPTIONAL - Magnetometer Inclination [get] </summary>
        /// \hideinitializer
        public static readonly string GetMagneticInclinationAngle = "?!GETMDANG!?";


        /// <summary> Gyroscope full scale [set] </summary>
        /// \hideinitializer
        public static readonly string SetGyroscopeFullScale = "?!GYLFS{0:D3}!?";
        /// <summary> Accelerometer full scale [set] </summary>
        /// \hideinitializer
        public static readonly string SetAccelerometerFullScale = "?!ACLFS{0:D3}!?";
        /// <summary> HDR Accelerometer full scale [set] </summary>
        /// \hideinitializer
        public static readonly string SetAccelerometerHDRFullScale = "?!ACHFS{0:D3}!?";
        /// <summary> Magnetometer full scale [set] </summary>
        /// \hideinitializer
        public static readonly string SetMagnetometerFullScale = "?!MAGFS{0:D3}!?";


        /// <summary> Sensor body pose index [set] </summary>
        /// \hideinitializer
        // public static readonly string SetSensorBodyPose = "?!SETBP{0:D3}!?";
        /// <summary> Sensor body pose index [get] </summary>
        /// \hideinitializer
        // public static readonly string GetSensorBodyPose = "?!GETBPOSE!?";


        /// <summary> Enable / Disable sensor sleep mode [set] </summary>
        /// \hideinitializer
        public static readonly string SetSensorSleepMode = "?!SETSLMOD!?";
        /// <summary> Sensor sleep mode flag [get] </summary>
        /// \hideinitializer
        public static readonly string GetSensorSleepMode = "?!GETSLMOD!?";


        /// <summary> Retrieve device configuration parameters [get] </summary>
        /// \hideinitializer
        public static readonly string GetConfigurationParams = "?!GETPARAM!?";

        /// <summary> Store all parameters (calibration and configuration) in the non-volatile memory of the device [store] </summary>
        /// \hideinitializer
        public static readonly string StoreConfigurationParameters = "?!STOREPAR!?";

        #endregion

        #region STREAMING COMMANDS

        // <summary> Start raw data streaming </summary>
        // \hideinitializer
        //public static readonly string StreamRawData = "?!RAWTX{0:D3}!?";
        /// <summary> Start quaterion streaming </summary>
        /// \hideinitializer
        public static readonly string StreamQuaternion = "?!START{0:D3}!?";
        /// <summary> Start raw data and quaterion streaming </summary>
        /// \hideinitializer
        public static readonly string StreamRawAndQuaternionData = "?!CALIB{0:D3}!?";
        /// <summary> Start HDR data streaming </summary>
        /// \hideinitializer
        public static readonly string StreamHDRData = "?!TXHDR{0:D3}!?";

        #endregion

        #region LOG COMMANDS

        /// <summary> Setup log mode. </summary>
        /// \hideinitializer
        public static readonly string SetLogMode = "?!LOGMD{0:D3}!?";
        /// <summary> Setup log frequency. </summary>
        /// \hideinitializer
        public static readonly string SetLogFrequency = "?!LOGFS{0:D3}!?";
        /// <summary> Enable data logging via Bluetooth. </summary>
        /// \hideinitializer
        public static readonly string SetupLogBT = "?!SETUPLOG!?";
        /// <summary> Start data logging via Bluetooth. </summary>
        /// \hideinitializer
        public static readonly string StartLogBT = "?!LOGSTART!?";

        #endregion

        #region TYME SYNC COMMANDS

        /// <summary> Enter Time Sync Mode operation. </summary>
        /// \hideinitializer
        public static readonly string EnterTimeSyncMode = "?!TIMESYNC!?";
        /// <summary> Exit Time Sync Mode operation. </summary>
        /// \hideinitializer
        public static readonly string ExitTimeSyncMode = "?!";

        /// <summary> Estimate clock drift. </summary>
        /// \hideinitializer
        public static readonly string ComputeClockDrift = "!!";
        /// <summary> Set clock drift. </summary>
        /// \hideinitializer
        public static readonly string SetClockDrift = "?!SETDRIFT!?";
        /// <summary> Get clock drift. </summary>
        /// \hideinitializer
        public static readonly string GetClockDrift = "?!GETDRIFT!?";

        /// <summary> Estimate clock offset. </summary>
        /// \hideinitializer
        public static readonly string ComputeClockOffset = "??";
        /// <summary> Set clock offset. </summary>
        /// \hideinitializer
        public static readonly string SetClockOffset = "?!SETCKOFF!?";
        /// <summary> Get clock offset. </summary>
        /// \hideinitializer
        public static readonly string GetClockOffset = "?!GETCKOFF!?";

        /// <summary> Get timestamp. </summary>
        /// \hideinitializer
        public static readonly string GetTimestamp = "!?";

        #endregion

        #region MEMORY MANAGEMENT COMMANDS

        /// <summary> Get the available space on the flash memory (6MB maximum). </summary>
        /// \hideinitializer
        public static readonly string GetAvailableMemory = "?!MEMSPACE!?";
        /// <summary> Erase memory. </summary>
        /// \hideinitializer
        public static readonly string EraseMemory = "?!ERASEMEM!?";
        /// <summary> Readout memory. </summary>
        /// \hideinitializer
        public static readonly string ReadMemory = "?!READDATA!?";

        /// <summary> Get the list of files written on memory. </summary>
        /// \hideinitializer
        public static readonly string GetFiles = "?!GETFILES!?";
        /// <summary> Get the specified file. </summary>
        /// \hideinitializer
        public static readonly string ReadFile = "?!READF{0:D3}!?";
        /// <summary> Get the log corresponding to one particular file within a specified time span. </summary>
        /// \hideinitializer
        public static readonly string ReadFileSpan = "?!FILESPAN!?";

        /// <summary> Get the number of bytes to download for a specified file. </summary>
        /// \hideinitializer
        public static readonly string GetPacketsNumber = "?!GETFP{0:D3}!?";
        /// <summary> Get one packet at a time for a specific file. </summary>
        /// \hideinitializer
        public static readonly string ReadPacket = "?!GETFILEP!?";

        #endregion

        #region ACKNOWLEDGE COMMANDS

        /// <summary> Confirmation message to be sent every 20 received packets. </summary>
        /// \hideinitializer
        public static readonly string PacketOK = "?!PACKETOK!?";
        /// <summary> Data corruption message to be issued within 1.5 seconds from the last packet transmission. </summary>
        /// \hideinitializer
        public static readonly string PacketKO = "?!PACKETKO!?";

        #endregion

        #region EMBEDDED BLUETOOTH MODULE COMMANDS

        /// <summary> Escape Bluetooth module. </summary>
        /// \hideinitializer
        public static readonly string BTcmdEscape = "@#@$@%";
        /// <summary> Reset Bluetooth module. </summary>
        /// \hideinitializer
        public static readonly string BTcmdReset = "AT+AB Reset\n";
        /// <summary> Bypass Bluetooth module. </summary>
        /// \hideinitializer
        public static readonly string BTcmdBypass = "AT+AB Bypass\n";
        /// <summary> Rename Bluetooth module. </summary>
        /// \hideinitializer
        public static readonly string BTcmdRename = "AT+AB DefaultLocalName {0}\n";

        #endregion

        #region GENERAL COMMANDS

        /// <summary> Stop any kind of data streaming. </summary>
        /// \hideinitializer
        public static readonly string StopTransmission = "?!STOPSEND!?";
        /// <summary> Shutdown device. </summary>
        /// \hideinitializer
        public static readonly string Shutdown = "?!SHUTDOWN!?";

        #endregion

        #region MISCELLANEOUS COMMANDS

        /// <summary> Getting the battery voltage. </summary>
        /// \hideinitializer
        public static readonly string GetBatteryVoltage = "?!GETBATTV!?";
        /// <summary> Getting the battery charge. </summary>
        /// \hideinitializer
        public static readonly string GetBatteryCharge = "?!GETBATTQ!?";

        /// <summary> Setting the RTC date. </summary>
        /// \hideinitializer
        public static readonly string SetDate = "?!SETEPOCH!?";
        /// <summary> Getting the EPOCH value stored in the RTC. </summary>
        /// \hideinitializer
        public static readonly string GetDate = "?!GETEPOCH!?";

        /// <summary> Getting the version of the firmware. </summary>
        /// \hideinitializer
        public static readonly string GetFirmwareVersion = "?!FIRMWARE!?";

        /// <summary> HDR mode availability check. </summary>
        /// \hideinitializer
        public static readonly string CheckHDRAvailability = "?!HDRMUSE?!?";

        /// <summary> Embedded magnetometer calibration. </summary>
        /// \hideinitializer
        public static readonly string EmbeddedMagnetometerCalibration = "?!MAGCALIB!?";

        #endregion
    }
}
