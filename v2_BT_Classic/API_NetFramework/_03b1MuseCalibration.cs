﻿//-----------------------------------------------------------------
//
// 221e srl CONFIDENTIAL
// 
// File Name:  _03b1SerialConnection.cs
// File Type:  Visual C# Source file
// Author:     Roberto Bortoletto
// Company:    221e srl
//             Copyright (c) 2017 All Rights Reserved
//-----------------------------------------------------------------

using System;
using System.Collections.Generic;

namespace Pit221e._03b1Muse
{
    /// <summary> _03b1MuseCalibration class. </summary>
    internal class _03b1MuseCalibration
    {
        private const int DOF = 9;                                  // degrees of freedom
        private float REFR_;                                         // Calibration reference (Acc: 1000; Mag: 400)

        private float[] DtD_;                                       // D'*D
        private float[] DtDcopy_;                                   // see save/restore_stat_run_magcalib()

        private float[] Dt1_;                                       // D'*ones
        private float[] Dt1copy_;

        private float[] v_;                                         // ellipsoid parameters
        
        private float vlast_;                                       // optimization (avoid risk of div-by-zero)

        private float[] Oxyz_;                                      // ellipsoid center/offset
        private float[] Rxyz_;                                      // ellipsoid radii/gain (related to eigenvalues)
        private float[] R3mtx_;                                     // ellipsoid rotation matrix (eigenvectors)

        private float[] R3mtxR_;                                    // optimize by composing rotation and scaling to ref radius
        private float[] OOxyz_;                                     // diagonal matrix * offset

        private float eig1_, eig2_, eig3_;                          // eigenvectors, shared by gain_magcalib() and R3mtx_magcalib()
        private float eigN_;                                        // normalization factor

        #region PROPERTIES

        public float CalibReference
        {
            get { return REFR_; }
            set { REFR_ = value; }
        }

        public float[] CalibParams
        {
            get { return GetParameters(); }
        }

        #endregion

        #region CONSTRUCTOR

        /// <summary> Constructor. Initializes a new instance of the class. </summary>
        /// <param name="reference"> Sensor calibration reference (i.e., 400 mG for Magnetometer, 1000 mg for Accelerometer). </param>
        public _03b1MuseCalibration(float reference, List<_03b1MuseMessage> MessageList)
        {
            DtD_ = new float[DOF * DOF];        // D'*D
            DtDcopy_ = new float[DOF * DOF];    // see save/restore_stat_run_magcalib()

            Dt1_ = new float[DOF];              // D'*ones
            Dt1copy_ = new float[DOF];

            v_ = new float[DOF];                // ellipsoid parameters

            vlast_ = 0;                         // optimization (avoid risk of div-by-zero)

            Oxyz_ = new float[3];               // ellipsoid center/offset
            Rxyz_ = new float[3];               // ellipsoid radii/gain (related to eigenvalues)
            R3mtx_ = new float[3 * 3];          // ellipsoid rotation matrix (eigenvectors)

            R3mtxR_ = new float[3 * 3];         // optimize by composing rotation and scaling to ref radius
            OOxyz_ = new float[3];              // diagonal matrix * offset

            eig1_ = 0;
            eig2_ = 0;
            eig3_ = 0;                          // eigenvectors, shared by gain_magcalib() and R3mtx_magcalib()

            eigN_ = 0;                          // normalization factor

            this.ResetCalibration();
            this.CalibReference = reference;
        }

        #endregion

        #region PUBLIC METHODS

        /// <summary> Perform generic sensor calibration. </summary>
        public bool RunCalibration()
        {
            bool ok = SolveCalib();
            FinalizeCalib();
            ok = ok && Check();
            ok = ok && CheckOffset();
            ok = ok && CheckGain();
            if (ok)
            {
                R3mtxCalib();                              // rotation matrix
                R3mtxOpt();                                // optimize rotation/gain
                ok = ok && InitRemapCalib() == 1;      // now remap_mag_calib() can be used
            }
            return ok;
        }

        /// <summary> Reset sensor calibration parameters to identity matrix and zero offset. </summary>
        public void ResetCalibration()                              // reset_remap_magcalib()
        {
            for (int r = 0; r < DOF; r++)
            {
                Dt1_[r] = 0.0f;
                v_[r] = 0.0f;
                for (int c = 0; c < DOF; c++)
                    DtD_[r * DOF + c] = 0.0f;
            }
            for (int r = 0; r < 3; r++)
            {
                Oxyz_[r] = 0.0f;
                Rxyz_[r] = 1.0f;
                for (int c = 0; c < 3; c++)
                    R3mtx_[r * 3 + c] = ((r == c) ? 1.0f : 0.0f);
            }
        }

        /// <summary> Perform accelerometer calibration. </summary>
        /// <param name="reference"> Sensor calibration reference (i.e., 1000 mg for Accelerometer). </param>
        public bool CalibrateAccelerometer(float reference)
        {
            this.ResetCalibration();
            this.CalibReference = reference;

            return false;
        }

        /// <summary> Perform magnetometer calibration. </summary>
        /// <param name="reference"> Sensor calibration reference (i.e., 400 mG for Magnetometer). </param>
        public bool CalibrateMagnetometer(float reference)
        {
            this.ResetCalibration();
            this.CalibReference = reference;

            return false;
        }

        #endregion

        ////////////////////////////////////////////////////////////////////////////////////////////

        #region PERFORM CALIBRATION

        private void InitCalib(float[] O, float[] R)                // set_magcalib(float[] O, float[] R)
        {
            // Initilize the parameters with the ones stored in the flash
            for (int r = 0; r < 3; r++)
            {
                Oxyz_[r] = O[r];
                for (int c = 0; c < 3; c++)
                    R3mtxR_[r * 3 + c] = R[r * 3 + c];
            }
        }

        private void RunCalib(float x, float y, float z)            // run_magcalib(float x, float y, float z)
        {
            float x2, y2, z2, x2y2z2;
            x2 = x * x;
            y2 = y * y;
            z2 = z * z;
            x2y2z2 = x2 + y2 + z2;
            v_[0] = x2 + y2 - 2.0f * z2;
            v_[1] = x2 - 2.0f * y2 + z2;
            v_[2] = 4.0f * x * y;
            v_[3] = 2.0f * x * z;
            v_[4] = 2.0f * y * z;
            v_[5] = 2.0f * x;
            v_[6] = 2.0f * y;
            v_[7] = 2.0f * z;
            v_[8] = 1.0f;
            for (int r = 0; r < DOF; r++)
            {
                for (int c = 0; c < DOF; c++)
                    DtD_[r * DOF + c] += (v_[r] * v_[c]);
                Dt1_[r] += v_[r] * x2y2z2;
            }
        }

        private bool SolveCalib()                                   // solve_magcalib()
        {
            float k;

            for (int r = 0; r < DOF; r++) 
                v_[r] = Dt1_[r];                    // final multiplication merged in
            
            for (int r = 0; r < DOF; r++)
            {
                if ((k = DtD_[r * DOF + r]) != 0.0f)
                {
                    for (int c2 = 0; c2 < DOF; c2++)
                        DtD_[r * DOF + c2] /= k;

                    v_[r] /= k;                     // final multiplication merged in
                }
                else
                {
                    int r2;
                    for (r2 = r + 1; (r2 < DOF) && ((k = DtD_[r2 * DOF + r]) == 0.0f); r2++) ;
                    if (k == 0.0f)
                        return false; // can not be inverted

                    for (int c2 = 0; c2 < DOF; c2++)
                        DtD_[r * DOF + c2] += DtD_[r2 * DOF + c2] / k; 
                    v_[r] += v_[r2] / k;            // final multiplication merged in
                }
                for (int r2 = 0; r2 < DOF; r2++)
                {
                    if (r2 != r)
                    {
                        k = -DtD_[r2 * DOF + r];
                        for (int c2 = 0; c2 < DOF; c2++)
                            DtD_[r2 * DOF + c2] += DtD_[r * DOF + c2] * k;  
                        v_[r2] += v_[r] * k;        // final multiplication merged in
                    }
                }
            }

            return true; // inversion successfull
        }

        private void FinalizeCalib()                                // solve_finish_magcalib()
        {
            float t1, t2;
            vlast_ = v_[8];
            for (int r = DOF - 1; r > 2; r--)
                v_[r] = -v_[r - 1];
            v_[3] = 2.0f * v_[3];
            t1 = v_[0];
            t2 = v_[1];
            v_[0] = (1.0f - t1 - t2);
            v_[1] = (1.0f - t1 + 2.0f * t2);
            v_[2] = (1.0f + 2.0f * t1 - t2);
            return;
        }

        private float[] GetParameters()
        {
            float[] result = new float[12];
            for (int i = 0; i < 9; i++)
            {
                result[i] = R3mtxR_[i];
            }
            //Compute the equivalent offset (M3txR*Oxyz)
            result[9] = OOxyz_[0];
            result[10] = OOxyz_[1];
            result[11] = OOxyz_[2];
            return result;
        }

        #endregion

        #region CALIBRATION MATRIX COMPUTATION

        private void R3mtxCalib()                                   // R3mtx_magcalib()
        {
            // must be called after gain_magcalib()
            float[] T3 = new float[3 * 3];
            float n;
            int rT;
            // A - eig1*eye(3)
            T3[0] = v_[0] / eigN_ - eig3_;
            T3[1] = v_[3] / eigN_;
            T3[2] = v_[4] / eigN_;
            T3[3] = T3[1];
            T3[4] = v_[1] / eigN_ - eig3_;
            T3[5] = v_[5] / eigN_;
            T3[6] = T3[2];
            T3[7] = T3[5];
            T3[8] = v_[2] / eigN_ - eig3_;
            rT = RankReduce(T3);
            if (rT == 0)
                return;  // init_magcalib() has already set R3mtx[] to eye(3)
            if (rT == 1)
            {
                UVcomplementS(R3mtx_, R3mtx_, T3, 0, 3, 0);
                SeqUcrossV(R3mtx_, R3mtx_, R3mtx_, 6, 0, 3);
            }
            else
            { // (rT==2)
                SeqUcrossV(R3mtx_, T3, T3, 0, 0, 3);

                n = (float)Math.Sqrt(R3mtx_[0] * R3mtx_[0] + R3mtx_[1] * R3mtx_[1] + R3mtx_[2] * R3mtx_[2]);
                R3mtx_[0] /= n;
                R3mtx_[1] /= n;
                R3mtx_[2] /= n;
                // A - eig2*eye(3)
                T3[0] = v_[0] / eigN_ - eig2_;
                T3[1] = v_[3] / eigN_;
                T3[2] = v_[4] / eigN_;
                T3[3] = T3[1];
                T3[4] = v_[1] / eigN_ - eig2_;
                T3[5] = v_[5] / eigN_;
                T3[6] = T3[2];
                T3[7] = T3[5];
                T3[8] = v_[2] / eigN_ - eig2_;
                rT = RankReduce(T3);
                if (rT == 1)
                {
                    UVcomplementS(R3mtx_, R3mtx_, R3mtx_, 3, 6, 0);
                }
                else
                { // (rt==2)
                    SeqUcrossV(R3mtx_, T3, T3, 3, 0, 3);
                    n = (float)Math.Sqrt(R3mtx_[3] * R3mtx_[3] + R3mtx_[4] * R3mtx_[4] + R3mtx_[5] * R3mtx_[5]);
                    R3mtx_[3] /= n;
                    R3mtx_[4] /= n;
                    R3mtx_[5] /= n;
                }
                SeqUcrossV(R3mtx_, R3mtx_, R3mtx_, 6, 0, 3);
            }
        }                       

        private void R3mtxOpt()                                     // R3mtx_gain_opt_magcalib()
        {
            // keep X,Y,Z axes if possible: largest values along diagonal, positive values
            int r, c, rm = 0, cm = 0;
            float t, m;
            m = 0.0f;
            for (r = 0; r < 3; r++)
                for (c = 0; c < 3; c++)
                    if ((t = (float)Math.Abs(R3mtx_[r * 3 + c])) > m)
                    {
                        m = t;
                        rm = r;
                        cm = c;
                    }
            if (rm != cm)
            {
                for (c = 0; c < 3; c++)
                {
                    t = R3mtx_[cm * 3 + c];
                    R3mtx_[cm * 3 + c] = R3mtx_[rm * 3 + c];
                    R3mtx_[rm * 3 + c] = t;
                } // row cm <-> row rm
                t = Rxyz_[cm];
                Rxyz_[cm] = Rxyz_[rm];
                Rxyz_[rm] = t; // radii in sync with eigenvectors
            }
            // now decision to swap or not rows in remaining 2x2 matrix
            if (cm == 2)
            {
                m = (float)Math.Abs(R3mtx_[0]);
                rm = 0;
                if ((t = (float)Math.Abs(R3mtx_[1])) > m)
                {
                    m = t;
                    rm = 1;
                }
                if ((t = (float)Math.Abs(R3mtx_[3])) > m)
                {
                    m = t;
                    rm = 1;
                }
                if (((float)Math.Abs(R3mtx_[4])) > m)
                    rm = 0;
                if (rm > 0)
                {
                    for (c = 0; c < 3; c++)
                    {
                        t = R3mtx_[c];
                        R3mtx_[c] = R3mtx_[3 + c];
                        R3mtx_[3 + c] = t;
                    }
                    t = Rxyz_[0];
                    Rxyz_[0] = Rxyz_[1];
                    Rxyz_[1] = t;
                }
            }
            else if (cm == 1)
            {
                m = (float)Math.Abs(R3mtx_[0]);
                rm = 0;
                if ((t = (float)Math.Abs(R3mtx_[2])) > m)
                {
                    m = t;
                    rm = 1;
                }
                if ((t = (float)Math.Abs(R3mtx_[6])) > m)
                {
                    m = t;
                    rm = 1;
                }
                if (((float)Math.Abs(R3mtx_[8])) > m)
                    rm = 0;
                if (rm > 0)
                {
                    for (c = 0; c < 3; c++)
                    {
                        t = R3mtx_[c];
                        R3mtx_[c] = R3mtx_[6 + c];
                        R3mtx_[6 + c] = t;
                    }
                    t = Rxyz_[0];
                    Rxyz_[0] = Rxyz_[2];
                    Rxyz_[2] = t;
                }
            }
            else
            {
                m = (float)Math.Abs(R3mtx_[4]);
                rm = 0;
                if ((t = (float)Math.Abs(R3mtx_[5])) > m)
                {
                    m = t;
                    rm = 1;
                }
                if ((t = (float)Math.Abs(R3mtx_[7])) > m)
                {
                    m = t;
                    rm = 1;
                }
                if (((float)Math.Abs(R3mtx_[8])) > m)
                    rm = 0;
                if (rm > 0)
                {
                    for (c = 0; c < 3; c++)
                    {
                        t = R3mtx_[3 + c];
                        R3mtx_[3 + c] = R3mtx_[6 + c];
                        R3mtx_[6 + c] = t;
                    }
                    t = Rxyz_[1];
                    Rxyz_[1] = Rxyz_[2];
                    Rxyz_[2] = t;
                }
            }
            // positive along diagonal
            if (R3mtx_[0] < 0.0f)
                for (c = 0; c < 3; c++)
                    R3mtx_[c] = -R3mtx_[c];
            if (R3mtx_[4] < 0.0f)
                for (c = 0; c < 3; c++)
                    R3mtx_[3 + c] = -R3mtx_[3 + c];
            if (R3mtx_[8] < 0.0f)
                for (c = 0; c < 3; c++)
                    R3mtx_[6 + c] = -R3mtx_[6 + c];
        }

        private int InitRemapCalib()                                // init_remap_magcalib()
        {
            int r, c, r2, c2;
            float k;
            float[] TR3mtx = new float[3 * 3]; // iR3mtx[3*3], T[3*3];
            R3mtxR_[0] = R3mtx_[0] / Rxyz_[0] * REFR_;
            R3mtxR_[1] = R3mtx_[1] / Rxyz_[0] * REFR_;
            R3mtxR_[2] = R3mtx_[2] / Rxyz_[0] * REFR_;
            R3mtxR_[3] = R3mtx_[3] / Rxyz_[1] * REFR_;
            R3mtxR_[4] = R3mtx_[4] / Rxyz_[1] * REFR_;
            R3mtxR_[5] = R3mtx_[5] / Rxyz_[1] * REFR_;
            R3mtxR_[6] = R3mtx_[6] / Rxyz_[2] * REFR_;
            R3mtxR_[7] = R3mtx_[7] / Rxyz_[2] * REFR_;
            R3mtxR_[8] = R3mtx_[8] / Rxyz_[2] * REFR_;

            for (r = 0; r < 3; r++)
                for (c = 0; c < 3; c++)
                    TR3mtx[r + c * 3] = R3mtx_[c + r * 3];
            for (r = 0; r < 3; r++)
            {
                if ((k = TR3mtx[r + r * 3]) != 0.0)
                    for (c2 = 0; c2 < 3; c2++)
                    {
                        TR3mtx[r + c2 * 3] /= k;
                        R3mtxR_[c2 + r * 3] /= k;
                    }
                else
                {
                    for (r2 = r + 1; (r2 < 3) && ((k = TR3mtx[r2 + r * 3]) == 0.0); r2++)
                        ;
                    if (k == 0.0f)
                        return 0;
                    for (c2 = 0; c2 < 3; c2++)
                    {
                        TR3mtx[r + c2 * 3] += TR3mtx[r2 + c2 * 3] / k;
                        R3mtxR_[c2 + r * 3] += R3mtxR_[c2 + r2 * 3] / k;
                    }
                }
                for (r2 = 0; r2 < 3; r2++)
                    if (r2 != r)
                    {
                        k = -TR3mtx[r2 + r * 3];
                        for (c2 = 0; c2 < 3; c2++)
                        {
                            TR3mtx[r2 + c2 * 3] += TR3mtx[r + c2 * 3] * k;
                            R3mtxR_[c2 + r2 * 3] += R3mtxR_[c2 + r * 3] * k;
                        }
                    }
            }

            //printf("R3mtxR\n"); for(r=0;r<3;r++) { for(c=0;c<3;c++) printf("%10lf ",R3mtxR[c*3+r]); printf("\n"); }

            OOxyz_[0] = 0.0f;
            OOxyz_[1] = 0.0f;
            OOxyz_[2] = 0.0f;
            for (c = 0; c < 3; c++)
            {
                OOxyz_[0] += R3mtxR_[0 + c] * Oxyz_[c];
                OOxyz_[1] += R3mtxR_[3 + c] * Oxyz_[c];
                OOxyz_[2] += R3mtxR_[6 + c] * Oxyz_[c];
            }

            return 1;
        }

        private void RemapCalib(float[] x, float[] y, float[] z)    // remap_magcalib(float[] x, float[] y, float[] z)
        { // optimized, needs init_remap_magcalib()
            float tx, ty, tz;
            tx = x[0] - Oxyz_[0];
            ty = y[0] - Oxyz_[1];
            tz = z[0] - Oxyz_[2]; // remove offset
            x[0] = tx * R3mtxR_[0] + ty * R3mtxR_[1] + tz * R3mtxR_[2]; // rotate and scale to reference radius
            y[0] = tx * R3mtxR_[3] + ty * R3mtxR_[4] + tz * R3mtxR_[5];
            z[0] = tx * R3mtxR_[6] + ty * R3mtxR_[7] + tz * R3mtxR_[8];
        }

        #endregion

        #region CHECK CALIBRATION

        private bool Check()                                        // check_magcalib()
        {
            int ko = 0;
            if (v_[0] <= 0.0f)
                ko = ko + 1;        // a,b,c always positive
            if (v_[1] <= 0.0f)
                ko = ko + 2;
            if (v_[2] <= 0.0f)
                ko = ko + 4;
            if ((v_[3] * v_[3]) >= (4 * v_[0] * v_[1]))
                ko = ko + 8;        // d^2-4ab<0 (XYcoeff^2-4*X2coeff*Y2coeff)
            if ((v_[4] * v_[4]) >= (4 * v_[0] * v_[2]))
                ko = ko + 16;       // e^2-4ac<0 (XZcoeff^2-4*X2coeff*Z2coeff)
            if ((v_[5] * v_[5]) >= (4 * v_[1] * v_[2]))
                ko = ko + 32;       // f^2-4bc<0 (YZcoeff^2-4*Y2coeff*Z2coeff)
            return (ko == 0);       // return 'ok' flag
        }

        private bool CheckOffset()                                  // offset_magcalib()
        {
            float[] T3 = new float[3 * 3];
            float k;

            T3[0] = v_[0];
            T3[1] = v_[3];
            T3[2] = v_[4];
            T3[3] = v_[3];
            T3[4] = v_[1];
            T3[5] = v_[5];
            T3[6] = v_[4];
            T3[7] = v_[5];
            T3[8] = v_[2];
            Oxyz_[0] = -v_[6];
            Oxyz_[1] = -v_[7];
            Oxyz_[2] = -v_[8];
            for (int r = 0; r < 3; r++)
            {
                if ((k = T3[r * 3 + r]) != 0.0f)
                {
                    for (int c2 = 0; c2 < 3; c2++)
                        T3[r * 3 + c2] /= k;
                    Oxyz_[r] /= k;                      // final multiplication merged in
                }
                else
                {
                    int r2;
                    for (r2 = r + 1; (r2 < 3) && ((k = T3[r2 * 3 + r]) == 0.0f); r2++) ;
                    if (k == 0.0f)
                        return false; // can not be inverted

                    for (int c2 = 0; c2 < 3; c2++)
                        T3[r * 3 + c2] += T3[r2 * 3 + c2] / k;
                    Oxyz_[r] += Oxyz_[r2] / k;          // final multiplication merged in
                }
                for (int r2 = 0; r2 < 3; r2++)
                {
                    if (r2 != r)
                    {
                        k = -T3[r2 * 3 + r];
                        for (int c2 = 0; c2 < 3; c2++)
                            T3[r2 * 3 + c2] += T3[r * 3 + c2] * k;
                        Oxyz_[r2] += Oxyz_[r] * k;      // final multiplication merged in
                    }
                }
            }
            return true;
        }

        private bool CheckGain()                                    // gain_magcalib()
        { 
            // must be called after offset_magcalib()
            float a11, a12, a13, a22, a23, a33, b11, b12, b13, b22, b23, b33;
            float p1, p2, p, q, r, phi;
            int ok;

            eigN_ = (Oxyz_[0] * v_[0] + Oxyz_[1] * v_[3] + Oxyz_[2] * v_[4] + v_[6]) * Oxyz_[0]
                    + (Oxyz_[0] * v_[3] + Oxyz_[1] * v_[1] + Oxyz_[2] * v_[5] + v_[7]) * Oxyz_[1]
                    + (Oxyz_[0] * v_[4] + Oxyz_[1] * v_[5] + Oxyz_[2] * v_[2] + v_[8]) * Oxyz_[2]
                    + (Oxyz_[0] * v_[6] + Oxyz_[1] * v_[7] + Oxyz_[2] * v_[8] - vlast_);

            eigN_ = -eigN_;

            a11 = v_[0] / eigN_;
            a12 = v_[3] / eigN_;
            a13 = v_[4] / eigN_;
            a22 = v_[1] / eigN_;
            a23 = v_[5] / eigN_;
            a33 = v_[2] / eigN_;

            p1 = a12 * a12 + a13 * a13 + a23 * a23;
            if (p1 == 0.0f)
            {   // diagonal
                eig1_ = a11;
                eig2_ = a22;
                eig3_ = a33;
            }
            else
            {   // not diagonal
                q = (a11 + a22 + a33) / 3.0f;
                p2 = (a11 - q) * (a11 - q) + (a22 - q) * (a22 - q)
                        + (a33 - q) * (a33 - q) + 2.0f * p1;
                p = (float)Math.Sqrt(p2 / 6.0f);

                // B = (1/p)*(A-q*I)
                b11 = (a11 - q) / p;
                b12 = a12 / p;
                b13 = a13 / p;
                b22 = (a22 - q) / p;
                b23 = a23 / p;
                b33 = (a33 - q) / p;

                // r = det(B)/2;
                r = (b11 * (b22 * b33 - b23 * b23) - b12 * (b12 * b33 - b23 * b13) + b13 * (b12 * b23 - b22 * b13)) / 2.0f;

                if (r <= -1.0f)
                    phi = (float)Math.PI / 3.0f;
                else if (r >= +1.0f)
                    phi = 0.0f;
                else
                    phi = (float)Math.Acos(r) / 3.0f;

                // eig3 <= eig2 <= eig1
                eig1_ = q + 2.0f * p * ((float)Math.Cos(phi));
                eig3_ = q + 2.0f * p * ((float)Math.Cos(phi + 2.0f * Math.PI / 3.0f));
                eig2_ = 3.0f * q - eig1_ - eig3_; 
            }
            ok = 1;
            if (eig3_ > 0.0f)
                Rxyz_[0] = (float)Math.Sqrt(1.0f / eig3_);
            else
                ok = 0;
            if (eig2_ > 0.0f)
                Rxyz_[1] = (float)Math.Sqrt(1.0f / eig2_);
            else
                ok = 0;
            if (eig1_ > 0.0f)
                Rxyz_[2] = (float)Math.Sqrt(1.0f / eig1_);
            else
                ok = 0;
            return ok == 1;
        }

        #endregion

        #region CALIBRATION STATS

        private void SaveStatCalib()
        {
            for (int r = 0; r < DOF; r++)
            {
                Dt1copy_[r] = Dt1_[r];
                for (int c = 0; c < DOF; c++)
                    DtDcopy_[r * DOF + c] = DtD_[r * DOF + c];
            }
        }

        private void RestoreStatCalib()
        {
            for (int r = 0; r < DOF; r++)
            {
                Dt1_[r] = Dt1copy_[r];
                for (int c = 0; c < DOF; c++)
                    DtD_[r * DOF + c] = DtDcopy_[r * DOF + c];
            }
        }

        #endregion

        #region MATRIX METHODS

        private int RankReduce(float[] T)
        { 
            // compute rank and reduce T to upper triangular matrix
            float k;
            for (int r = 0; r < 3; r++)
            {
                if ((k = T[r * 3 + r]) != 0.0f)
                {
                    for (int c2 = 0; c2 < 3; c2++)
                        T[r * 3 + c2] /= k;
                }
                else
                {
                    int r2;
                    for (r2 = r + 1; (r2 < 3) && ((k = T[r2 * 3 + r]) == 0.0f); r2++)
                        ;
                    if (k == 0.0f)
                        return r; // can not be inverted
                    for (int c2 = 0; c2 < 3; c2++)
                        T[r * 3 + c2] += T[r2 * 3 + c2] / k;
                }
                for (int r2 = r + 1; r2 < 3; r2++)
                {
                    k = -T[r2 * 3 + r];
                    for (int c2 = 0; c2 < 3; c2++)
                        T[r2 * 3 + c2] += T[r * 3 + c2] * k;
                }
            }
            return 3;
        }

        private void UVcomplementS(float[] u, float[] v, float[] s, int o1, int o2, int o3)
        {
            float n;
            n = (float)Math.Sqrt(s[o3 + 0] * s[o3 + 0] + s[o3 + 1] * s[o3 + 1] + s[o3 + 2] * s[o3 + 2]);
            s[o3 + 0] /= n;
            s[o3 + 1] /= n;
            s[o3 + 2] /= n;
            if (Math.Abs(s[o3 + 0]) >= Math.Abs(s[o3 + 1]))
            {
                n = 1 / ((float)Math.Sqrt(s[o3 + 0] * s[o3 + 0] + s[o3 + 2] * s[o3 + 2]));
                u[o1 + 0] = -s[o3 + 2] * n;
                u[o1 + 1] = 0.0f;
                u[o1 + 2] = +s[o3 + 0] * n;
                v[o2 + 0] = +s[o3 + 1] * u[o1 + 2];
                v[o2 + 1] = +s[o3 + 2] * u[o1 + 0] - s[o3 + 0] * u[o1 + 2];
                v[o2 + 2] = -s[o3 + 1] * u[o1 + 0];
            }
            else
            {
                n = 1 / ((float)Math.Sqrt(s[o3 + 1] * s[o3 + 1] + s[o3 + 2] * s[o3 + 2]));
                u[o1 + 0] = 0.0f;
                u[o1 + 1] = +s[o3 + 2] * n;
                u[o1 + 2] = -s[o3 + 1] * n;
                v[o2 + 0] = +s[o3 + 1] * u[o1 + 2] - s[o3 + 2] * u[o1 + 1];
                v[o2 + 1] = -s[o3 + 0] * u[o1 + 2];
                v[o2 + 2] = +s[o3 + 0] * u[o1 + 1];
            }
        }

        private void SeqUcrossV(float[] s, float[] u, float[] v, int o1, int o2, int o3)
        {
            s[o1 + 0] = u[o2 + 1] * v[o3 + 2] - u[o2 + 2] * v[o3 + 1];
            s[o1 + 1] = u[o2 + 2] * v[o3 + 0] - u[o2 + 0] * v[o3 + 2];
            s[o1 + 2] = u[o2 + 0] * v[o3 + 1] - u[o2 + 1] * v[o3 + 0];
        }

        #endregion

        ////////////////////////////////////////////////////////////////////////////////////////////

        /*
        public static bool minimizationCalibration(List<float[]> list)
        {
            //Initilization
            Matrix x = new Matrix(9, 1);
            x[0, 0] = 1;
            x[1, 0] = 0;
            x[2, 0] = 0;
            x[3, 0] = 1;
            x[4, 0] = 0;
            x[5, 0] = 1;
            x[6, 0] = 0;
            x[7, 0] = 0;
            x[8, 0] = 0;

            //Compute reference magnitude
            float M = REFR;


            //Jacobian
            try
            {
                Matrix f = new Matrix(list.Count, 1);
                for (int i = 0; i < 5; i++)
                {
                    Matrix J = new Matrix(list.Count, 9);

                    for (int j = 0; j < list.Count; j++)
                    {
                        float[] acq = list[j];

                        f[j, 0] = M - (float)Math.Sqrt((acq[0] * x[0, 0] + acq[1] * x[1, 0] + acq[2] * x[2, 0] - x[6, 0]) * (acq[0] * x[0, 0] + acq[1] * x[1, 0] + acq[2] * x[2, 0] - x[6, 0]) + (acq[0] * x[1, 0] + acq[1] * x[3, 0] + acq[2] * x[4, 0] - x[7, 0]) * (acq[0] * x[1, 0] + acq[1] * x[3, 0] + acq[2] * x[4, 0] - x[7, 0]) + (acq[0] * x[2, 0] + acq[1] * x[4, 0] + acq[2] * x[5, 0] - x[8, 0]) * (acq[0] * x[2, 0] + acq[1] * x[4, 0] + acq[2] * x[5, 0] - x[8, 0]));

                        float argSum = (float)((acq[0] * x[0, 0] + acq[1] * x[1, 0] + acq[2] * x[2, 0] - x[6, 0]) * (acq[0] * x[0, 0] + acq[1] * x[1, 0] + acq[2] * x[2, 0] - x[6, 0])) + (float)((acq[0] * x[1, 0] + acq[1] * x[3, 0] + acq[2] * x[4, 0] - x[7, 0]) * (acq[0] * x[1, 0] + acq[1] * x[3, 0] + acq[2] * x[4, 0] - x[7, 0])) + (float)((acq[0] * x[2, 0] + acq[1] * x[4, 0] + acq[2] * x[5, 0] - x[8, 0]) * (acq[0] * x[2, 0] + acq[1] * x[4, 0] + acq[2] * x[5, 0] - x[8, 0]));


                        //Jacobian Computation
                        float arg1 = (float)(acq[0] * x[0, 0] + acq[1] * x[1, 0] + acq[2] * x[2, 0] - x[6, 0]);
                        float J1 = (float)(-0.5 / Math.Sqrt(argSum) * 2 * (arg1) * acq[0]);
                        float arg2 = (float)(acq[0] * x[1, 0] + acq[1] * x[3, 0] + acq[2] * x[4, 0] - x[7, 0]);
                        float J2 = (float)(-0.5 / Math.Sqrt(argSum) * (2 * (arg1) * acq[1] + 2 * (arg2) * acq[0]));
                        arg2 = (float)(acq[0] * x[2, 0] + acq[1] * x[4, 0] + acq[2] * x[5, 0] - x[8, 0]);
                        float J3 = (float)(-0.5 / Math.Sqrt(argSum) * (2 * (arg1) * acq[2] + 2 * (arg2) * acq[0]));


                        float arg = (float)(acq[0] * x[1, 0] + acq[1] * x[3, 0] + acq[2] * x[4, 0] - x[7, 0]);
                        float J4 = (float)(-0.5 / Math.Sqrt(argSum) * 2 * (arg) * acq[1]);
                        arg2 = (float)(acq[0] * x[2, 0] + acq[1] * x[4, 0] + acq[2] * x[5, 0] - x[8, 0]);
                        float J5 = (float)(-0.5 / Math.Sqrt(argSum) * (2 * (arg) * acq[2] + 2 * (arg2) * acq[1]));

                        arg = (float)(acq[0] * x[2, 0] + acq[1] * x[4, 0] + acq[2] * x[5, 0] - x[8, 0]);
                        float J6 = (float)(-0.5 / Math.Sqrt(argSum) * 2 * (arg) * acq[2]);

                        //Offset
                        arg = (float)(acq[0] * x[0, 0] + acq[1] * x[1, 0] + acq[2] * x[2, 0] - x[6, 0]);
                        float J7 = (float)(0.5 / Math.Sqrt(argSum) * 2 * (arg));
                        arg = (float)(acq[0] * x[1, 0] + acq[1] * x[3, 0] + acq[2] * x[4, 0] - x[7, 0]);
                        float J8 = (float)(0.5 / Math.Sqrt(argSum) * 2 * (arg));
                        arg = (float)(acq[0] * x[2, 0] + acq[1] * x[4, 0] + acq[2] * x[5, 0] - x[8, 0]);
                        float J9 = (float)(0.5 / Math.Sqrt(argSum) * 2 * (arg));

                        J[j, 0] = J1;
                        J[j, 1] = J2;
                        J[j, 2] = J3;
                        J[j, 3] = J4;
                        J[j, 4] = J5;
                        J[j, 5] = J6;
                        J[j, 6] = J7;
                        J[j, 7] = J8;
                        J[j, 8] = J9;
                    }
                    //Gauss-Newton Step
                    x = x - ((Matrix.Transpose(J) * J)).Invert() * Matrix.Transpose(J) * f;
                }
                R3mtxR_[0] = (float)x[0, 0];
                R3mtxR_[1] = (float)x[1, 0];
                R3mtxR_[2] = (float)x[2, 0];
                R3mtxR_[3] = (float)x[1, 0];
                R3mtxR_[4] = (float)x[3, 0];
                R3mtxR_[5] = (float)x[4, 0];
                R3mtxR_[6] = (float)x[2, 0];
                R3mtxR_[7] = (float)x[4, 0];
                R3mtxR_[8] = (float)x[5, 0];
                OOxyz_[0] = (float)x[6, 0];
                OOxyz_[1] = (float)x[7, 0];
                OOxyz_[2] = (float)x[8, 0];
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }
        */
    }
}
