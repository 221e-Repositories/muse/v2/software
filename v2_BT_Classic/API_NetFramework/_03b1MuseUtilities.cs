﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Management;
using System.Runtime.InteropServices;
using System.Linq;
using System.Text.RegularExpressions;
using System.IO;

namespace Pit221e._03b1Muse
{
    /// <summary> _03b1MuseUtilities class. </summary>
    public static class _03b1MuseUtilities
    {
        #region DATA ENCODING / DECODING METHODS

        /// <summary> Encode SHORT data. </summary>
        /// <param name="inVal"></param>
        /// <returns> Returns an array of bytes representing the SHORT input value. </returns>
        public static byte[] EncodeShortData(short inVal)
        {
            byte[] result = BitConverter.GetBytes(inVal);
            return result;
        }

        /// <summary> Decode SHORT data. </summary>
        /// <param name="msB"></param>
        /// <param name="lsB"></param>
        /// <returns> Returns a SHORT data. </returns>
        public static short DecodeShortData(byte msB, byte lsB)
        {
            short result = (short)BitConverter.ToUInt16(new byte[2] { lsB, msB }, 0);
            return result;
        }

        /// <summary> Encode INTEGER data. </summary>
        /// <param name="inVal"></param>
        /// <returns> Returns an array of bytes representing the INTEGER input value. </returns>
        public static byte[] EncodeIntData(int inVal)
        {
            byte[] result = BitConverter.GetBytes(inVal);
            return result;
        }

        /// <summary> Decode INTEGER data. </summary>
        /// <param name="b4"></param>
        /// <param name="b3"></param>
        /// <param name="b2"></param>
        /// <param name="b1"></param>
        /// <returns> Returns an INTEGER data. </returns>
        public static int DecodeIntData(byte b4, byte b3, byte b2, byte b1)
        {
            int result = (int)BitConverter.ToUInt32(new byte[4] { b1, b2, b3, b4 }, 0);
            return result;
        }

        /// <summary> Encode FLOAT data. </summary>
        /// <param name="inVal"></param>
        /// <returns> Returns an array of bytes representing the FLOAT input value. </returns>
        public static byte[] EncodeFloatData(float inVal)
        {
            byte[] result = BitConverter.GetBytes(inVal);
            return result;
        }

        /// <summary> Decode FLOAT data. </summary>
        /// <param name="b4"></param>
        /// <param name="b3"></param>
        /// <param name="b2"></param>
        /// <param name="b1"></param>
        /// <returns> Returns a FLOAT data. </returns>
        public static float DecodeFloatData(byte b4, byte b3, byte b2, byte b1)
        {
            float result = BitConverter.ToSingle(new byte[4] { b1, b2, b3, b4 }, 0);
            return result;
        }

        #endregion

        #region CHECKSUM METHOD

        /// <summary> Compute Cyclic Redundancy Check. </summary>
        /// <param name="crc">crc</param>
        /// <param name="data">data</param>
        /// <returns> Returns UNSIGNED INTEGER crc. </returns>
        public static uint CRC32(uint crc, uint data)
        {
            int i;

            crc = crc ^ data;

            for (i = 0; i < 32; i++)
                if ((crc & 0x80000000) != 0)
                    crc = (crc << 1) ^ 0x04C11DB7; // Polynomial used in STM32
                else
                    crc = (crc << 1);

            return (crc);
        }

        #endregion

        #region HIGH RESOLUTION DATE TIME

        /// <summary> High Resolution Data time availability. </summary>
        internal static bool IsAvailable { get; private set; }

        [DllImport("Kernel32.dll", CallingConvention = CallingConvention.Winapi)]
        internal static extern void GetSystemTimePreciseAsFileTime(out long filetime);

        /// <summary> Gets current local time. </summary>
        internal static DateTime UtcNow
        {
            get
            {
                long filetime;
                try
                {
                    GetSystemTimePreciseAsFileTime(out filetime);
                    IsAvailable = true;
                }
                catch (EntryPointNotFoundException)
                {
                    // Not running on Windows 8 or higher.
                    IsAvailable = false;
                }

                if (!IsAvailable)
                    throw new InvalidOperationException("High resolution clock isn't available.");

                GetSystemTimePreciseAsFileTime(out filetime);

                return DateTime.FromFileTimeUtc(filetime);
            }
        }

        #endregion

        #region SEARCH DEVICES

        /// <summary> Allows you to retrieve a list of available devices / ports for the computer the application is running on. </summary>
        /// <returns> A list of triplet containing Name, associated COM Port and MAC Address of each device. </returns>
        public static List<Tuple<string, string, string>> SearchDevices()
        {
            // Save mac address, port and name for each device
            List<Tuple<string, string, string>> dev = new List<Tuple<string, string, string>>();

            string tmp = "";
            string dName = "";
            string dMac = "";
            string dPort = "";

            if (Environment.OSVersion.Version.Major >= 6 && Environment.OSVersion.Version.Minor >= 2)
            {
                // Search for devices
                using (var searcher1 = new ManagementObjectSearcher("root\\CIMV2",
                                                                    "SELECT Name, DeviceID " +
                                                                    "FROM Win32_PnPEntity " +
                                                                    "WHERE Caption like '%Muse_%' or " +
                                                                            "Caption like '%ed Up!%' or " +
                                                                            "Caption like '%neMEMSi%' or " +
                                                                            "Caption like '%BioMX_%'"))
                    foreach (ManagementBaseObject queryObj1 in searcher1.Get())
                    {
                        dName = queryObj1["Name"].ToString();
                        tmp = queryObj1["DeviceID"].ToString();
                        dMac = tmp.Substring(tmp.IndexOf("BLUETOOTHDEVICE_") + 16, 12);

                        using (var searcher2 = new ManagementObjectSearcher("root\\CIMV2",
                                                                            "SELECT Name " +
                                                                            "FROM Win32_PnPEntity " +
                                                                            "WHERE PNPDeviceID like '%" + dMac + "%'"))

                            foreach (ManagementBaseObject queryObj2 in searcher2.Get())
                            {
                                tmp = queryObj2["Name"].ToString();
                                if (tmp.Contains("COM"))
                                {
                                    dPort = tmp.Split(new char[] { '(', ')' }, 3)[1];
                                    int i = 0;
                                    string dMac_formatted = "";
                                    for (i = 0; i <= 8; i = i + 2)
                                        dMac_formatted = dMac_formatted + dMac.Substring(i, 2) + ":";
                                    dMac_formatted = dMac_formatted + dMac.Substring(i, 2);
                                    dev.Add(new Tuple<string,string,string>(dName, dPort, dMac_formatted));
                                }
                            }
                    }

                // Sort list
                dev = dev.OrderBy(t => t.Item1).ToList();
            }
            else
            {
                // Lettura delle porte COM
                string[] portNames = SerialPort.GetPortNames();

                // Extract and sorted ports using number
                List<int> portNumbers = new List<int>();
                for (int i = 0; i < portNames.Length; i++)
                    portNumbers.Add(Convert.ToInt16(portNames[i].Replace("COM", "")));
                portNumbers.Sort();

                for (int i = 0; i < portNumbers.Count; i++)
                    dev.Add(new Tuple<string, string, string>("COM" + portNumbers[i], "", ""));
            }

            return dev;
        }

        #endregion

        #region WRITE DATA TO FILE

        internal static void InsertHeader(string filename, string header)
        {
            var tempfile = Path.GetTempFileName();
            using (var writer = new StreamWriter(tempfile))
            using (var reader = new StreamReader(filename))
            {
                writer.WriteLine(header);
                while (!reader.EndOfStream)
                    writer.WriteLine(reader.ReadLine());
            }
            File.Copy(tempfile, filename, true);
            File.Delete(tempfile);
        }

        public static void WriteFile(StreamWriter file, string sensorName, int[] sensorConfig, int /*_03b1MuseHW.acq_type*/ opMode, int acqFreq, List<_03b1MuseMessage> data)
        {
            if (file != null)
            {
                string header = "Date: " + System.DateTime.UtcNow + "\n" + 
                                "File: " + ((FileStream)(file.BaseStream)).Name + "\n" + 
                                "Sensor: " + sensorName + "\n" + 
                                "Configuration: " + sensorConfig[0] + " dps; " + sensorConfig[1] + " g; " + sensorConfig[2] + " g (HDR); " + sensorConfig[3] + " G\n" + 
                                "Frequency: " + acqFreq + " Hz\n";

                string line = "";
                switch ((int)opMode)
                {
                    case 0: // STREAM_RAW
                        header = header + "Operating Mode: " + "STREAM_RAW" + "\n\n";
                        break;
                    case 1: // STREAM_QUATERNION
                        header = header + "Operating Mode: " + "STREAM_QUATERNION" + "\n\n";
                        header = header + "qW\tqX\tqY\tqZ\n";
                        file.WriteLine(header);
                        foreach (_03b1MuseMessage msg in data)
                        {
                            line = msg.Quaternion[0] + "\t" + msg.Quaternion[1] + "\t" + msg.Quaternion[2] + "\t" + msg.Quaternion[3];
                            line = line.Replace(',', '.');
                            file.WriteLine(line);
                        }
                        break;
                    case 2: // STREAM_RAW_AND_QUATERNION
                        header = header + "Operating Mode: " + "STREAM_RAW_AND_QUATERNION" + "\n\n";
                        header = header + "Timestamp\tGyr_X\tGyr_Y\tGyr_Z\tAcc_X\tAcc_Y\tAcc_Z\tMag_X\tMag_Y\tMag_Z\tqW\tqX\tqY\tqZ\n";
                        file.WriteLine(header);
                        foreach (_03b1MuseMessage msg in data)
                        {
                            line = msg.TimestampTX + "\t" +
                                   +msg.Gyroscope[0] + "\t" + msg.Gyroscope[1] + "\t" + msg.Gyroscope[2] + "\t" +
                                   +msg.Accelerometer[0] + "\t" + msg.Accelerometer[1] + "\t" + msg.Accelerometer[2] + "\t" +
                                   +msg.Magnetometer[0] + "\t" + msg.Magnetometer[1] + "\t" + msg.Magnetometer[2] + "\t" +
                                   +msg.Quaternion[0] + "\t" + msg.Quaternion[1] + "\t" + msg.Quaternion[2] + "\t" + msg.Quaternion[3];
                            line = line.Replace(',', '.');
                            file.WriteLine(line);
                        }
                        break;
                    case 3: // STREAM_HDR
                        header = header + "Operating Mode: " + "STREAM_HDR" + "\n\n";
                        header = header + "Timestamp\tGyr_X\tGyr_Y\tGyr_Z\tAcc_X\tAcc_Y\tAcc_Z\tHDR_Acc_X\tHDR_Acc_Y\tHDR_Acc_Z\n";
                        file.WriteLine(header);
                        foreach (_03b1MuseMessage msg in data)
                        {
                            line = msg.TimestampTX + "\t" +
                                   +msg.Gyroscope[0] + "\t" + msg.Gyroscope[1] + "\t" + msg.Gyroscope[2] + "\t" +
                                   +msg.Accelerometer[0] + "\t" + msg.Accelerometer[1] + "\t" + msg.Accelerometer[2] + "\t" +
                                   +msg.HDRAccelerometer[0] + "\t" + msg.HDRAccelerometer[1] + "\t" + msg.HDRAccelerometer[2];
                            line = line.Replace(',', '.');
                            file.WriteLine(line);
                        }
                        break;
                    case 4: // LOG_RAW
                        header = header + "Operating Mode: " + "LOG_RAW" + "\n\n";
                        break;
                    case 5: // LOG_QUATERNION
                        header = header + "Operating Mode: " + "LOG_QUATERNION" + "\n\n";
                        header = header + "qW\tqX\tqY\tqZ\n";
                        file.WriteLine(header);
                        foreach (_03b1MuseMessage msg in data)
                        {
                            line = msg.Quaternion[0] + "\t" + msg.Quaternion[1] + "\t" + msg.Quaternion[2] + "\t" + msg.Quaternion[3];
                            line = line.Replace(',', '.');
                            file.WriteLine(line);
                        }
                        break;
                    case 6: // LOG_RAW_AND_QUATERNION
                        header = header + "Operating Mode: " + "LOG_RAW_AND_QUATERNION" + "\n\n";
                        header = header + "Timestamp\tGyr_X\tGyr_Y\tGyr_Z\tAcc_X\tAcc_Y\tAcc_Z\tMag_X\tMag_Y\tMag_Z\tqW\tqX\tqY\tqZ\n";
                        file.WriteLine(header);
                        foreach (_03b1MuseMessage msg in data)
                        {
                            line = msg.TimestampTX + "\t" +
                                   +msg.Gyroscope[0] + "\t" + msg.Gyroscope[1] + "\t" + msg.Gyroscope[2] + "\t" +
                                   +msg.Accelerometer[0] + "\t" + msg.Accelerometer[1] + "\t" + msg.Accelerometer[2] + "\t" +
                                   +msg.Magnetometer[0] + "\t" + msg.Magnetometer[1] + "\t" + msg.Magnetometer[2] + "\t" +
                                   +msg.Quaternion[0] + "\t" + msg.Quaternion[1] + "\t" + msg.Quaternion[2] + "\t" + msg.Quaternion[3];
                            line = line.Replace(',', '.');
                            file.WriteLine(line);
                        }
                        break;
                    case 7: // LOG_HDR
                        header = header + "Operating Mode: " + "LOG_HDR" + "\n\n";
                        header = header + "Timestamp\tGyr_X\tGyr_Y\tGyr_Z\tAcc_X\tAcc_Y\tAcc_Z\tHDR_Acc_X\tHDR_Acc_Y\tHDR_Acc_Z\n";
                        file.WriteLine(header);
                        foreach (_03b1MuseMessage msg in data)
                        {
                            line = msg.TimestampTX + "\t" +
                                   +msg.Gyroscope[0] + "\t" + msg.Gyroscope[1] + "\t" + msg.Gyroscope[2] + "\t" +
                                   +msg.Accelerometer[0] + "\t" + msg.Accelerometer[1] + "\t" + msg.Accelerometer[2] + "\t" +
                                   +msg.HDRAccelerometer[0] + "\t" + msg.HDRAccelerometer[1] + "\t" + msg.HDRAccelerometer[2];
                            line = line.Replace(',', '.');
                            file.WriteLine(line);
                        }
                        break;
                    case 8: // LOG_HIGH_RES
                        header = header + "Operating Mode: " + "LOG_HIGH_RES" + "\n\n";
                        header = header + "Timestamp\tGyr_X\tGyr_Y\tGyr_Z\tAcc_X\tAcc_Y\tAcc_Z\tMag_X\tMag_Y\tMag_Z\tqW\tqX\tqY\tqZ\n";
                        file.WriteLine(header);
                        foreach (_03b1MuseMessage msg in data)
                        {
                            line = msg.TimestampTX + "\t" +
                                   +msg.Gyroscope[0] + "\t" + msg.Gyroscope[1] + "\t" + msg.Gyroscope[2] + "\t" +
                                   +msg.Accelerometer[0] + "\t" + msg.Accelerometer[1] + "\t" + msg.Accelerometer[2] + "\t" +
                                   +msg.Magnetometer[0] + "\t" + msg.Magnetometer[1] + "\t" + msg.Magnetometer[2] + "\t" +
                                   +msg.Quaternion[0] + "\t" + msg.Quaternion[1] + "\t" + msg.Quaternion[2] + "\t" + msg.Quaternion[3];
                            line = line.Replace(',', '.');
                            file.WriteLine(line);
                        }
                        break;
                    default:
                        throw new ArgumentException("Invalid acquisition type");
                }
            }
            else
                throw new ArgumentException("Invalide file");
        }

        #endregion
    }
}
