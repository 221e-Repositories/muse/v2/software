﻿//-----------------------------------------------------------------
//
// 221e srl CONFIDENTIAL
// 
// File Name:  _03b1SerialConnection.cs
// File Type:  Visual C# Source file
// Author:     Roberto Bortoletto
// Company:    221e srl
//             Copyright (c) 2017 All Rights Reserved
//-----------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Timers;

using System.Threading;
using System.Diagnostics;
using System.IO;

namespace Pit221e._03b1Muse
{
    public struct _03b1MuseFileHeader
    {
        private string fileVersion_;
        public string VERSION
        {
            get { return fileVersion_; }
            set { fileVersion_ = value; }
        }

        // private string devVersion_;
        // public string DEVICE
        // {
        //     get { return devVersion_; }
        //     set { devVersion_ = value; }
        // }

        private string firmVersion_;
        public string FIRMWARE
        {
            get { return firmVersion_; }
            set { firmVersion_ = value; }
        }

        private int[] devConfig_;
        public int[] CONFIGURATION
        {
            get { return devConfig_; }
            set { devConfig_ = value; }
        }

        private float[] devCalib_;
        public float[] CALIBRATION
        {
            get { return devCalib_; }
            set { devCalib_ = value; }
        }

        private string acqMode_;
        public string MODE
        {
            get { return acqMode_; }
            set { acqMode_ = value; }
        }

        private int acqFreq_;
        public int FREQUENCY
        {
            get { return acqFreq_; }
            set { acqFreq_ = value; }
        }

        private string[] acqChannels_;
        public string[] CHANNELS
        {
            get { return acqChannels_; }
            set { acqChannels_ = value; }
        }

        private string dataEncoding_;
        public string DATA
        {
            get { return dataEncoding_; }
            set { dataEncoding_ = value; }
        }

        public override string ToString()
        {
            return "VERSION\t" + this.VERSION + "\n" +
                   // "DEVICE\t" + this.DEVICE + "\n" +
                   "FIRMWARE\t" + this.FIRMWARE + "\n" +
                   "CONFIGURATION\t" + string.Join("\t", this.CONFIGURATION) + "\n" +
                   "CALIBRATION\t" + string.Join("\t", this.CALIBRATION) + "\n" +
                   "MODE\t" + this.MODE + "\n" +
                   "FREQUENCY\t" + this.FREQUENCY + "\n" +
                   "CHANNELS\t" + string.Join("\t", this.CHANNELS) + "\n" +
                   "DATA\t" + this.DATA;
        }
    }

    // Event handlers
    // public delegate void _03b1NewLogSample(object sender, int value);

    /// <summary> _03b1MuseSerialConnection class. </summary>
    /// Use this class to establish an event-driven I/O serial communication over Bluetooth Classic with a Muse device.
    public class _03b1MuseSerialConnection
    {
        private SerialPort sp_;             // SerialPort object
        private bool spStatus_;             // True: is connected; False otherwise
        private bool spTXstatus_;           // True: is transmitting; False otherwise
        private bool spTSstatus_;           // True: is time sync drift / offset computation; False otherwise

        private bool tsMode_;               // True: time-sync mode; False otherwise
        private float tDrift_;              // Time-sync drift
        private double[] tOffset_;            // Time-sync offset {sec, millisec}
        
        private bool enableBTcmd_;          // Enable / Disable Bluetooth command mode

        private _03b1MuseMessage spDevMsg_;            // Current message
        private List<_03b1MuseMessage> spDevMsgList_;  // Acquisition message list

        private int packetDim_;             // Message dimension [byte]
        private bool firstPacket_;          // First received message
        private int lastReceivedPacket_;    // Last received packet
        private int acquiredPackets_;
        private int lastMsgIdx_;

        private bool packetReceived_;

        private bool acqLog_;               // acqLog = false;
        private bool singlePacket_;
        private int numOfLogPackets_;       // numberOfLogPackets = 0;

        private System.Timers.Timer spTimer_;           // Timer
        private DateTime previousTime_;

        // public event _03b1NewLogSample NewLogSample;    // New log sample event

        private _03b1MuseFileHeader mfh_;
        private string tmpPath_;
        private StreamWriter streamWriter_;

        #region CONSTRUCTORS

        /// <summary> Default Constructor. Initializes a new instance of the class with predefined serial communication configuration. </summary>
        /// <param name="port"> The port with which we're communicating through, i.e; COM1, COM2, etc. </param>
        /// <param name="syncEnabled"> Enable/Disable Time-sync mode for multiple device synchronization. </param>
        public _03b1MuseSerialConnection(string port, bool syncEnabled = false)
        {
            // Update connection and transmission status
            spStatus_ = false;
            spTXstatus_ = false;
            tsMode_ = false;

            // Serial connection
            try
            {
                Console.WriteLine("***********************************************");
                Console.WriteLine("Connecting Device... - {0}", port);
                // Create a SerialPort object
                sp_ = new SerialPort(port, 115200, Parity.None, 8, StopBits.One);
                sp_.DataReceived += new SerialDataReceivedEventHandler(DataReceivedHandler);

                // If serial port is already open, close it.
                if (sp_.IsOpen == true)
                    sp_.Close();

                // Open Serial Port 
                while (sp_.IsOpen == false)
                    sp_.Open();
            }
            catch (Exception)
            {
                if (sp_.IsOpen == true)
                    sp_.Close();

                while (sp_.IsOpen == false)
                    sp_.Open();
            }

            spStatus_ = true;
            packetDim_ = _03b1MuseHW.RAW_DATA_MESSAGE_SIZE;

            // Flush I/O Buffer
            sp_.DiscardInBuffer();
            sp_.DiscardOutBuffer();

            // Setup device data
            spDevMsg_ = new _03b1MuseMessage();
            spDevMsgList_ = new List<_03b1MuseMessage>();

            // packetDim_ = 52;
            firstPacket_ = true;
            previousTime_ = new DateTime();
            lastMsgIdx_ = 0;
            acquiredPackets_ = 0;
            lastReceivedPacket_ = 0;
            packetReceived_ = false;

            // Setup timeout
            sp_.ReadTimeout = 2000;
            sp_.WriteTimeout = 2000;

            // Timer init
            spTimer_ = new System.Timers.Timer(1500);
            spTimer_.Elapsed += new ElapsedEventHandler(PacketKO);

            tOffset_ = new double[2];
            tOffset_[0] = 0; tOffset_[1] = 0;

            sp_.Write(_03b1MuseHW.ExitTimeSyncMode);

            // Time sync setup if option enabled
            if (syncEnabled == true)
            {
                // Compute and set clock drift only if necessary
                float drift = this.GetClockDrift();
                Console.WriteLine(drift);
                // if (drift < 0.9999 || drift > 1.0001 || drift == 1.0)    
                // if (drift < 0.999 || drift > 1.001 || drift == 1.0)    
                // if (drift < 0.9985 || drift > 1.0015 || drift == 1.0)       
                // if (drift < 0.998 || drift > 1.002 || drift == 1.0)       
                if (drift < 0.995 || drift > 1.005 || drift == 1.0)       
                {
                    drift = this.ComputeClockDrift(400);
                    if (drift >= 0.995 && drift <= 1.005)
                    {
                        this.SetClockDrift(drift);
                        System.Threading.Thread.Sleep(150);
                    }
                    // else
                        // throw new ArgumentException("Clock drift estimation failed!");
                }
                tDrift_ = drift;

                // Always compute and set clock offset 
                double o = this.ComputeClockOffset(5);
                System.Threading.Thread.Sleep(150);
                UInt32 offsetSec = (UInt32)((-o) / ((double)1000));
                UInt32 offsetMillis = (UInt32)((-o) % 1000);
                this.SetClockOffset(offsetSec, offsetMillis);
                System.Threading.Thread.Sleep(150);

                // Save only offset with second resolution for internal application
                tOffset_[0] = offsetSec;
                tOffset_[1] = offsetMillis;

                tsMode_ = true;
            }
        }

        // <summary> Constructor. Initializes a new instance of the class with specified serial connection parameters. </summary>
        // <param name="port"> The port with which we're communicating through, i.e; COM1, COM2, etc. </param>
        // <param name="baudRate"> A measure of the speed of serial communication, roughly equivalent to bits per second. </param>
        // <param name="parity"> The even or odd quality of the number of 1's or 0's in a binary code, often used to determine the integrity of data especially after transmission. </param>
        // <param name="dataBits"> The number of bits used to represent one character of data. </param>
        // <param name="stopBits"> A bit that signals the end of a transmission unit. </param>
        // <param name="syncEnabled"> Enable/Disable Time-sync mode. </param>
        //public _03b1MuseSerialConnection(string port, int baudRate, Parity parity, int dataBits, StopBits stopBits, bool syncEnabled = false)
        //{
        //    // Update connection and transmission status
        //    spStatus_ = false;
        //    spTXstatus_ = false;
        //    tsMode_ = false;

        //    // Serial connection
        //    try
        //    {
        //        // Create a SerialPort object
        //        sp_ = new SerialPort(port, baudRate, parity, dataBits, stopBits);
        //        sp_.DataReceived += new SerialDataReceivedEventHandler(DataReceivedHandler);

        //        // If serial port is already open, close it.
        //        if (sp_.IsOpen == true)
        //            sp_.Close();

        //        // Open Serial Port 
        //        while (sp_.IsOpen == false)
        //            sp_.Open();
        //    }
        //    catch (Exception)
        //    {
        //        if (sp_.IsOpen == true)
        //            sp_.Close();

        //        while (sp_.IsOpen == false)
        //            sp_.Open();
        //    }

        //    spStatus_ = true;
        //    packetDim_ = _03b1MuseHW.RAW_DATA_MESSAGE_SIZE;

        //    // Flush I/O Buffer
        //    sp_.DiscardInBuffer();
        //    sp_.DiscardOutBuffer();

        //    // Setup device data
        //    spDevMsg_ = new _03b1MuseMessage();
        //    spDevMsgList_ = new List<_03b1MuseMessage>();
            
        //    // packetDim_ = 52;
        //    firstPacket_ = true;
        //    previousTime_ = new DateTime();
        //    lastMsgIdx_ = 0;
        //    acquiredPackets_ = 0;
        //    lastReceivedPacket_ = 0;

        //    // Setup timeout
        //    sp_.ReadTimeout = 2000;
        //    sp_.WriteTimeout = 2000;

        //    // Timer init
        //    spTimer_ = new System.Timers.Timer(1500);
        //    spTimer_.Elapsed += new ElapsedEventHandler(PacketKO);

        //    tOffset_ = new double[2];
        //    tOffset_[0] = 0; tOffset_[1] = 0;

        //    // Time sync setup if option enabled
        //    if (syncEnabled == true)
        //    {
        //        // Compute and set clock drift only if necessary
        //        float drift = this.GetClockDrift();
        //        if (drift <= 0.9 || drift >= 1.1 || drift == 1.0)
        //        {
        //            drift = this.ComputeClockDrift(300);
        //            if (drift > 0.9 && drift < 1.1)
        //            {
        //                this.SetClockDrift(drift);
        //                System.Threading.Thread.Sleep(500);
        //            }
        //            else
        //                throw new ArgumentException("Clock drift estimation failed!");
        //        }
        //        tDrift_ = drift;

        //        // Always compute and set clock offset 
        //        double o = this.ComputeClockOffset(5);
        //        UInt32 offsetSec = (UInt32)((-o) / ((double)1000));
        //        UInt32 offsetMillis = (UInt32)((-o) % 1000);
        //        this.SetClockOffset(offsetSec, offsetMillis);
        //        System.Threading.Thread.Sleep(500);

        //        // Save only offset with second resolution for internal application
        //        tOffset_[0] = offsetSec;
        //        tOffset_[1] = offsetMillis;

        //        tsMode_ = true;
        //    }
        //}

        #endregion

        #region PROPERTIES

        /// <summary> Get serial connection status. </summary>
        /// \hideinitializer
        public bool Status
        {
            get { return spStatus_; }
        }

        /// <summary> Get device transmission status. </summary>
        /// \hideinitializer
        public bool StatusTX
        {
            get { return spTXstatus_; }
        }

        /// <summary> Check if time-sync connection mode is enabled. </summary>
        /// \hideinitializer
        public bool SyncModeEnabled
        {
            get { return tsMode_; }
        }

        /// <summary> Get device clock drift. </summary>
        /// \hideinitializer
        public float ClockDrift
        {
            get { return tDrift_; }
        }

        /// <summary> Get device clock offset. </summary>
        /// \hideinitializer
        public double[] ClockOffset
        {
            get { return tOffset_; }
        }

        /// <summary> Get the current message as _03b1MuseMessage object. </summary>
        /// \hideinitializer
        public _03b1MuseMessage Message
        {
            get { return spDevMsg_; }
        }

        /// <summary> Get the list of available messages as a list of _03b1MuseMessage objects. </summary>
        /// \hideinitializer
        public List<_03b1MuseMessage> MessageList
        {
            get { return spDevMsgList_; }
        }

        public int NumOfLogPackets
        {
            get { return numOfLogPackets_ / 512; }
        }
        #endregion

        #region _03b1hw COMMUNICATION PROTOCOL

        #region CALIBRATION COMMANDS

        /// <summary> Gyroscope offset calibration procedure. </summary>
        /// \hideinitializer
        public float[] CalibrateGyroscope()
        {
            // Flush buffer
            sp_.DiscardInBuffer();
            sp_.DiscardOutBuffer();
            
            // Send message
            sp_.Write(_03b1MuseHW.CalibrateGyroscope);

            // ... WAIT FOR CALIBRATION ... about 20 seconds
            Console.WriteLine("Wait for calibration ...");
            System.Threading.Thread.Sleep((int)_03b1MuseHW.GYRO_CALIB_TIME + 2000);

            // Send message
            sp_.Write(_03b1MuseHW.GetGyroscopeOffset);
            while (sp_.BytesToRead < 14) ;

            // Read message
            byte[] buffer = new byte[14];
            int msg = sp_.Read(buffer, 0, 14);

            // Decode message
            float[] result = null;
            if (buffer[0] == 'G' && buffer[1] == 'B')
            {
                result = new float[3];
                for (int i = 0; i < 3; i++)
                    result[i] = _03b1MuseUtilities.DecodeFloatData(buffer[2 + 4 * i], buffer[3 + 4 * i], buffer[4 + 4 * i], buffer[5 + 4 * i]);
            }

            return result;
        }

        /// <summary> Gyroscope offset calibration command [get]. </summary>
        /// \hideinitializer
        public float[] GetGyroscopeOffset()
        {
            // Flush buffer
            sp_.DiscardInBuffer();
            sp_.DiscardOutBuffer();

            // Send message
            sp_.Write(_03b1MuseHW.GetGyroscopeOffset);
            while (sp_.BytesToRead < 14) ;

            // Read message
            byte[] buffer = new byte[14];
            int msg = sp_.Read(buffer, 0, 14);

            // Decode message
            float[] result = null;
            if (buffer[0] == 'G' && buffer[1] == 'B')
            {
                result = new float[3];
                for (int i = 0; i < 3; i++)
                    result[i] = _03b1MuseUtilities.DecodeFloatData(buffer[2 + 4 * i], buffer[3 + 4 * i], buffer[4 + 4 * i], buffer[5 + 4 * i]);
            }

            return result;
        }

        /// <summary> Accelerometer calibration procedure </summary>
        /// <param name="data"> Accelerometer data </param>
        /// <returns> Accelerometer calibration parameters </returns>
        public float[] CalibrateAccelerometer()
        {
            _03b1MuseCalibration myCalib = new _03b1MuseCalibration(1000, null);
            float[] result = null;
            return result;
        }

        /// <summary> Send Accelerometer calibration parameters </summary>
        /// <param name="parameters"></param>
        public void SetAccelerometerCalibParams(float[] parameters)
        {
            // Flush buffer
            sp_.DiscardInBuffer();
            sp_.DiscardOutBuffer();

            uint[] data = new uint[12];
            Buffer.BlockCopy(parameters, 0, data, 0, parameters.Length * 4);

            // Send message
            sp_.Write(_03b1MuseHW.SetAccelerometerCalibParams);

            // Set calibration parameters
            byte[] data2 = new byte[48];
            for (int i = 0; i < 12; i++)
            {
                for (int j = 0; j < 4; j++)
                    data2[i * 4 + j] = (byte)(data[i] >> ((3 - j) * 8) & 0x000000FF);
            }

            // Send message
            sp_.Write(data2, 0, 48);
            System.Threading.Thread.Sleep(150);
        }

        /// <summary> Retrieve Accelerometer calibration parameters </summary>
        /// <returns> Accelerometer calibration parameters </returns>
        public float[] GetAccelerometerCalibParams()
        {
            // Flush buffer
            sp_.DiscardInBuffer();
            sp_.DiscardOutBuffer();

            // Send message
            sp_.Write(_03b1MuseHW.GetAccelerometerCalibParams);
            while (sp_.BytesToRead < 50) ;

            // Read message
            byte[] buffer = new byte[50];
            int msg = sp_.Read(buffer, 0, 50);

            // Decode message
            float[] result = null;
            if (buffer[0] == 'C' && buffer[1] == 'C')
            {
                result = new float[12];
                for (int i = 0; i < 12; i++)
                    result[i] = _03b1MuseUtilities.DecodeFloatData(buffer[2 + i * 4], buffer[3 + i * 4], buffer[4 + i * 4], buffer[5 + i * 4]);
            }

            return result;
        }

        /// <summary> Magnetometer calibration procedure </summary>
        /// <param name="data"> Magnetometer data </param>
        /// <returns> Magnetometer calibration parameters </returns>
        public float[] CalibrateMagnetometer(bool embeddedEnabled = true)
        {
            float[] result = null;
            if (embeddedEnabled == false)
            {
                // Manage standalone calibration procedure
                throw new Exception("Argument Exception");
            }
            else
            {
                // Manage embedded calibration procedure
                bool res = EmbeddedMagnetometerCalibration();
                if (res == true)
                {
                    this.StoreConfigurationParams();
                    result = this.GetMagnetometerCalibParams();
                }
            }
            return result;
        }

        // Embedded magnetometer calibration procedure
        private bool EmbeddedMagnetometerCalibration()
        {
            bool result = false;

            // Flush buffer
            sp_.DiscardInBuffer();
            sp_.DiscardOutBuffer();

            // Send message
            sp_.Write(_03b1MuseHW.EmbeddedMagnetometerCalibration);

            DateTime t = DateTime.Now;
            while ((t - DateTime.Now).TotalSeconds < 40 && sp_.BytesToRead < 8) ;

            if (sp_.BytesToRead >= 8)
            {
                // Read message
                byte[] buffer = new byte[sp_.BytesToRead];
                sp_.Read(buffer, 0, sp_.BytesToRead);

                // Decode message
                if (buffer[0] == 'C' && buffer[1] == 'A' && buffer[2] == 'L' && buffer[3] == 'S' && buffer[4] == 'T' && buffer[5] == 'A' && buffer[6] == 'T')
                {
                    if (buffer[7] == 1)
                        result = true;
                }
            }
            return result;
        }

        /// <summary> Send Magnetometer calibration parameters </summary>
        /// <param name="parameters"></param>
        public void SetMagnetometerCalibParams(float[] parameters)
        {
            // Flush buffer
            sp_.DiscardInBuffer();
            sp_.DiscardOutBuffer();

            uint[] data = new uint[12];
            Buffer.BlockCopy(parameters, 0, data, 0, parameters.Length * 4);

            // Send message
            sp_.Write(_03b1MuseHW.SetMagnetometerCalibParams);

            // Set calibration parameters
            byte[] data2 = new byte[48];
            for (int i = 0; i < 12; i++)
            {
                for (int j = 0; j < 4; j++)
                    data2[i * 4 + j] = (byte)(data[i] >> ((3 - j) * 8) & 0x000000FF);
            }

            // Send message
            sp_.Write(data2, 0, 48);
            System.Threading.Thread.Sleep(150);
        }

        /// <summary> Retrieve Magnetometer calibration parameters </summary>
        /// <returns> Magnetometer calibration parameters </returns>
        public float[] GetMagnetometerCalibParams()
        {
            // Flush buffer
            sp_.DiscardInBuffer();
            sp_.DiscardOutBuffer();

            // Send message
            sp_.Write(_03b1MuseHW.GetMagnetometerCalibParams);
            while (sp_.BytesToRead < 50) ;

            // Read message
            byte[] buffer = new byte[50];
            int msg = sp_.Read(buffer, 0, 50);

            // Decode message
            float[] result = null;
            if (buffer[0] == 'C' && buffer[1] == 'C')
            {
                result = new float[12];
                for (int i = 0; i < 12; i++)
                    result[i] = _03b1MuseUtilities.DecodeFloatData(buffer[2 + i * 4], buffer[3 + i * 4], buffer[4 + i * 4], buffer[5 + i * 4]);
            }

            return result;
        }

        /// <summary> Magnetic Incliantion angle computation procedure. </summary>
        /// \hideinitializer
        public float ComputeMagneticInclinationAngle()
        {
            // Flush buffer
            sp_.DiscardInBuffer();
            sp_.DiscardOutBuffer();

            // Send message
            sp_.Write(_03b1MuseHW.ComputeMagneticInclinationAngle);

            // ... WAIT FOR CALIBRATION ... about 20 seconds
            System.Threading.Thread.Sleep((int)_03b1MuseHW.DIP_CALIB_TIME + 2000);

            // Send message
            sp_.Write(_03b1MuseHW.GetMagneticInclinationAngle);
            while (sp_.BytesToRead < 6) ;

            // Read message
            byte[] buffer = new byte[6];
            int msg = sp_.Read(buffer, 0, 6);

            // Decode message
            float result = 0;
            if (buffer[0] == 'D' && buffer[1] == 'A')
                result = _03b1MuseUtilities.DecodeFloatData(buffer[2], buffer[3], buffer[4], buffer[5]);
            
            return result;
        }

        /// <summary> Retrieve Magnetic Inclination angle. </summary>
        /// \hideinitializer
        public float GetMagneticInclinationAngle()
        {
            // Flush buffer
            sp_.DiscardInBuffer();
            sp_.DiscardOutBuffer();

            // Send message
            sp_.Write(_03b1MuseHW.GetMagneticInclinationAngle);
            while (sp_.BytesToRead < 6) ;

            // Read message
            byte[] buffer = new byte[6];
            int msg = sp_.Read(buffer, 0, 6);

            // Decode message
            float result = 0;
            if (buffer[0] == 'D' && buffer[1] == 'A')
                result = _03b1MuseUtilities.DecodeFloatData(buffer[2], buffer[3], buffer[4], buffer[5]);

            return result;
        }

        /// <summary> Set Gyroscope full scale. </summary>
        /// <param name="dpsValue"> Gyroscope full scale [dps]. </param>
        /// The available full scales include: 500, 1000, 2000 or 4000 dps.
        // public void SetGyroscopeFullScale(_03b1MuseHW.Gyr_fs dpsValue)
        public void SetGyroscopeFullScale(int key)
        {
            // Flush buffer
            sp_.DiscardInBuffer();
            sp_.DiscardOutBuffer();

            Dictionary<int, int> tmp = _03b1MuseHW.GetGyrFS();
            int val = 0;
            bool res = tmp.TryGetValue(key, out val);

            // Set parameter
            if (res == true)
            {
                string cmd = string.Format(_03b1MuseHW.SetGyroscopeFullScale, key);

                // Send message
                sp_.Write(cmd);
                System.Threading.Thread.Sleep(150);
            }
        }

        /// <summary> Get Gyroscope full scale. </summary>
        /// <param name="enableDecoding"> Enable / Disable parameters decoding. Default is ENABLED. </param>
        /// <returns> Current gyroscope full scale. </returns>
        public int GetGyroscopeFullScale(bool enableDecoding = true)
        {
            int[] parameters = GetConfigurationParams(enableDecoding);
            return parameters[0];
        }

        /// <summary> Sets Accelerometer full scale. </summary>
        /// <param name="gValue"> Accelerometer full scale [g]. </param>
        /// The available full scales include: 2, 4, 6, 8 or 16 g.
        // public void SetAccelerometerFullScale(_03b1MuseHW.Acc_fs gValue)
        public void SetAccelerometerFullScale(int key)
        {
            // Flush buffer
            sp_.DiscardInBuffer();
            sp_.DiscardOutBuffer();

            Dictionary<int, int> tmp = _03b1MuseHW.GetAxlFS();
            int val = 0;
            bool res = tmp.TryGetValue(key, out val);

            // Set parameter
            if (res == true)
            {
                string cmd = string.Format(_03b1MuseHW.SetAccelerometerFullScale, key);

                // Send message
                sp_.Write(cmd);
                System.Threading.Thread.Sleep(150);
            }
        }

        /// <summary> Get Accelerometer full scale. </summary>
        /// <param name="enableDecoding"> Enable / Disable parameters decoding. Default is ENABLED. </param>
        /// <returns> Current accelerometer full scale. </returns>
        public int GetAccelerometerFullScale(bool enableDecoding = true)
        {
            int[] parameters = GetConfigurationParams(enableDecoding);
            return parameters[1];
        }

        /// <summary> Sets HDR Accelerometer Full Scale. </summary>
        /// <param name="gHDRValue"> HDR Accelerometer Full Scale [g]. </param>
        /// The available full scales include: 100, 200 or 400 g.
        // public void SetAccelerometerHDRFullScale(_03b1MuseHW.Acc_HDR_fs gHDRValue)
        public void SetAccelerometerHDRFullScale(int key)
        {
            // Flush buffer
            sp_.DiscardInBuffer();
            sp_.DiscardOutBuffer();

            Dictionary<int, int> tmp = _03b1MuseHW.GetAxlHDRFS();
            int val = 0;
            bool res = tmp.TryGetValue(key, out val);

            // Set parameter
            if (res == true)
            {
                string cmd = string.Format(_03b1MuseHW.SetAccelerometerHDRFullScale, key);

                // Send message
                sp_.Write(cmd);
                System.Threading.Thread.Sleep(150);
            }
        }

        /// <summary> Gets HDR Accelerometer Full Scale. </summary>
        public int GetAccelerometerHDRFullScale(bool enableDecoding = true)
        {
            int[] parameters = GetConfigurationParams(enableDecoding);
            return parameters[3];
        }

        /// <summary> Sets Magnetometer Full Scale. </summary>
        /// <param name="gaussValue"> Magnetometer Full Scale [Gauss]. </param>
        /// The available full scales include: 2, 4, 8 or 12 G.
        // public void SetMagnetometerFullScale(_03b1MuseHW.Mag_fs gaussValue)
        public void SetMagnetometerFullScale(int key)
        {
            // Flush buffer
            sp_.DiscardInBuffer();
            sp_.DiscardOutBuffer();

            Dictionary<int, int> tmp = _03b1MuseHW.GetMagFS();
            int val = 0;
            bool res = tmp.TryGetValue(key, out val);

            // Set parameter
            if (res == true)
            {
                string cmd = string.Format(_03b1MuseHW.SetMagnetometerFullScale, key);

                // Send message
                sp_.Write(cmd);
                System.Threading.Thread.Sleep(150);
            }
        }

        /// <summary> Gets Magnetometer Full Scale. </summary>
        public int GetMagnetometerFullScale(bool enableDecoding = true)
        {
            int[] parameters = GetConfigurationParams(enableDecoding);
            return parameters[2];
        }

        // <summary> Set sensor body pose index. </summary>
        // <param name="bodyPose"> Sensor body pose index to be set (i.e., ranging from 0 to 255, default: 0). </param>
        /*
        public void SetSensorBodyPose(int bodyPose)
        {
            // Flush buffer
            sp_.DiscardOutBuffer();

            // Send message
            sp_.Write(string.Format(_03b1MuseHW.SetSensorBodyPose, bodyPose));
            System.Threading.Thread.Sleep(150);
        }
        */

        // <summary> Get sensor body pose index. </summary>
        /*
        public int GetSensorBodyPose()
        {
            int result = -1;

            // Flush buffer
            sp_.DiscardInBuffer();
            sp_.DiscardOutBuffer();

            // Send message
            sp_.Write(_03b1MuseHW.GetSensorBodyPose);
            while (sp_.BytesToRead < 3) ;

            // Read message
            byte[] buffer = new byte[3];
            sp_.Read(buffer, 0, 3);
            if (buffer[0] == 'B' && buffer[1] == 'P')
                result = buffer[2];

            return result;
        }
        */
       
        /// <summary> Enable / Disable sensor sleep mode. </summary>
        /// <param name="mode"> True = enable; False = disable. </param>
        public void SetSensorSleepMode(bool mode)
        {
            // Flush buffer
            sp_.DiscardOutBuffer();

            byte[] sleepMode = BitConverter.GetBytes(mode);

            // Send message
            sp_.Write(_03b1MuseHW.SetSensorSleepMode);      // Header (command)
            sp_.Write(sleepMode, 0, 1);                     // Parameters
            sp_.Write("!");                                 // Footer

            System.Threading.Thread.Sleep(150);
        }

        /// <summary> Get sensor sleep mode. </summary>
        /// \hideinitializer
        public bool GetSensorSleepMode()
        {
            bool result = false; // default -> sleep mode disabled!

            // Flush buffer
            sp_.DiscardInBuffer();
            sp_.DiscardOutBuffer();

            // Send message
            sp_.Write(_03b1MuseHW.GetSensorSleepMode);
            while (sp_.BytesToRead < 3) ;

            // Read message
            byte[] buffer = new byte[3];
            int msg = sp_.Read(buffer, 0, 3);
            
            // Decode message
            if (buffer[0] == 'S' && buffer[1] == 'M')
                result = Convert.ToBoolean(buffer[2]);

            return result;
        }

        /// <summary> Set Device Configuration Parameters. </summary>
        /// <param name="dpsValue"> Gyroscope full scale </param>
        /// <param name="gValue"> Accelerometer full scale </param>
        /// <param name="gaussValue"> Magnetometer full scale </param>
        /// <param name="gHDRValue"> Accelerometer HDR full scale </param>
        /// <param name="lgMode"> Log mode </param>
        /// <param name="lgFreq"> Log frequency </param>
        /// The parameters are provided in the following order: gyroscope, accelerometer, magnetometer and HDR accelerometer full scales followed by log mode and frequency if set.
        // public void SetConfigurationParams(_03b1MuseHW.Gyr_fs dpsValue, _03b1MuseHW.Acc_fs gValue, _03b1MuseHW.Mag_fs gaussValue, _03b1MuseHW.Acc_HDR_fs gHDRValue, _03b1MuseHW.Log_mode lgMode, int lgFreq)
        public void SetConfigurationParams(int gyrFSKey, int axlFSKey, int magFSKey, int axlHDRFSKey, int logModeKey, int logFreq)
        {
            SetGyroscopeFullScale(gyrFSKey);            // Gyroscope FS
            SetAccelerometerFullScale(axlFSKey);          // Accelerometer FS
            SetMagnetometerFullScale(magFSKey);       // Magnetometer FS
            SetAccelerometerHDRFullScale(axlHDRFSKey);    // Accelerometer HDR FS

            SetupLog(logModeKey, logFreq);                   // Log Mode and Frequency
        }

        /// <summary> Retrieve Device Configuration Parameters. </summary>
        /// <param name="enableDecoding"> Enable / Disable parameters decoding procedure. Default is ENABLED. </param>
        /// <returns> An array of integers representing the current device configuration. </returns>
        /// The parameters are provided in the following order: gyroscope, accelerometer, magnetometer and HDR accelerometer full scales followed by log mode and frequency if set.
        public int[] GetConfigurationParams(bool enableDecoding = true)
        {
            // Flush buffer
            sp_.DiscardInBuffer();
            sp_.DiscardOutBuffer();

            // Send message
            sp_.Write(_03b1MuseHW.GetConfigurationParams);
            while (sp_.BytesToRead < 8) ;

            // Read message
            byte[] buffer = new byte[8];
            int msg = sp_.Read(buffer, 0, 8);

            // Decode message
            int[] result = null;
            if (buffer[0] == 'P' && buffer[1] == 'P')
            {
                result = new int[6];

                // Copy all the parameters from serial buffer, which are also the output, if no decoding is required
                for (int k = 0; k < 6; k++)
                    result[k] = buffer[2 + k];

                // By default the decoded parameters are provided to the user
                if (enableDecoding == true)
                {
                    result[0] = _03b1MuseHW.gyr_fs_value[result[0]];        // Gyroscope FS
                    result[1] = _03b1MuseHW.acc_fs_value[result[1]];        // Accelerometer FS
                    result[2] = _03b1MuseHW.mag_fs_value[result[2]];        // Magnetometer FS
                    result[3] = _03b1MuseHW.acc_hdr_fs_value[result[3]];    // Accelerometer HDR FS
                }
                // Positions 4 and 5 contains log mode and log frequency, respectively
            }
            return result;
        }

        /// <summary> Stores Configuration Parameters permanently into the non-volatile memory of the device. </summary>
        /// \hideinitializer
        public void StoreConfigurationParams()
        {
            // Flush buffer
            sp_.DiscardInBuffer();
            sp_.DiscardOutBuffer();

            // Send message
            sp_.Write(_03b1MuseHW.StoreConfigurationParameters);
            System.Threading.Thread.Sleep(150);
        }


        #endregion

        #region STREAMING COMMANDS

        // <summary> Starts raw data streaming at a specified frequency </summary>
        // \hideinitializer
        //public void StartRawDataStreaming(int frequency)
        //{
        //    // Flush buffer
        //    sp_.DiscardInBuffer();
        //    sp_.DiscardOutBuffer();

        //    if (frequency > 0 && frequency <= _03b1MuseHW.MAX_STREAM_FREQUENCY)
        //    {
        //        // Send message
        //        sp_.Write(string.Format(_03b1MuseHW.StreamRawData, frequency));
        //        spTXstatus_ = true;

        //        // Setup packet dimension
        //        packetDim_ = _03b1MuseHW.RAW_DATA_MESSAGE_SIZE;
        //        sp_.ReceivedBytesThreshold = packetDim_;

        //        System.Threading.Thread.Sleep(150);
        //    }
        //    else
        //        throw new ArgumentException("{0} is not a valid argument", frequency.ToString());

        //}

        /// <summary> Starts HDR data streaming at a specified frequency </summary>
        /// \hideinitializer
        public void StartHDRDataStreaming(int frequency)
        {
            // Flush buffer
            sp_.DiscardInBuffer();
            sp_.DiscardOutBuffer();

            if (frequency > 0 && frequency <= _03b1MuseHW.MAX_STREAM_FREQUENCY)
            {
                ///////////////////////////////////////////////////////////////////////////////

                mfh_.VERSION = "1.0.0";
                // mfh_.DEVICE = this.devName_;                     // TODO - add [get] command on fw side
                // mfh_.FIRMWARE = this.GetFirmwareVersion();       // TODO - improve version management on fw side
                // mfh_.CONFIGURATION = this.GetConfigurationParams();
                // List<float> tmp = new List<float>();
                // tmp.AddRange(this.GetGyroscopeOffset());
                // tmp.AddRange(this.GetAccelerometerCalibParams());
                // tmp.AddRange(this.GetMagnetometerCalibParams());
                // mfh_.CALIBRATION = tmp.ToArray();
                mfh_.MODE = "stream";
                mfh_.FREQUENCY = frequency;
                mfh_.CHANNELS = new string[] { "Timestamp", "Gyr_X", "Gyr_Y", "Gyr_Z", "Acc_X", "Acc_Y", "Acc_Z", "Acc_X_HDR", "Acc_Y_HDR", "Acc_Z_HDR" };
                mfh_.DATA = "ASCII";

                tmpPath_ = Path.GetTempFileName();
                streamWriter_ = new StreamWriter(tmpPath_, true);

                ///////////////////////////////////////////////////////////////////////////////

                // Console.WriteLine("Start Streaming... - {0} Hz", frequency);

                // Send message
                sp_.Write(string.Format(_03b1MuseHW.StreamHDRData, frequency));
                spTXstatus_ = true;
               
                // Setup packet dimension
                packetDim_ = _03b1MuseHW.HDR_DATA_MESSAGE_SIZE;
                sp_.ReceivedBytesThreshold = packetDim_;

                System.Threading.Thread.Sleep(150);
            }
            else
                throw new ArgumentException("{0} is not a valid argument", frequency.ToString());
        }

        /// <summary> Starts quaterion streaming at a specified frequency <see cref=""/>_03b1MuseHW</see></summary>
        /// \hideinitializer
        public void StartQuaternionStreaming(int frequency)
        {
            // Flush buffer
            sp_.DiscardInBuffer();
            sp_.DiscardOutBuffer();

            if (frequency > 0 && frequency <= _03b1MuseHW.MAX_STREAM_FREQUENCY)
            {
                ///////////////////////////////////////////////////////////////////////////////

                mfh_.VERSION = "1.0.0";
                // mfh_.DEVICE = this.devName_;                     // TODO - add [get] command on fw side
                // mfh_.FIRMWARE = this.GetFirmwareVersion();       // TODO - improve version management on fw side
                // mfh_.CONFIGURATION = this.GetConfigurationParams();
                // List<float> tmp = new List<float>();
                // tmp.AddRange(this.GetGyroscopeOffset());
                // tmp.AddRange(this.GetAccelerometerCalibParams());
                // tmp.AddRange(this.GetMagnetometerCalibParams());
                // mfh_.CALIBRATION = tmp.ToArray();
                mfh_.MODE = "stream";
                mfh_.FREQUENCY = frequency;
                mfh_.CHANNELS = new string[] { "qW", "qX", "qY", "qZ" };
                mfh_.DATA = "ASCII";

                tmpPath_ = Path.GetTempFileName();
                streamWriter_ = new StreamWriter(tmpPath_, true);

                ///////////////////////////////////////////////////////////////////////////////

                // Console.WriteLine("Start Streaming... - {0} Hz", frequency);

                // Send message
                sp_.Write(string.Format(_03b1MuseHW.StreamQuaternion, frequency));
                spTXstatus_ = true;

                // Setup packet dimension
                packetDim_ = _03b1MuseHW.QUATERNION_MESSAGE_SIZE;
                sp_.ReceivedBytesThreshold = packetDim_;

                System.Threading.Thread.Sleep(150);
            }
            else
                throw new ArgumentException("{0} is not a valid argument", frequency.ToString());
        }

        /// <summary> Starts raw data streaming at a specified frequency </summary>
        /// \hideinitializer
        public void StartRawAndQuaternionStreaming(int frequency)
        {
            // Flush buffer
            sp_.DiscardInBuffer();
            sp_.DiscardOutBuffer();

            if (frequency > 0 && frequency <= _03b1MuseHW.MAX_STREAM_FREQUENCY)
            {
                ///////////////////////////////////////////////////////////////////////////////

                mfh_.VERSION = "1.0.0";
                // mfh_.DEVICE = this.devName_;                     // TODO - add [get] command on fw side
                // mfh_.FIRMWARE = this.GetFirmwareVersion();       // TODO - improve version management on fw side
                // mfh_.CONFIGURATION = this.GetConfigurationParams();
                // List<float> tmp = new List<float>();
                // tmp.AddRange(this.GetGyroscopeOffset());
                // tmp.AddRange(this.GetAccelerometerCalibParams());
                // tmp.AddRange(this.GetMagnetometerCalibParams());
                // mfh_.CALIBRATION = tmp.ToArray();
                mfh_.MODE = "stream";
                mfh_.FREQUENCY = frequency;
                mfh_.CHANNELS = new string[] { "Timestamp", "Gyr_X", "Gyr_Y", "Gyr_Z", "Acc_X", "Acc_Y", "Acc_Z", "Mag_X", "Mag_Y", "Mag_Z", "qW", "qX", "qY", "qZ" };
                mfh_.DATA = "ASCII";

                tmpPath_ = Path.GetTempFileName();
                streamWriter_ = new StreamWriter(tmpPath_, true);

                ///////////////////////////////////////////////////////////////////////////////

                // Console.WriteLine("Start Streaming... - {0} Hz", frequency);

                // Send message
                sp_.Write(string.Format(_03b1MuseHW.StreamRawAndQuaternionData, frequency));
                spTXstatus_ = true;

                // Setup packet dimension
                packetDim_ = _03b1MuseHW.RAW_DATA_MESSAGE_SIZE;
                sp_.ReceivedBytesThreshold = packetDim_;

                System.Threading.Thread.Sleep(150);
            }
            else
                throw new ArgumentException("{0} is not a valid argument", frequency.ToString());
        }

        #endregion

        #region LOG COMMANDS

        /// <summary> Start data logging via Bluetooth </summary>
        /// \hideinitializer
        public void StartLog()
        {
            // Flush buffer
            sp_.DiscardInBuffer();
            sp_.DiscardOutBuffer();

            // Send message
            sp_.Write(_03b1MuseHW.SetupLogBT);
            System.Threading.Thread.Sleep(500);

            // Send message
            sp_.Write(_03b1MuseHW.StartLogBT);
            System.Threading.Thread.Sleep(150);
        }

        /// <summary> Setup log mode and frequency </summary>
        /// \hideinitializer
        // public void SetupLog(_03b1MuseHW.Log_mode lgMode, int lgFreq)
        public void SetupLog(int logModeKey, int lgFreq)
        {
            // Flush buffer
            sp_.DiscardInBuffer();
            sp_.DiscardOutBuffer();

            // Setup parameters
            SetLogMode(logModeKey);
            SetLogFrequency(lgFreq);
        }

        /// <summary> Sets log mode </summary>
        /// \hideinitializer
        // public void SetLogMode(_03b1MuseHW.Log_mode lgMode)
        public void SetLogMode(int key)
        {
            // Flush buffer
            sp_.DiscardInBuffer();
            sp_.DiscardOutBuffer();

            Dictionary<int, string> tmp = _03b1MuseHW.GetLogMode();
            string val = "";
            bool res = tmp.TryGetValue(key, out val);

            // Set parameter
            if (res == true)
            {
                string cmd = string.Format(_03b1MuseHW.SetLogMode, key);

                // Send message
                sp_.Write(cmd);
                System.Threading.Thread.Sleep(150);
            }
        }

        /// <summary> Gets log mode </summary>
        /// \hideinitializer
        public int GetLogMode()
        {
            int[] parameters = GetConfigurationParams();
            return parameters[4];
        }

        /// <summary> Sets log frequency </summary>
        /// \hideinitializer
        public void SetLogFrequency(int frequency)
        {
            // Flush buffer
            sp_.DiscardInBuffer();
            sp_.DiscardOutBuffer();

            if (frequency > 0 && frequency <= _03b1MuseHW.MAX_LOG_FREQUENCY)
            {
                // Send message
                sp_.Write(string.Format(_03b1MuseHW.SetLogFrequency, frequency));
                System.Threading.Thread.Sleep(150);
            }
            else
                throw new ArgumentException("Invalid transmission frequency. Maximum frequency: " + _03b1MuseHW.MAX_LOG_FREQUENCY + ".");
        }

        /// <summary> Gets log frequency </summary>
        /// \hideinitializer
        public int GetLogFrequency()
        {
            int[] parameters = GetConfigurationParams();
            return parameters[5];
        }

        #endregion

        #region TYME SYNC COMMANDS

        // Enter time sync mode operation
        private void EnterTimeSyncMode()
        {
            sp_.WriteLine(_03b1MuseHW.EnterTimeSyncMode);
        }

        // Exit time sync mode operating
        private void ExitTimeSyncMode()
        {
            sp_.Write(_03b1MuseHW.ExitTimeSyncMode);
        }

        // Compute clock drift
        public float ComputeClockDrift(UInt16 n)
        {
            spTSstatus_ = true;

            Console.WriteLine("Compute Clock Drift...");

            // Flush buffer
            sp_.DiscardInBuffer();
            EnterTimeSyncMode();    // sp_.WriteLine(_03b1hw.EnterTimeSyncMode);
            System.Threading.Thread.Sleep(200);
            double clockDrift = 0;
            if (n > 0)
            {
                List<int> nodeList = new List<int>();
                List<double> centralList = new List<double>();
                for (int i = 0; i < n; i++)
                {
                    DateTime counter = DateTime.Now;
                    DateTime central = _03b1MuseUtilities.UtcNow;
                    sp_.WriteLine(_03b1MuseHW.ComputeClockDrift);   // "!!"
                    while (sp_.BytesToRead < 6 && (DateTime.Now - counter).TotalSeconds <= 2) ;
                    if ((DateTime.Now - counter).TotalSeconds <= 2)
                    {
                        byte[] buffer = new byte[6];
                        sp_.Read(buffer, 0, 6);
                        if (buffer[0] == 'C' && buffer[1] == 'D')
                        {
                            nodeList.Add(_03b1MuseUtilities.DecodeIntData(buffer[2], buffer[3], buffer[4], buffer[5]));
                            centralList.Add((central.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds));
                        }
                        System.Threading.Thread.Sleep(150);
                    }
                    else
                    {
                        nodeList.Clear();
                        centralList.Clear();
                        break;
                    }
                }

                double Sxy = 0, Sx = 0, Sy = 0, Sxx = 0;
                
                double centralOffset = 0;
                if (centralList.Count > 0)
                    centralOffset = centralList[0];

                int nodeOffset = nodeList[0];
                for (int i = 0; i < n-1; i++)
                {
                    centralList[i] = centralList[i] - centralOffset;
                    nodeList[i] = nodeList[i] - nodeOffset;
                    Sxy += nodeList[i] * centralList[i];
                    Sy += nodeList[i];
                    Sx += centralList[i];
                    Sxx += Math.Pow(centralList[i], 2);
                }
                clockDrift = (n * Sxy - Sx * Sy) / (n * Sxx - Math.Pow(Sx, 2));
            }

            ExitTimeSyncMode();     // sp_.WriteLine(_03b1hw.ExitTimeSyncMode);     // "?!"
            System.Threading.Thread.Sleep(200);

            spTSstatus_ = false;

            Console.WriteLine("{0:F}", (float)clockDrift);

            return (float)clockDrift;
        }

        // Set clock drift
        public void SetClockDrift(float f)
        {
            // Console.WriteLine("Set Clock Drift...");

            uint[] tempDrift = new uint[1];
            float[] tempDriftFloat = new float[1];
            tempDriftFloat[0] = f;
            Buffer.BlockCopy(tempDriftFloat, 0, tempDrift, 0, 4);
            byte[] b = new byte[4];
            for (int i = 0; i < 4; i++)
                b[i] = (byte)(tempDrift[0] >> ((3 - i) * 8) & 0x000000FF);

            sp_.Write(_03b1MuseHW.SetClockDrift);
            sp_.Write(b, 0, 4);

            System.Threading.Thread.Sleep(150);
        }

        // Retrieve clock drift
        public float GetClockDrift()
        {
            // Send message
            sp_.Write(_03b1MuseHW.GetClockDrift);
            while (sp_.BytesToRead < 6) ;

            // Read message
            byte[] buffer = new byte[6];
            sp_.Read(buffer, 0, 6);
            // Check transmission pattern
            float result = 1;
            if (buffer[0] == 'C' && buffer[1] == 'D')
                result = _03b1MuseUtilities.DecodeFloatData(buffer[2], buffer[3], buffer[4], buffer[5]);

            // Console.WriteLine("Retrieve Clock Drift...\t{0:G}\n", result);

            return result;
        }

        // Compute clock offset
        public double ComputeClockOffset(UInt16 n)
        {
            spTSstatus_ = true;

            Console.WriteLine("Compute Clock Offset...");

            //Retrieve the clock drift
            float a = GetClockDrift();
            double offset = 0;
            System.Threading.Thread.Sleep(200);
            EnterTimeSyncMode();    // sp_.WriteLine(_03b1hw.EnterTimeSyncMode);
            System.Threading.Thread.Sleep(200);
            if (n > 0)
            {
                List<int> nodeList = new List<int>();
                List<double> centralList = new List<double>();

                for (int i = 0; i < n; i++)
                {
                    DateTime T1 = _03b1MuseUtilities.UtcNow;
                    sp_.WriteLine(_03b1MuseHW.ComputeClockOffset);  // "??"
                    while (sp_.BytesToRead < 2) ;
                    DateTime T4 = _03b1MuseUtilities.UtcNow;
                    while (sp_.BytesToRead < 6) ;
                    byte[] buffer = new byte[6];
                    sp_.Read(buffer, 0, 6);
                    if (buffer[0] == 'C' && buffer[1] == 'O')
                    {
                        nodeList.Add(_03b1MuseUtilities.DecodeIntData(buffer[2], buffer[3], buffer[4], buffer[5]));
                        centralList.Add(T1.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds +
                                        T4.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds);
                    }
                    System.Threading.Thread.Sleep(100);
                }
                for (int i = 0; i < nodeList.Count; i++)
                    offset += nodeList[i] / a - centralList[i];
                offset /= n * 2;
            }
            ExitTimeSyncMode();     // sp_.WriteLine(_03b1hw.ExitTimeSyncMode);     // "?!"
            System.Threading.Thread.Sleep(200);

            spTSstatus_ = false;

            Console.WriteLine("{0:F}", offset);

            return offset;
        }

        // Set clock offset
        public void SetClockOffset(UInt32 offsetSec, UInt32 offsetMillis)
        {
            // Console.WriteLine("Set Clock Offset...");

            byte[] b = new byte[8];
            for (int i = 0; i < 4; i++)
                b[i] = (byte)(offsetSec >> ((3 - i) * 8) & 0x000000FF);

            for (int i = 0; i < 4; i++)
                b[4 + i] = (byte)(offsetMillis >> ((3 - i) * 8) & 0x000000FF);

            sp_.Write(_03b1MuseHW.SetClockOffset);
            sp_.Write(b, 0, 8);

            System.Threading.Thread.Sleep(150);
        }

        // Retrieve clock offset
        public double GetClockOffset()
        {
            // Flush buffer
            sp_.DiscardInBuffer();
            sp_.Write(_03b1MuseHW.GetClockOffset);
            while (sp_.BytesToRead < 10) ;
            byte[] buffer = new byte[10];
            sp_.Read(buffer, 0, 10);
            double result = 0;
            if (buffer[0] == 'C' && buffer[1] == 'O')
            {
                result = _03b1MuseUtilities.DecodeIntData(buffer[2], buffer[3], buffer[4], buffer[5]);
                result = result * 1000;
                result += _03b1MuseUtilities.DecodeIntData(buffer[6], buffer[7], buffer[8], buffer[9]);
            }
            result *= -1;

            // Console.Write("Retrieve Clock Offset...\t{0:G}\n", (float)result);
            
            return result;
        }

        // Gets Timestamp
        public double[] GetTimestamp()
        {
            Console.WriteLine("Retrieve Timestamp...");

            DateTime d, t;
            sp_.WriteLine(_03b1MuseHW.EnterTimeSyncMode);
            System.Threading.Thread.Sleep(200);

            d = _03b1MuseUtilities.UtcNow;
            sp_.WriteLine(_03b1MuseHW.GetTimestamp);    // "!?"

            while (sp_.BytesToRead < 2) ;

            t = _03b1MuseUtilities.UtcNow;
            double central = t.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds;
            double delta = t.Subtract(d).TotalMilliseconds;

            while (sp_.BytesToRead < 6) ;

            byte[] buffer = new byte[6];
            sp_.Read(buffer, 0, 6);
            double[] result = new double[3];
            result[0] = central;
            result[2] = delta;

            if (buffer[0] == 'T' && buffer[1] == 'T')
                result[1] = (double)_03b1MuseUtilities.DecodeIntData(buffer[2], buffer[3], buffer[4], buffer[5]);
            sp_.WriteLine(_03b1MuseHW.ExitTimeSyncMode);    // "?!"
            System.Threading.Thread.Sleep(150);

            return result;
        }

        #endregion

        #region MEMORY MANAGEMENT COMMANDS

        /// <summary> Memory space [get] </summary>
        /// <returns> The amount of available memory [Bytes] </returns>
        /// \hideinitializer
        public int GetAvailableMemory()
        {
            // Flush buffer
            sp_.DiscardInBuffer();
            sp_.DiscardOutBuffer();

            // Send message
            sp_.Write(_03b1MuseHW.GetAvailableMemory);
            while (sp_.BytesToRead < 6) ;

            // Read message
            byte[] buffer = new byte[6];
            sp_.Read(buffer, 0, 6);

            // Decode message
            int space = 0;
            if (buffer[0] == 'S' && buffer[1] == 'S')
                space = (int)_03b1MuseUtilities.DecodeIntData(buffer[2], buffer[3], buffer[4], buffer[5]);

            // Return memory space in Bytes
            return space;   
        }

        /// <summary> Erase device memory [erase] </summary>
        /// \hideinitializer
        public void EraseMemory()
        {
            // Flush buffer
            sp_.DiscardInBuffer();
            sp_.DiscardOutBuffer();

            // Stop any kind of transmission before erasing memory
            this.StopTransmission();
            
            // Send message
            sp_.Write(_03b1MuseHW.EraseMemory);
            System.Threading.Thread.Sleep(150);
        }

        /// <summary> Read device memory content </summary>
        /// \hideinitializer
        public List<List<_03b1MuseMessage>> ReadMemory(string destPath = "", string fileLabel = "", bool flagSync = false)
        {
            List<List<_03b1MuseMessage>> msgListList = null;

            // Flush buffer
            sp_.DiscardInBuffer();
            sp_.DiscardOutBuffer();

            spDevMsgList_.Clear();

            // Send message
            sp_.Write(_03b1MuseHW.ReadMemory);
            while (sp_.BytesToRead < 6) ;

            // Read message
            byte[] buffer = new byte[6];
            sp_.Read(buffer, 0, 6);

            // Decode message
            if (buffer[0] == 'N' && buffer[1] == 'N')
                numOfLogPackets_ = _03b1MuseUtilities.DecodeIntData(buffer[2], buffer[3], buffer[4], buffer[5]);

            if (numOfLogPackets_ > 0)
            {
                acqLog_ = true;
                singlePacket_ = false;

                // Start timer
                spTimer_.Start();

                // Wait for the end of download (making a blocking call)
                Console.WriteLine("Start download...");
                Console.WriteLine("Log Packets: {0}", numOfLogPackets_ / 512);
                while (acqLog_ == true) ;
                Console.WriteLine("Download completed!");

                List<DateTime> fList = this.GetFiles();

                ///////////////////////////////////////////////////////////////////////////////
                // Add data to list of list<_03b1MuseMessage>

                msgListList = new List<List<_03b1MuseMessage>>();

                int j = 0;
                int[] epochValues = new int[fList.Count];
                for (int i = 0; i < fList.Count; i++)
                    epochValues[i] = (int)(fList[i] - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds;

                for (int i = 0; i < fList.Count; i++)
                {
                    string fileName = "";
                    if (destPath != "")
                    {
                        fileName = destPath + "\\" + fileLabel + "_" + (fList[i].ToString().Replace("/", "-") + ".txt").Replace(":", "");
                        streamWriter_ = new StreamWriter(fileName, false);

                        streamWriter_.WriteLine("Timestamp\tGyr_X\tGyr_Y\tGyr_Z\tAcc_X\tAcc_Y\tAcc_Z\tMag_X\tMag_Y\tMag_Z\tqW\tqX\tqY\tqZ");
                    }

                    List<_03b1MuseMessage> msgList = new List<_03b1MuseMessage>();
                    bool exitWrite = false;
                    for (j = j; j < MessageList.Count && !exitWrite; j++)
                    {
                        msgList.Add(MessageList[j]);
                        if (destPath != "")
                        {
                            streamWriter_.WriteLine(MessageList[j].TimestampTX + "\t" +
                                                    MessageList[j].Gyroscope[0] + "\t" + MessageList[j].Gyroscope[1] + "\t" + MessageList[j].Gyroscope[2] + "\t" +
                                                    MessageList[j].Accelerometer[0] + "\t" + MessageList[j].Accelerometer[1] + "\t" + MessageList[j].Accelerometer[2] + "\t" +
                                                    MessageList[j].Magnetometer[0] + "\t" + MessageList[j].Magnetometer[1] + "\t" + MessageList[j].Magnetometer[2] + "\t" +
                                                    MessageList[j].Quaternion[0] + "\t" + MessageList[j].Quaternion[1] + "\t" + MessageList[j].Quaternion[2] + "\t" + MessageList[j].Quaternion[3]);
                            streamWriter_.Flush();
                        }

                        if (i + 1 < fList.Count)
                        {
                            // int cTime = 0;


                            // if (flagSync)
                            // {
                            //     double currentClockOffset = this.GetClockOffset();

                            //     cTime = (int)((MessageList[j].TimestampTX + (_03b1MuseHW.REFERENCE_EPOCH * 1000) - (currentClockOffset * 1000)) / 1000);
                            // }
                            // else
                            //     MessageList[j + 1].TimestampTX == epochValues[i + 1] ? lastItem = true : lastItem = false;

                            if (MessageList[j].TimestampTX == epochValues[i + 1])
                            {
                                exitWrite = true;
                                if (destPath != "")
                                {
                                    streamWriter_.Close();
                                    Console.WriteLine("Save completed! - File: {0}", fileName);
                                    System.Threading.Thread.Sleep(150);
                                }
                                break;
                            }
                        }
                        else
                        {
                            // Last element
                            if (j + 1 == MessageList.Count && destPath != "")
                            {
                                streamWriter_.Close();
                                Console.WriteLine("Save completed! - File: {0}", fileName);
                            }
                        }
                    }
                    msgListList.Add(msgList);
                }
            }

            return msgListList;
        }
         
        /// <summary> Getting the list of files written on memory as a list of DateTime identifiers </summary>
        /// \hideinitializer
        public List<DateTime> GetFiles()
        {
            // Flush buffer
            sp_.DiscardInBuffer();
            sp_.DiscardOutBuffer();

            // Send message
            sp_.WriteLine(_03b1MuseHW.GetFiles); // sp_.WriteLine("?!GETFILES!?");
            Thread.Sleep(150);

            // Read message
            byte[] buffer = new byte[sp_.BytesToRead];
            sp_.Read(buffer, 0, sp_.BytesToRead);

            // Decode message
            List<DateTime> result = new List<DateTime>();
            if (buffer[0] == 'F' && buffer[1] == 'F')
            {
                int n = _03b1MuseUtilities.DecodeShortData(buffer[2], buffer[3]);

                for (int i = 0; i < n; i++)
                {
                    DateTime d = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                    d = d.AddSeconds(_03b1MuseUtilities.DecodeIntData(buffer[i * 4 + 4], buffer[i * 4 + 4 + 1], buffer[i * 4 + 4 + 2], buffer[i * 4 + 4 + 3]));
                    result.Add(d);
                }
            }

            return result;
        }

        /// <summary> Read a specified file </summary>
        /// <param name="fileNumber"> Identifier number of the file to be read </param>
        public List<_03b1MuseMessage> ReadFile(int fileNumber, string destPath = "")
        {
            // Flush buffer
            sp_.DiscardInBuffer();
            sp_.DiscardOutBuffer();

            if (fileNumber > 0)
            {
                spDevMsgList_.Clear();
                
                // Create message
                string cmd = string.Format(_03b1MuseHW.ReadFile, fileNumber);

                // Send message
                sp_.Write(cmd); // sp_.Write(number);
                while (sp_.BytesToRead < 6) ;

                // Read message
                byte[] buffer = new byte[6];
                sp_.Read(buffer, 0, 6);

                // Decode message
                if (buffer[0] == 'N' && buffer[1] == 'N')
                    numOfLogPackets_ = _03b1MuseUtilities.DecodeIntData(buffer[2], buffer[3], buffer[4], buffer[5]);

                if (numOfLogPackets_ > 0)
                {
                    acqLog_ = true;
                    singlePacket_ = false;

                    // Start timer
                    spTimer_.Start();

                    // Wait for the end of download (making a blocking call)
                    Console.WriteLine("Start download...");
                    while (acqLog_ == true) ;
                    Console.WriteLine("Download completed!");

                    ///////////////////////////////////////////////////////////////////////////////
                    // Save data to file...

                    if (destPath != "")
                    {
                        Console.WriteLine("Save data to file...");

                        streamWriter_ = new StreamWriter(destPath, false);

                        mfh_.VERSION = "1.0.0";
                        // mfh_.DEVICE = this.devName_;                     // TODO - add [get] command on fw side
                        mfh_.FIRMWARE = this.GetFirmwareVersion();          // TODO - improve version management on fw side
                        mfh_.CONFIGURATION = this.GetConfigurationParams();
                        List<float> tmp = new List<float>();
                        tmp.AddRange(this.GetGyroscopeOffset());
                        tmp.AddRange(this.GetAccelerometerCalibParams());
                        tmp.AddRange(this.GetMagnetometerCalibParams());
                        mfh_.CALIBRATION = tmp.ToArray();
                        mfh_.MODE = "log";
                        mfh_.FREQUENCY = this.GetLogFrequency();
                        mfh_.CHANNELS = new string[] { "Timestamp", "Gyr_X", "Gyr_Y", "Gyr_Z", "Acc_X", "Acc_Y", "Acc_Z", "Mag_X", "Mag_Y", "Mag_Z", "qW", "qX", "qY", "qZ" };
                        mfh_.DATA = "ASCII";

                        streamWriter_.WriteLine(mfh_.ToString());

                        foreach (_03b1MuseMessage msg in spDevMsgList_)
                        {
                            streamWriter_.WriteLine(msg.TimestampTX + "\t" +
                                                    msg.Gyroscope[0] + "\t" + msg.Gyroscope[1] + "\t" + msg.Gyroscope[2] + "\t" +
                                                    msg.Accelerometer[0] + "\t" + msg.Accelerometer[1] + "\t" + msg.Accelerometer[2] + "\t" +
                                                    msg.Magnetometer[0] + "\t" + msg.Magnetometer[1] + "\t" + msg.Magnetometer[2] + "\t" +
                                                    msg.Quaternion[0] + "\t" + msg.Quaternion[1] + "\t" + msg.Quaternion[2] + "\t" + msg.Quaternion[3]);
                            streamWriter_.Flush();
                        }
                        streamWriter_.Close();

                        Console.WriteLine("Save completed!");
                    }

                    ///////////////////////////////////////////////////////////////////////////////
                }
            }
            else
                throw new ArgumentException("{0} is not a valid argument", fileNumber.ToString());

            return spDevMsgList_;
        }
        /// <summary> Read a specified file interval </summary>
        /// <param name="fileNumber"> Identifier number of the file to be read </param>
        /// <param name="startEpoch"> Start epoch of the file span to be read </param>
        /// <param name="endEpoch"> End epoch of the file span to be read </param>
        /// <param name="destPath"> Path of the .txt file where save the downloaded content </param>
        public List<_03b1MuseMessage> ReadFileSpan(int fileNumber, int startEpoch, int endEpoch, string destPath = "")
        {
            // Flush buffer
            sp_.DiscardInBuffer();
            sp_.DiscardOutBuffer();

            if (fileNumber > 0)
            {
                spDevMsgList_.Clear();

                // Encoding parameters
                byte[] fNumber = BitConverter.GetBytes(fileNumber);
                byte[] sEpochBytes = BitConverter.GetBytes(startEpoch);
                byte[] eEpochBytes = BitConverter.GetBytes(endEpoch);

                // Compose message body
                byte[] parameters = { fNumber[1], fNumber[0],
                                      sEpochBytes[3], sEpochBytes[2], sEpochBytes[1], sEpochBytes[0],
                                      eEpochBytes[3], eEpochBytes[2], eEpochBytes[1], eEpochBytes[0] };

                // Send message
                sp_.Write(_03b1MuseHW.ReadFileSpan);    // Header (command)
                sp_.Write(parameters, 0, 10);           // Parameters
                sp_.Write("!");							// Footer

                while (sp_.BytesToRead < 6) ;

                // Read message
                byte[] buffer = new byte[6];
                sp_.Read(buffer, 0, 6);
                if (buffer[0] == 'N' && buffer[1] == 'N')
                    numOfLogPackets_ = _03b1MuseUtilities.DecodeIntData(buffer[2], buffer[3], buffer[4], buffer[5]);

                if (numOfLogPackets_ > 0)
                {
                    acqLog_ = true;
                    singlePacket_ = false;

                    // Start timer
                    spTimer_.Start();

                    // Wait for the end of download (making a blocking call)
                    Console.WriteLine("Start download...");
                    while (acqLog_ == true) ;
                    Console.WriteLine("Download completed!");

                    ///////////////////////////////////////////////////////////////////////////////
                    // Save data to file...

                    if (destPath != "")
                    {
                        Console.WriteLine("Save data to file...");

                        streamWriter_ = new StreamWriter(destPath, false);

                        streamWriter_.WriteLine("Timestamp\tGyr_X\tGyr_Y\tGyr_Z\tAcc_X\tAcc_Y\tAcc_Z\tMag_X\tMag_Y\tMag_Z\tqW\tqX\tqY\tqZ");
                        foreach (_03b1MuseMessage msg in spDevMsgList_)
                        {
                            streamWriter_.WriteLine(msg.TimestampTX + "\t" +
                                                    msg.Gyroscope[0] + "\t" + msg.Gyroscope[1] + "\t" + msg.Gyroscope[2] + "\t" +
                                                    msg.Accelerometer[0] + "\t" + msg.Accelerometer[1] + "\t" + msg.Accelerometer[2] + "\t" +
                                                    msg.Magnetometer[0] + "\t" + msg.Magnetometer[1] + "\t" + msg.Magnetometer[2] + "\t" +
                                                    msg.Quaternion[0] + "\t" + msg.Quaternion[1] + "\t" + msg.Quaternion[2] + "\t" + msg.Quaternion[3]);
                            streamWriter_.Flush();
                        }
                        streamWriter_.Close();

                        Console.WriteLine("Save completed!");
                    }

                    ///////////////////////////////////////////////////////////////////////////////
                }
            }
            else
                throw new ArgumentException("{0} is not a valid argument", fileNumber.ToString());

            return spDevMsgList_;
        }
        /// <summary> Get the number of bytes of a specific file. </summary>
        /// <param name="fileNumber"> Identifier number of the file to be read </param>
        public int GetPacketsNumber(int fileNumber)
        {
            // Flush buffer
            sp_.DiscardInBuffer();
            sp_.DiscardOutBuffer();

            // Send message
            string cmd = string.Format(_03b1MuseHW.GetPacketsNumber, fileNumber);
            sp_.Write(cmd); // sp_.Write(number);
            while (sp_.BytesToRead < 6) ;

            // Read message
            byte[] buffer = new byte[6];
            sp_.Read(buffer, 0, 6);

            // Decode message
            if (buffer[0] == 'N' && buffer[1] == 'N')
                numOfLogPackets_ = _03b1MuseUtilities.DecodeIntData(buffer[2], buffer[3], buffer[4], buffer[5]);

            return numOfLogPackets_;
        }
        /// <summary> Get the specific packet of a given file. </summary>
        /// <param name="fileNumber"> Identifier number of the file to be read </param>
        /// <param name="packetNumber"> Packet number to be downloaded </param>
        public void ReadPacket(int fileNumber, int packetNumber)
        {
            // Flush buffer
            sp_.DiscardInBuffer();
            sp_.DiscardOutBuffer();

            if (fileNumber > 0 && packetNumber >= 0)
            {
                if(packetNumber == 0)
                    spDevMsgList_.Clear();

                // Encoding parameters
                byte[] fNumber = BitConverter.GetBytes(fileNumber);
                byte[] pNumber = BitConverter.GetBytes(packetNumber);

                // Compose message body
                byte[] parameters = { fNumber[1], fNumber[0], pNumber[1], pNumber[0] };

                // Send message
                sp_.Write(_03b1MuseHW.ReadPacket);  // Header (command)
                sp_.Write(parameters, 0, 4);        // Parameters
                sp_.Write("!");                     // Footer

                acqLog_ = true;
                singlePacket_ = true;
                // packetReceived_ = false;

                // Start timer
                spTimer_.Start();

                // Wait for the end of download (making a blocking call)
                // Console.WriteLine("Start download...");
                // while (packetReceived_ == false) ;
                // while (acqLog_ == true) ;
                // Console.WriteLine("Download completed!");
            }
        }

        #endregion

        #region ACKNOWLEDGE

        /// <summary> Send positive acknowledge to device </summary>
        public void PacketOK()
        {
            Console.WriteLine("Packet OK - {0}", acquiredPackets_ / 512);

            sp_.Write(_03b1MuseHW.PacketOK);
        }

        /// <summary> Send negative acknowledge to device </summary>
        public void PacketKO(object sender, EventArgs e)
        {
            spTimer_.Stop();

            sp_.DiscardInBuffer();

            Console.WriteLine("Packet ERROR - {0}", acquiredPackets_ / 512);

            if (spDevMsgList_.Count - lastMsgIdx_ - 1 >= 0)
            {
                spDevMsgList_.RemoveRange(lastMsgIdx_ + 1, spDevMsgList_.Count - lastMsgIdx_ - 1);
                acquiredPackets_ -= lastReceivedPacket_ * 512;
                lastReceivedPacket_ = 0;
            }
            sp_.Write(_03b1MuseHW.PacketKO);

            spTimer_.Start();
        }

        #endregion

        #region EMBEDDED BLUETOOTH MODULE COMMANDS

        /// <summary> Enter Bluetooth module command mode </summary>
        public void EnterBluetoothCommandMode()
        {
            byte[] buffer = new byte[1024];
            if (spTXstatus_ == true)
            {
                // Stop transmission, flush the buffer and wait for 2 seconds in order to allow the remote command mode
                StopTransmission();
                // sp_.DiscardInBuffer();
                System.Threading.Thread.Sleep(3000);
            }

            sp_.Write(_03b1MuseHW.BTcmdEscape);
            enableBTcmd_ = true;
            System.Threading.Thread.Sleep(2000);
        }

        /// <summary> Escape Bluetooth module </summary>
        public void BTcmdEscape()
        {
            sp_.Write(_03b1MuseHW.BTcmdEscape);
        }

        /// <summary> Reset Bluetooth module </summary>
        public void BTcmdReset()
        {
            if (enableBTcmd_ == false)
            {
                // EnterBluetoothCommandMode();
                byte[] buffer = new byte[1024];
                if (spTXstatus_ == true)
                {
                    // Stop transmission, flush the buffer and wait for 2 seconds in order to allow the remote command mode
                    StopTransmission();
                    // sp_.DiscardInBuffer();
                    System.Threading.Thread.Sleep(3000);
                }

                sp_.Write(_03b1MuseHW.BTcmdEscape);
                enableBTcmd_ = true;
                System.Threading.Thread.Sleep(2000);
            }
            sp_.Write(_03b1MuseHW.BTcmdReset);
            System.Threading.Thread.Sleep(2000);
            sp_.DiscardInBuffer();
        }

        /// <summary> Bypass Bluetooth module </summary>
        public void BTcmdBypass()
        {
            if (enableBTcmd_ == false)
                EnterBluetoothCommandMode();
            sp_.Write(_03b1MuseHW.BTcmdBypass);
        }

        /// <summary> Rename Bluetooth module </summary>
        /// <param name="newName"> New name to be set </param>
        public void BTcmdRename(string name)
        {
            if (enableBTcmd_ == false)
            {
                // EnterBluetoothCommandMode();
                byte[] buffer = new byte[1024];
                if (spTXstatus_ == true)
                {
                    // Stop transmission, flush the buffer and wait for 2 seconds in order to allow the remote command mode
                    StopTransmission();
                    // sp_.DiscardInBuffer(); // already performed with a stop transmission command
                    System.Threading.Thread.Sleep(3000);
                }

                sp_.Write(_03b1MuseHW.BTcmdEscape);
                enableBTcmd_ = true;
                System.Threading.Thread.Sleep(2000);
            }
            string cmd = string.Format(_03b1MuseHW.BTcmdRename, name);
            sp_.Write(cmd);
            BTcmdReset();
        }

        #endregion

        #region GENERAL COMMANDS

        /// <summary> Stop any kind of data streaming </summary>
        public void StopTransmission(string path = "")
        {
            Console.WriteLine("Stop Transmission...");

            // Flush buffer
            sp_.DiscardInBuffer();
            sp_.DiscardOutBuffer();

            // Send message
            sp_.WriteTimeout = 1000;

            sp_.Write(_03b1MuseHW.StopTransmission);
            System.Threading.Thread.Sleep(150);

            spTXstatus_ = false;

            // Flush buffer
            sp_.DiscardInBuffer();
            sp_.DiscardOutBuffer();

            // Manage file writing and saving
            if (tmpPath_ != null && path != "")
            {
                Console.WriteLine("Save data to file... - {0}", path);

                streamWriter_.Close();                          // Close temporary file stream writer (recorded data)

                // Update file header with device configuration / calibration information
                // mfh_.VERSION = "1.0.0";
                // mfh_.DEVICE = this.devName_;                 // TODO - add [get] command on fw side
                mfh_.FIRMWARE = this.GetFirmwareVersion();      // TODO - improve version management on fw side
                mfh_.CONFIGURATION = this.GetConfigurationParams();
                List<float> tmp = new List<float>();
                tmp.AddRange(this.GetGyroscopeOffset());
                tmp.AddRange(this.GetAccelerometerCalibParams());
                tmp.AddRange(this.GetMagnetometerCalibParams());
                mfh_.CALIBRATION = tmp.ToArray();
                // mfh_.MODE = "STREAM";
                // mfh_.FREQUENCY = frequency;
                // mfh_.CHANNELS = new string[] { "Timestamp", "Gyr_X", "Gyr_Y", "Gyr_Z", "Acc_X", "Acc_Y", "Acc_Z", "Acc_X_HDR", "Acc_Y_HDR", "Acc_Z_HDR" };
                // mfh_.DATA = "ASCII";

                _03b1MuseUtilities.InsertHeader(tmpPath_, mfh_.ToString());    // Insert file header
                File.Copy(tmpPath_, path, true);            // Copy recorded data into file
                File.Delete(tmpPath_);                      // Delete temporary file
            }
        }

        /// <summary> Shutdown and disconnect device </summary>
        public void Shutdown()
        {
            // Flush buffer
            sp_.DiscardInBuffer();
            sp_.DiscardOutBuffer();

            // Send message
            sp_.WriteTimeout = 1000;

            sp_.Write(_03b1MuseHW.Shutdown);
            System.Threading.Thread.Sleep(150);

            // Flush Input Buffer
            // Thread.Sleep(500);

            // Flush buffer
            sp_.DiscardInBuffer();
            sp_.DiscardOutBuffer();

            Disconnect();
        }

        /// <summary> Disconnect device </summary>
        public void Disconnect()
        {
            // Check Bluetooth module performed in StopTransmission
            if (spTXstatus_ == true)
                StopTransmission();
                // System.Threading.Thread.Sleep(150);

            sp_.Close();
            spStatus_ = false;
        }

        #endregion

        #region MISCELLANEOUS COMMANDS

        /// <summary> Getting the battery voltage </summary>
        public int GetBatteryVoltage()
        {
            int result = 0;

            // Flush buffer
            sp_.DiscardInBuffer();
            sp_.DiscardOutBuffer();

            // Send message
            sp_.Write(_03b1MuseHW.GetBatteryVoltage);
            while (sp_.BytesToRead < 4) ;

            // Read message
            byte[] buffer = new byte[4];
            int msg = sp_.Read(buffer, 0, 4);

            // Decode message
            if (buffer[0] == 'B' && buffer[1] == 'V')
                result = (ushort)_03b1MuseUtilities.DecodeShortData(buffer[2],buffer[3]);   // mV

            return result;
        }

        /// <summary> Getting the battery charge </summary>
        public int GetBatteryCharge()
        {
            int result = 0;

            // Flush buffer
            sp_.DiscardInBuffer();
            sp_.DiscardOutBuffer();

            // Send message
            sp_.Write(_03b1MuseHW.GetBatteryCharge);
            while (sp_.BytesToRead < 4) ;

            // Read message
            byte[] buffer = new byte[4];
            int msg = sp_.Read(buffer, 0, 4);

            // Decode message
            if (buffer[0] == 'B' && buffer[1] == 'Q')
                result = (ushort)buffer[2];

            return result;
        }

        public int[] ResetGasGauge()
        {
            int[] result = { -1, -1 };

            // Flush buffer
            sp_.DiscardInBuffer();
            sp_.DiscardOutBuffer();

            // Send message
            sp_.Write(_03b1MuseHW.GetBatteryCharge);
            while (sp_.BytesToRead < 4) ;

            // Read message
            byte[] buffer = new byte[4];
            int msg = sp_.Read(buffer, 0, 4);

            // Decode message
            if (buffer[0] == 'B' && buffer[1] == 'Q')
                result[0] = (ushort)buffer[2];

            System.Threading.Thread.Sleep(5000);

            // Flush buffer
            sp_.DiscardInBuffer();
            sp_.DiscardOutBuffer();

            // Send message
            sp_.Write(_03b1MuseHW.GetBatteryCharge);
            while (sp_.BytesToRead < 4) ;

            // Read message
            buffer = new byte[4];
            msg = sp_.Read(buffer, 0, 4);

            // Decode message
            if (buffer[0] == 'B' && buffer[1] == 'Q')
                result[1] = (ushort)buffer[2];

            return result;
        }

        /// <summary> Set date </summary>
        public void SetDate()
        {
            // Flush buffer
            sp_.DiscardInBuffer();
            sp_.DiscardOutBuffer();

            TimeSpan t = DateTime.Now.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc));
            uint secondsSinceEpoch = (uint)t.TotalSeconds;
            byte[] b = new byte[4];
            for (int i = 0; i < 4; i++)
                b[i] = (byte)(secondsSinceEpoch >> ((3 - i) * 8) & 0x000000FF);

            // Send message
            sp_.Write(_03b1MuseHW.SetDate);
            sp_.Write(b, 0, 4);
        }

        /// <summary> Get date </summary>
        public DateTime GetDate()
        {
            DateTime result;

            // Flush buffer
            sp_.DiscardInBuffer();
            sp_.DiscardOutBuffer();

            // Send message
            sp_.Write(_03b1MuseHW.GetDate);
            while (sp_.BytesToRead < 6) ;

            // Read message
            byte[] buffer = new byte[6];
            sp_.Read(buffer, 0, 6);

            result = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

            // Decode message
            if (buffer[0] == 'T' && buffer[1] == 'T')
            {
                int timeStamp = _03b1MuseUtilities.DecodeIntData(buffer[2], buffer[3], buffer[4], buffer[5]);
                result = result.AddSeconds(timeStamp);
            }

            return result;
        }

        /// <summary> Get current firmware version </summary>
        public string GetFirmwareVersion()
        {
            string result = "";

            // Flush buffer
            sp_.DiscardInBuffer();
            sp_.DiscardOutBuffer();

            // Send message
            sp_.Write(_03b1MuseHW.GetFirmwareVersion);
            while (sp_.BytesToRead < 3) ;

            // Read message
            byte[] buffer = new byte[3];
            sp_.Read(buffer, 0, 3);

            // Decode message
            if (buffer[0] == 'F' && buffer[1] == 'V')
                result = Convert.ToString(buffer[2] / 10);

            return result;
        }

        /// <summary> Check HDR mode availability  </summary>
        public bool CheckHDRAvailability()
        {
            bool result = false;

            // Flush buffer
            sp_.DiscardInBuffer();
            sp_.DiscardOutBuffer();

            // Send message
            sp_.Write(_03b1MuseHW.CheckHDRAvailability);
            while (sp_.BytesToRead < 8) ;

            // Read message
            byte[] buffer = new byte[8];
            sp_.Read(buffer, 0, 8);

            // Decode message
            if (buffer[0] == 'H' && buffer[1] == 'D' && buffer[2] == 'R' && buffer[3] == 'M' && buffer[4] == 'U' && buffer[5] == 'S' && buffer[6] == 'E')
            {
                if (buffer[7] == 1)
                    result = true;
            }

            return result;
        }

        #endregion

        #endregion

        #region PRIVATE METHODS

        /// <summary> Data receive event handler. </summary>
        private void DataReceivedHandler(object sender, SerialDataReceivedEventArgs e)
        {
            // Debugger.Break();
            if (acqLog_ && singlePacket_)
            {
                // LOG MODE - Versione OPICE - edy style!
                if (sp_.BytesToRead >= 516)
                {
                    // spTXstatus_ = true;
                    byte[] buffer = new byte[516];
                    sp_.Read(buffer, 0, 516);
                    // Check transmission pattern
                    if (buffer[0] == 'P' && buffer[1] == 'K')
                    {
                        // Stop timer
                        spTimer_.Stop();

                        uint parity = 0;
                        int mode = buffer[2];
                        int freq = buffer[3];
                        
                        if (mode > 0 && mode < 5)
                        {
                            int epoch = _03b1MuseUtilities.DecodeIntData(buffer[4], buffer[5], buffer[6], buffer[7]);
                            _03b1MuseMessage tempData = new _03b1MuseMessage();
                            int dataIndex = 0;
                            int[] temp = new int[3];
                            float[] tempF = new float[3];
                            for (int j = 0; j < 512; j++)
                            {
                                // Accelerometer, Gyroscope, Magnetometer, Quaternion
                                parity += buffer[j];
                                if (j > 7 && j % 2 == 0)
                                {
                                    temp[dataIndex % 3] = _03b1MuseUtilities.DecodeShortData(buffer[j], buffer[j + 1]);
                                    tempF[dataIndex % 3] = ((float)_03b1MuseUtilities.DecodeShortData(buffer[j], buffer[j + 1]));
                                    dataIndex++;

                                    if (mode == 1 && dataIndex == 3)
                                    {
                                        float[] tempQ = new float[4];
                                        float tempN = 0;
                                        for (int k = 0; k < 3; k++)
                                        {
                                            tempQ[k + 1] = ((float)temp[k]) / 32767;
                                            tempN += (tempQ[k + 1] * tempQ[k + 1]);
                                        }
                                        tempQ[0] = (float)Math.Sqrt(1 - (tempN));
                                        tempN = 0;
                                        for (int k = 0; k < 4; k++)
                                            tempN += (tempQ[k] * tempQ[k]);
                                        for (int k = 0; k < 4; k++)
                                            tempQ[k] /= tempN;
                                        tempData.Quaternion = tempQ;
                                    }
                                    else
                                    {
                                        if (dataIndex == 3) // Accelerometer 
                                        {
                                            // tempData.Accelerometer[0] = (float)temp[0];
                                            // tempData.Accelerometer[1] = (float)temp[1];
                                            // tempData.Accelerometer[2] = (float)temp[2];

                                            tempData.Accelerometer = tempF;
                                        }
                                        else
                                        {
                                            if (dataIndex == 6) // Gyroscope
                                                tempData.Gyroscope = tempF;
                                            else
                                            {
                                                if (dataIndex == 9) // Magnetometer
                                                    tempData.Magnetometer = temp;
                                                else
                                                {
                                                    if (dataIndex == 12)
                                                    {
                                                        float[] tempQ = new float[4];
                                                        float tempN = 0;
                                                        for (int k = 0; k < 3; k++)
                                                        {
                                                            tempQ[k + 1] = ((float)temp[k]) / 32767;
                                                            tempN += (tempQ[k + 1] * tempQ[k + 1]);
                                                        }
                                                        tempQ[0] = (float)Math.Sqrt(1 - (tempN));
                                                        tempN = 0;
                                                        for (int k = 0; k < 4; k++)
                                                            tempN += (tempQ[k] * tempQ[k]);
                                                        for (int k = 0; k < 4; k++)
                                                            tempQ[k] /= tempN;
                                                        tempData.Quaternion = tempQ;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    if (((((j - 7) + 1) % (mode * 6) == 0) && mode != 5)) //  || ((((j - 7) + 1) % (3 * 6) == 0) && mode == 5))
                                    {
                                        ////////////////////////////////////////////////////////////////////////
                                        // TO BE MODIFIED IN ORDER TO REMOVE THEM FROM _03b1Message object!!!
                                        tempData.LogFrequency = freq;
                                        tempData.LogMode = mode;
                                        ////////////////////////////////////////////////////////////////////////
                                        tempData.TimestampTX = epoch;

                                        // Store data
                                        dataIndex = 0;
                                        spDevMsgList_.Add(tempData);
                                        tempData = new _03b1MuseMessage();
                                    }
                                }
                            }

                            acquiredPackets_ += 512;
                            lastReceivedPacket_++;

                            packetReceived_ = true;

                            if (lastReceivedPacket_ % 20 == 0 || acquiredPackets_ >= numOfLogPackets_)
                            {
                                // Transmit acknowledge
                                PacketOK();
                                // acqLog_ = false;
                                lastReceivedPacket_ = 0;
                                lastMsgIdx_ = MessageList.Count - 1;
                            }
                            // Deal with a new packet
                            // if (NewLogSample != null)
                            //     NewLogSample(this, (int)((acquiredPackets_ * 100) / numOfLogPackets_));

                            //Start timer
                            spTimer_.Start();
                        }
                        else if (mode == 5)
                        {
                            int epoch = _03b1MuseUtilities.DecodeIntData(buffer[4], buffer[5], buffer[6], buffer[7]);
                            _03b1MuseMessage tempData = new _03b1MuseMessage();

                            int dataIndex = 0;
                            byte[] msgData = new byte[36];
                            for (int j = 0; j < 512; j++)
                            {
                                parity += buffer[j];

                                if (j > 7)
                                {
                                    msgData[dataIndex % 36] = buffer[j];
                                    dataIndex++;

                                    if (dataIndex % 36 == 0)
                                    {
                                        // Accelerometer
                                        float[] tempAcc = new float[3];
                                        tempAcc[0] = _03b1MuseUtilities.DecodeFloatData(msgData[0], msgData[1], msgData[2], msgData[3]);
                                        tempAcc[1] = _03b1MuseUtilities.DecodeFloatData(msgData[4], msgData[5], msgData[6], msgData[7]);
                                        tempAcc[2] = _03b1MuseUtilities.DecodeFloatData(msgData[8], msgData[9], msgData[10], msgData[11]);
                                        tempData.Accelerometer = tempAcc;

                                        // Gyroscope
                                        float[] tempGyro = new float[3];
                                        tempGyro[0] = _03b1MuseUtilities.DecodeFloatData(msgData[12 + 0], msgData[12 + 1], msgData[12 + 2], msgData[12 + 3]);
                                        tempGyro[1] = _03b1MuseUtilities.DecodeFloatData(msgData[12 + 4], msgData[12 + 5], msgData[12 + 6], msgData[12 + 7]);
                                        tempGyro[2] = _03b1MuseUtilities.DecodeFloatData(msgData[12 + 8], msgData[12 + 9], msgData[12 + 10], msgData[12 + 11]);
                                        tempData.Gyroscope = tempGyro;

                                        // Magnetometer
                                        int[] tempMagn = new int[3];
                                        tempMagn[0] = _03b1MuseUtilities.DecodeShortData(msgData[24 + 0], msgData[24 + 1]);
                                        tempMagn[1] = _03b1MuseUtilities.DecodeShortData(msgData[24 + 2], msgData[24 + 3]);
                                        tempMagn[2] = _03b1MuseUtilities.DecodeShortData(msgData[24 + 4], msgData[24 + 5]);
                                        tempData.Magnetometer = tempMagn;

                                        // Quaternion
                                        int[] tempF = new int[3];
                                        tempF[0] = _03b1MuseUtilities.DecodeShortData(msgData[30 + 0], msgData[30 + 1]);
                                        tempF[1] = _03b1MuseUtilities.DecodeShortData(msgData[30 + 2], msgData[30 + 3]);
                                        tempF[2] = _03b1MuseUtilities.DecodeShortData(msgData[30 + 4], msgData[30 + 5]);

                                        float[] tempQ = new float[4];
                                        float tempN = 0;
                                        for (int k = 0; k < 3; k++)
                                        {
                                            tempQ[k + 1] = ((float)tempF[k]) / 32767;
                                            tempN += (tempQ[k + 1] * tempQ[k + 1]);
                                        }
                                        tempQ[0] = (float)Math.Sqrt(1 - (tempN));
                                        tempN = 0;
                                        for (int k = 0; k < 4; k++)
                                            tempN += (tempQ[k] * tempQ[k]);
                                        for (int k = 0; k < 4; k++)
                                            tempQ[k] /= tempN;
                                        tempData.Quaternion = tempQ;

                                        ////////////////////////////////////////////////////////////////////////
                                        // TO BE MODIFIED IN ORDER TO REMOVE THEM FROM _03b1Message object!!!
                                        tempData.LogFrequency = freq;
                                        tempData.LogMode = mode;
                                        ////////////////////////////////////////////////////////////////////////
                                        tempData.TimestampTX = epoch;

                                        // Store data
                                        dataIndex = 0;
                                        spDevMsgList_.Add(tempData);
                                        tempData = new _03b1MuseMessage();
                                    }
                                }
                            }

                            acquiredPackets_ += 512;
                            // lastReceivedPacket_++;

                            // packetReceived_ = true;

                            // if (lastReceivedPacket_ % 20 == 0 || acquiredPackets_ >= numOfLogPackets_)
                            // {
                                // Transmit acknowledge
                                // PacketOK();
                                // acqLog_ = false;
                                // lastReceivedPacket_ = 0;
                                // lastMsgIdx_ = MessageList.Count - 1;
                            // }
                            // Deal with a new packet
                            // if (NewLogSample != null)
                            //     NewLogSample(this, (int)((acquiredPackets_ * 100) / numOfLogPackets_));

                            //Start timer
                            // spTimer_.Start();
                        }
                        //acquiredPackets_ += 512;
                        //lastReceivedPacket_++;

                        //packetReceived_ = true;

                        //if (lastReceivedPacket_ % 20 == 0 || acquiredPackets_ >= numOfLogPackets_)
                        //{
                        //    // Transmit acknowledge
                        //    PacketOK();
                        //    // acqLog_ = false;
                        //    lastReceivedPacket_ = 0;
                        //    lastMsgIdx_ = MessageList.Count - 1;
                        //}
                        //// Deal with a new packet
                        //// if (NewLogSample != null)
                        ////     NewLogSample(this, (int)((acquiredPackets_ * 100) / numOfLogPackets_));

                        ////Start timer
                        //spTimer_.Start();
                    }
                }

                if (acquiredPackets_ >= numOfLogPackets_)
                {
                    // spTXstatus_ = false;
                    acqLog_ = false;
                    // packetReceived_ = false;
                    numOfLogPackets_ = 0;
                    acquiredPackets_ = 0;

                    // Stop timer
                    spTimer_.Stop();

                    /////////////////////////////////////////////////////////////////////////////////

                    //if (streamWriter_ != null)
                    //{
                    //    Console.WriteLine("Save data to file...");

                    //    streamWriter_.WriteLine("Timestamp\tGyr_X\tGyr_Y\tGyr_Z\tAcc_X\tAcc_Y\tAcc_Z\tMag_X\tMag_Y\tMag_Z\tqW\tqX\tqY\tqZ");
                    //    foreach (_03b1MuseMessage msg in spDevMsgList_)
                    //    {
                    //        streamWriter_.WriteLine(msg.TimestampTX + "\t" +
                    //                                msg.Gyroscope[0] + "\t" + msg.Gyroscope[1] + "\t" + msg.Gyroscope[2] + "\t" +
                    //                                msg.Accelerometer[0] + "\t" + msg.Accelerometer[1] + "\t" + msg.Accelerometer[2] + "\t" +
                    //                                msg.Magnetometer[0] + "\t" + msg.Magnetometer[1] + "\t" + msg.Magnetometer[2] + "\t" +
                    //                                msg.Quaternion[0] + "\t" + msg.Quaternion[1] + "\t" + msg.Quaternion[2] + "\t" + msg.Quaternion[3]);
                    //        streamWriter_.Flush();
                    //    }
                    //    streamWriter_.Close();
                    //}

                    /////////////////////////////////////////////////////////////////////////////////
                }
            }
            else if (acqLog_ && !singlePacket_)
            {
                // LOG MODE - Versione OK - old style!
                if (sp_.BytesToRead >= 516)
                {
                    // Read message
                    byte[] buffer = new byte[516];
                    sp_.Read(buffer, 0, 516);

                    // Check transmission pattern
                    if (buffer[0] == 'P' && buffer[1] == 'K')
                    {
                        // Stop timer
                        spTimer_.Stop();

                        uint parity = 0;
                        int mode = buffer[2];   // Log mode
                        int freq = buffer[3];   // Log frequency

                        // Extract data based on log mode
                        if (mode > 0 && mode < 5)
                        {
                            // Timestamp
                            int epoch = _03b1MuseUtilities.DecodeIntData(buffer[4], buffer[5], buffer[6], buffer[7]);

                            _03b1MuseMessage tempData = new _03b1MuseMessage();
                            int dataIndex = 0;
                            int[] temp = new int[3];
                            float[] tempF = new float[3];
                            for (int j = 0; j < 512; j++)
                            {
                                // Accelerometer, Gyroscope, Magnetometer, Quaternion
                                parity += buffer[j];
                                if (j > 7 && j % 2 == 0)
                                {
                                    temp[dataIndex % 3] = _03b1MuseUtilities.DecodeShortData(buffer[j], buffer[j + 1]);
                                    tempF[dataIndex % 3] = ((float)_03b1MuseUtilities.DecodeShortData(buffer[j], buffer[j + 1]));
                                    dataIndex++;

                                    if (mode == 1 && dataIndex == 3)
                                    {
                                        float[] tempQ = new float[4];
                                        float tempN = 0;
                                        for (int k = 0; k < 3; k++)
                                        {
                                            tempQ[k + 1] = ((float)temp[k]) / 32767;
                                            tempN += (tempQ[k + 1] * tempQ[k + 1]);
                                        }
                                        tempQ[0] = (float)Math.Sqrt(1 - (tempN));
                                        tempN = 0;
                                        for (int k = 0; k < 4; k++)
                                            tempN += (tempQ[k] * tempQ[k]);
                                        for (int k = 0; k < 4; k++)
                                            tempQ[k] /= tempN;
                                        tempData.Quaternion = tempQ;
                                    }
                                    else
                                    {
                                        if (dataIndex == 3) // Accelerometer
                                            tempData.Accelerometer = tempF;
                                        else
                                        {
                                            if (dataIndex == 6) // Gyroscope
                                                tempData.Gyroscope = tempF;
                                            else
                                            {
                                                if (dataIndex == 9) // Magnetometer
                                                    tempData.Magnetometer = temp;
                                                else
                                                {
                                                    if (dataIndex == 12)
                                                    {
                                                        float[] tempQ = new float[4];
                                                        float tempN = 0;
                                                        for (int k = 0; k < 3; k++)
                                                        {
                                                            tempQ[k + 1] = ((float)temp[k]) / 32767;
                                                            tempN += (tempQ[k + 1] * tempQ[k + 1]);
                                                        }
                                                        tempQ[0] = (float)Math.Sqrt(1 - (tempN));
                                                        tempN = 0;
                                                        for (int k = 0; k < 4; k++)
                                                            tempN += (tempQ[k] * tempQ[k]);
                                                        for (int k = 0; k < 4; k++)
                                                            tempQ[k] /= tempN;
                                                        tempData.Quaternion = tempQ;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    if (((((j - 7) + 1) % (mode * 6) == 0) && mode != 5)) // || ((((j - 7) + 1) % (3 * 6) == 0) && mode == 5))
                                    {
                                        ////////////////////////////////////////////////////////////////////////
                                        // TO BE MODIFIED IN ORDER TO REMOVE THEM FROM _03b1Message object!!!
                                        tempData.LogFrequency = freq;
                                        tempData.LogMode = mode;
                                        ////////////////////////////////////////////////////////////////////////
                                        tempData.TimestampTX = epoch;

                                        // Store data
                                        dataIndex = 0;
                                        spDevMsgList_.Add(tempData);
                                        tempData = new _03b1MuseMessage();
                                    }
                                }
                            }
                        }
                        else if (mode == 5)
                        {
                            int epoch = _03b1MuseUtilities.DecodeIntData(buffer[4], buffer[5], buffer[6], buffer[7]);
                            _03b1MuseMessage tempData = new _03b1MuseMessage();

                            int dataIndex = 0;
                            byte[] msgData = new byte[36];
                            for (int j = 0; j < 512; j++)
                            {
                                parity += buffer[j];

                                if (j > 7)
                                {
                                    msgData[dataIndex % 36] = buffer[j];
                                    dataIndex++;

                                    if (dataIndex % 36 == 0)
                                    {
                                        // Accelerometer
                                        float[] tempAcc = new float[3];
                                        tempAcc[0] = _03b1MuseUtilities.DecodeFloatData(msgData[0], msgData[1], msgData[2], msgData[3]);
                                        tempAcc[1] = _03b1MuseUtilities.DecodeFloatData(msgData[4], msgData[5], msgData[6], msgData[7]);
                                        tempAcc[2] = _03b1MuseUtilities.DecodeFloatData(msgData[8], msgData[9], msgData[10], msgData[11]);
                                        tempData.Accelerometer = tempAcc;

                                        // Gyroscope
                                        float[] tempGyro = new float[3];
                                        tempGyro[0] = _03b1MuseUtilities.DecodeFloatData(msgData[12 + 0], msgData[12 + 1], msgData[12 + 2], msgData[12 + 3]);
                                        tempGyro[1] = _03b1MuseUtilities.DecodeFloatData(msgData[12 + 4], msgData[12 + 5], msgData[12 + 6], msgData[12 + 7]);
                                        tempGyro[2] = _03b1MuseUtilities.DecodeFloatData(msgData[12 + 8], msgData[12 + 9], msgData[12 + 10], msgData[12 + 11]);
                                        tempData.Gyroscope = tempGyro;

                                        // Magnetometer
                                        int[] tempMagn = new int[3];
                                        tempMagn[0] = _03b1MuseUtilities.DecodeShortData(msgData[24 + 0], msgData[24 + 1]);
                                        tempMagn[1] = _03b1MuseUtilities.DecodeShortData(msgData[24 + 2], msgData[24 + 3]);
                                        tempMagn[2] = _03b1MuseUtilities.DecodeShortData(msgData[24 + 4], msgData[24 + 5]);
                                        tempData.Magnetometer = tempMagn;

                                        // Quaternion
                                        int[] tempF = new int[3];
                                        tempF[0] = _03b1MuseUtilities.DecodeShortData(msgData[30 + 0], msgData[30 + 1]);
                                        tempF[1] = _03b1MuseUtilities.DecodeShortData(msgData[30 + 2], msgData[30 + 3]);
                                        tempF[2] = _03b1MuseUtilities.DecodeShortData(msgData[30 + 4], msgData[30 + 5]);

                                        float[] tempQ = new float[4];
                                        float tempN = 0;
                                        for (int k = 0; k < 3; k++)
                                        {
                                            tempQ[k + 1] = ((float)tempF[k]) / 32767;
                                            tempN += (tempQ[k + 1] * tempQ[k + 1]);
                                        }
                                        tempQ[0] = (float)Math.Sqrt(1 - (tempN));
                                        tempN = 0;
                                        for (int k = 0; k < 4; k++)
                                            tempN += (tempQ[k] * tempQ[k]);
                                        for (int k = 0; k < 4; k++)
                                            tempQ[k] /= tempN;
                                        tempData.Quaternion = tempQ;

                                        ////////////////////////////////////////////////////////////////////////
                                        // TO BE MODIFIED IN ORDER TO REMOVE THEM FROM _03b1Message object!!!
                                        tempData.LogFrequency = freq;
                                        tempData.LogMode = mode;
                                        ////////////////////////////////////////////////////////////////////////
                                        tempData.TimestampTX = epoch;

                                        // Store data
                                        dataIndex = 0;
                                        spDevMsgList_.Add(tempData);
                                        tempData = new _03b1MuseMessage();
                                    }
                                }
                            }
                        }
                        else if (mode == 6)
                        {
                            // Log with high - resolution timestamp and data for time - sync application
    
                            // int epoch = _03b1MuseUtilities.DecodeIntData(buffer[4], buffer[5], buffer[6], buffer[7]);
                            _03b1MuseMessage tempData = new _03b1MuseMessage();

                            int dataIndex = 0;
                            byte[] msgData = new byte[42];
                            for (int j = 0; j < 512; j++)
                            {
                                parity += buffer[j];

                                if (j > 7)
                                {
                                    msgData[dataIndex % 42] = buffer[j];
                                    dataIndex++;

                                    if (dataIndex % 42 == 0)
                                    {
                                        // Timestamp
                                        UInt64 currentTimestamp = BitConverter.ToUInt64(new byte[8] { msgData[5], msgData[4], msgData[3], msgData[2], msgData[1], msgData[0], 0, 0 }, 0);

                                        // Gyroscope
                                        float[] tempGyro = new float[3];
                                        tempGyro[0] = _03b1MuseUtilities.DecodeFloatData(msgData[6 + 0], msgData[6 + 1], msgData[6 + 2], msgData[6 + 3]);
                                        tempGyro[1] = _03b1MuseUtilities.DecodeFloatData(msgData[6 + 4], msgData[6 + 5], msgData[6 + 6], msgData[6 + 7]);
                                        tempGyro[2] = _03b1MuseUtilities.DecodeFloatData(msgData[6 + 8], msgData[6 + 9], msgData[6 + 10], msgData[6 + 11]);
                                        tempData.Gyroscope = tempGyro;

                                        // Accelerometer
                                        float[] tempAcc = new float[3];
                                        tempAcc[0] = _03b1MuseUtilities.DecodeFloatData(msgData[6 + 12 + 0], msgData[6 + 12 + 1], msgData[6 + 12 + 2], msgData[6 + 12 + 3]);
                                        tempAcc[1] = _03b1MuseUtilities.DecodeFloatData(msgData[6 + 12 + 4], msgData[6 + 12 + 5], msgData[6 + 12 + 6], msgData[6 + 12 + 7]);
                                        tempAcc[2] = _03b1MuseUtilities.DecodeFloatData(msgData[6 + 12 + 8], msgData[6 + 12 + 9], msgData[6 + 12 + 10], msgData[6 + 12 + 11]);
                                        tempData.Accelerometer = tempAcc;

                                        // Magnetometer
                                        int[] tempMagn = new int[3];
                                        tempMagn[0] = _03b1MuseUtilities.DecodeShortData(msgData[6 + 12 + 12 + 0], msgData[6 + 12 + 12 + 1]);
                                        tempMagn[1] = _03b1MuseUtilities.DecodeShortData(msgData[6 + 12 + 12 + 2], msgData[6 + 12 + 12 + 3]);
                                        tempMagn[2] = _03b1MuseUtilities.DecodeShortData(msgData[6 + 12 + 12 + 4], msgData[6 + 12 + 12 + 5]);
                                        tempData.Magnetometer = tempMagn;

                                        // Quaternion
                                        int[] tempF = new int[3];
                                        tempF[0] = _03b1MuseUtilities.DecodeShortData(msgData[6 + 12 + 12 + 6 + 0], msgData[6 + 12 + 12 + 6 + 1]);
                                        tempF[1] = _03b1MuseUtilities.DecodeShortData(msgData[6 + 12 + 12 + 6 + 2], msgData[6 + 12 + 12 + 6 + 3]);
                                        tempF[2] = _03b1MuseUtilities.DecodeShortData(msgData[6 + 12 + 12 + 6 + 4], msgData[6 + 12 + 12 + 6 + 5]);

                                        float[] tempQ = new float[4];
                                        float tempN = 0;
                                        for (int k = 0; k < 3; k++)
                                        {
                                            tempQ[k + 1] = ((float)tempF[k]) / 32767;
                                            tempN += (tempQ[k + 1] * tempQ[k + 1]);
                                        }
                                        tempQ[0] = (float)Math.Sqrt(1 - (tempN));
                                        tempN = 0;
                                        for (int k = 0; k < 4; k++)
                                            tempN += (tempQ[k] * tempQ[k]);
                                        for (int k = 0; k < 4; k++)
                                            tempQ[k] /= tempN;
                                        tempData.Quaternion = tempQ;

                                        ////////////////////////////////////////////////////////////////////////
                                        // TO BE MODIFIED IN ORDER TO REMOVE THEM FROM _03b1Message object!!!
                                        tempData.LogFrequency = freq;
                                        tempData.LogMode = mode;
                                        ////////////////////////////////////////////////////////////////////////
                                        tempData.TimestampTX = currentTimestamp;

                                        // Store data
                                        dataIndex = 0;
                                        spDevMsgList_.Add(tempData);
                                        tempData = new _03b1MuseMessage();
                                    }
                                }
                            }
                        }

                        acquiredPackets_ += 512;
                        lastReceivedPacket_++;

                        if (lastReceivedPacket_ % 20 == 0 || acquiredPackets_ >= numOfLogPackets_)
                        {
                            // Transmit acknowledge
                            PacketOK();

                            Console.WriteLine("spDevMsgList_ length: " + spDevMsgList_.Count);

                            lastReceivedPacket_ = 0;
                            lastMsgIdx_ = MessageList.Count - 1;
                        }
                        // Deal with a new packet
                        // if (NewLogSample != null)
                        //     NewLogSample(this, (int)((acquiredPackets_ * 100) / numOfLogPackets_));

                        //Start timer
                        spTimer_.Start();
                    }
                }

                if (acquiredPackets_ >= numOfLogPackets_)
                {
                    // spTXstatus_ = false;
                    acqLog_ = false;
                    numOfLogPackets_ = 0;
                    acquiredPackets_ = 0;

                    // Stop timer
                    spTimer_.Stop();
                }
            }
            else
            {
                // STREAM mode
                GetStreamSample();
            }
        }

        /// <summary> Reads and decode stream type sample. </summary>
        private bool GetStreamSample()
        {
            byte[] buffer = new byte[packetDim_];
            byte[] c = new byte[1];
            char id;
            if (packetDim_ == 54 || packetDim_ == 36)
                id = 't';
            else
                id = 'Q';

            bool firstIDFound = false;
            bool waiting = false;
            bool packetCompleted = false;

            DateTime d = DateTime.Now;

            while (packetCompleted == false && spTXstatus_ == true)
            {
                if ((DateTime.Now - d).TotalMilliseconds > 1000)
                    spTXstatus_ = false;

                if (sp_.BytesToRead > 0)
                {
                    if (!waiting)
                    {
                        sp_.Read(c, 0, 1);
                        if (c[0] == id)
                        {
                            if (!firstIDFound)
                            {
                                // This is the firs ID read, waiting for the second
                                firstIDFound = true;
                                buffer[0] = c[0];
                            }
                            else
                            {
                                // I have already read an ID, waiting for the next bytes
                                buffer[1] = c[0];
                                waiting = true;
                            }
                        }
                        else
                            firstIDFound = false;
                    }
                    else if (sp_.BytesToRead >= packetDim_ - 2)
                    {
                        // I have all message data, put it into the buffer
                        byte[] data = new byte[packetDim_ - 2];
                        sp_.Read(data, 0, data.Length);
                        for (int i = 0; i < data.Length; i++)
                            buffer[i + 2] = data[i];

                        packetCompleted = true;
                    }
                }
            }

            if (spTXstatus_ == true)
            {
                // Read data waiting in the buffer
                _03b1MuseMessage result = null;

                switch (packetDim_)
                {
                    case 54:
                        result = ReadRawAndQuaternionData(buffer);
                        break;
                    case 36:
                        result = ReadHDRData(buffer);
                        break;
                    case 18:
                        result = ReadQuaternion(buffer);
                        break;
                    default:
                        result = ReadRawAndQuaternionData(buffer);
                        break;
                }

                if (result != null)
                {
                    this.spDevMsg_ = result;

                    if (firstPacket_)
                        firstPacket_ = false;

                    switch (packetDim_)
                    {
                        case 54:
                            Console.WriteLine(result.TimestampTX + "\t" + 
                                              string.Join("\t", result.Gyroscope) + "\t" +
                                              string.Join("\t", result.Accelerometer) + "\t" +
                                              string.Join("\t", result.Magnetometer) + "\t" + 
                                              string.Join("\t", result.Quaternion));

                            streamWriter_.WriteLine(result.TimestampTX + "\t" + 
                                                    result.Gyroscope[0] + "\t" + result.Gyroscope[1] + "\t" + result.Gyroscope[2] + "\t" +
                                                    result.Accelerometer[0] + "\t" + result.Accelerometer[1] + "\t" + result.Accelerometer[2] + "\t" +
                                                    result.Magnetometer[0] + "\t" + result.Magnetometer[1] + "\t" + result.Magnetometer[2] + "\t" +
                                                    result.Quaternion[0] + "\t" + result.Quaternion[1] + "\t" + result.Quaternion[2] + "\t" + result.Quaternion[3]);
                            break;
                        case 36:
                            Console.WriteLine(result.TimestampTX + "\t" + 
                                              string.Join("\t", result.Gyroscope) + "\t" +
                                              string.Join("\t", result.Accelerometer) + "\t" +
                                              string.Join("\t", result.HDRAccelerometer));

                            streamWriter_.WriteLine(result.TimestampTX + "\t" + 
                                                    result.Gyroscope[0] + "\t" + result.Gyroscope[1] + "\t" + result.Gyroscope[2] + "\t" +
                                                    result.Accelerometer[0] + "\t" + result.Accelerometer[1] + "\t" + result.Accelerometer[2] + "\t" +
                                                    result.HDRAccelerometer[0] + "\t" + result.HDRAccelerometer[1] + "\t" + result.HDRAccelerometer[2]);
                            break;
                        case 18:
                            Console.WriteLine(string.Join("\t", result.Quaternion));

                            streamWriter_.WriteLine(result.Quaternion[0] + "\t" + result.Quaternion[1] + "\t" + result.Quaternion[2] + "\t" + result.Quaternion[3]);
                            break;
                        default:
                            Console.WriteLine(result.TimestampTX + "\t" +
                                              string.Join("\t", result.Gyroscope) + "\t" +
                                              string.Join("\t", result.Accelerometer) + "\t" +
                                              string.Join("\t", result.Magnetometer) + "\t" +
                                              string.Join("\t", result.Quaternion));

                            streamWriter_.WriteLine(result.TimestampTX + "\t" + 
                                                    result.Gyroscope[0] + "\t" + result.Gyroscope[1] + "\t" + result.Gyroscope[2] + "\t" +
                                                    result.Accelerometer[0] + "\t" + result.Accelerometer[1] + "\t" + result.Accelerometer[2] + "\t" +
                                                    result.Magnetometer[0] + "\t" + result.Magnetometer[1] + "\t" + result.Magnetometer[2] + "\t" +
                                                    result.Quaternion[0] + "\t" + result.Quaternion[1] + "\t" + result.Quaternion[2] + "\t" + result.Quaternion[3]);
                            break;
                    }
                    streamWriter_.Flush();
                }
            }

            return spTXstatus_;
        }

        /// <summary> Reads raw data message. </summary>
        private _03b1MuseMessage ReadRawData(byte[] buffer)
        {
            _03b1MuseMessage result = null;

            // Check transmission pattern
            if (buffer[0] == 't' && buffer[1] == 't' && buffer[6] == 'G' && buffer[7] == 'G' && buffer[14] == 'M' && buffer[15] == 'M' && buffer[22] == 'A' && buffer[23] == 'A' && buffer[36] == 'Q' && buffer[37] == 'Q')
            {
                result = new _03b1MuseMessage();

                if (SyncModeEnabled == true)
                {
                    // Decoding timestamp with time sync offset
                    result.TimestampTX = (int)(_03b1MuseUtilities.DecodeIntData(buffer[2], buffer[3], buffer[4], buffer[5])) + (int)(tOffset_[0] * 1000);
                    if (firstPacket_)
                        result.TimestampRX = 0;
                    else
                        result.TimestampRX = (DateTime.Now - previousTime_).Milliseconds + (DateTime.Now - previousTime_).Seconds * 1000;
                    previousTime_ = DateTime.Now;
                }
                else
                {
                    // Decoding timestamp without time sync offset
                    result.TimestampTX = (int)(_03b1MuseUtilities.DecodeIntData(buffer[2], buffer[3], buffer[4], buffer[5]));
                    if (firstPacket_)
                        result.TimestampRX = 0;
                    else
                        result.TimestampRX = (DateTime.Now - previousTime_).Milliseconds + (DateTime.Now - previousTime_).Seconds * 1000;
                    previousTime_ = DateTime.Now;
                }
            
                // Decoding Gyroscope, Accelerometer and Magnetometer data
                for (int i = 0; i < 3; i++)
                {
                    result.Gyroscope[i] = ((float)_03b1MuseUtilities.DecodeShortData(buffer[i * 2 + 8], buffer[i * 2 + 9]));
                    result.Magnetometer[i] = _03b1MuseUtilities.DecodeShortData(buffer[i * 2 + 16], buffer[i * 2 + 17]);
                    result.Accelerometer[i] = _03b1MuseUtilities.DecodeShortData(buffer[i * 2 + 24], buffer[i * 2 + 25]);
                }

                // Decoding message ID
                result.ID = _03b1MuseUtilities.DecodeIntData(buffer[32], buffer[33], buffer[34], buffer[35]);

                // Decoding Quaternion
                for (int i = 0; i < 4; i++)
                    result.Quaternion[i] = _03b1MuseUtilities.DecodeFloatData(buffer[i * 4 + 38], buffer[i * 4 + 39], buffer[i * 4 + 40], buffer[i * 4 + 41]);
            }
            return result;
        }

        /// <summary> Reads raw and quaternion data message. </summary>

        private _03b1MuseMessage ReadRawAndQuaternionData(byte[] buffer)
        {
            _03b1MuseMessage result = null;

            // Check transmission pattern
            if (buffer[0] == 't' && buffer[1] == 't' && buffer[6] == 'G' && buffer[7] == 'G' && buffer[14] == 'M' && buffer[15] == 'M' && buffer[22] == 'A' && buffer[23] == 'A' && buffer[36] == 'Q' && buffer[37] == 'Q')
            {
                result = new _03b1MuseMessage();

                if (SyncModeEnabled == true)
                {
                    // Decoding timestamp with time sync offset
                    result.TimestampTX = _03b1MuseUtilities.DecodeIntData(buffer[2], buffer[3], buffer[4], buffer[5]) + (tOffset_[0] * 1000);
                    if (firstPacket_)
                        result.TimestampRX = 0;
                    else
                        result.TimestampRX = (DateTime.Now - previousTime_).Milliseconds + (DateTime.Now - previousTime_).Seconds * 1000;
                    previousTime_ = DateTime.Now;
                }
                else
                {
                    // Decoding timestamp without time sync offset
                    result.TimestampTX = _03b1MuseUtilities.DecodeIntData(buffer[2], buffer[3], buffer[4], buffer[5]);
                    if (firstPacket_)
                        result.TimestampRX = 0;
                    else
                        result.TimestampRX = (DateTime.Now - previousTime_).Milliseconds + (DateTime.Now - previousTime_).Seconds * 1000;
                    previousTime_ = DateTime.Now;
                }

                // Decoding Gyroscope, Accelerometer and Magnetometer data
                for (int i = 0; i < 3; i++)
                {
                    result.Gyroscope[i] = ((float)_03b1MuseUtilities.DecodeShortData(buffer[i * 2 + 8], buffer[i * 2 + 9]));
                    result.Magnetometer[i] = _03b1MuseUtilities.DecodeShortData(buffer[i * 2 + 16], buffer[i * 2 + 17]);
                    result.Accelerometer[i] = _03b1MuseUtilities.DecodeShortData(buffer[i * 2 + 24], buffer[i * 2 + 25]);
                }
                
                // Decoding message ID
                result.ID = _03b1MuseUtilities.DecodeIntData(buffer[32], buffer[33], buffer[34], buffer[35]);

                // Decoding Quaternion
                for (int i = 0; i < 4; i++)
                    result.Quaternion[i] = _03b1MuseUtilities.DecodeFloatData(buffer[i * 4 + 38], buffer[i * 4 + 39], buffer[i * 4 + 40], buffer[i * 4 + 41]);
            }
            return result;
            
        }

        /// <summary> Reads HDR data message. </summary>
        private _03b1MuseMessage ReadHDRData(byte[] buffer)
        {
            _03b1MuseMessage result = null;

            // Check transmission pattern
            if (buffer[0] == 't' && buffer[1] == 't' && buffer[6] == 'G' && buffer[7] == 'G' && buffer[14] == 'H' && buffer[15] == 'H' && buffer[28] == 'A' && buffer[29] == 'A')
            {
                result = new _03b1MuseMessage();

                if (SyncModeEnabled == true)
                {
                    // Decoding timestamp with time sync offset
                    result.TimestampTX = (_03b1MuseUtilities.DecodeIntData(buffer[2], buffer[3], buffer[4], buffer[5]) / 10) + (tOffset_[0] * 1000);
                    if (firstPacket_)
                        result.TimestampRX = 0;
                    else
                        result.TimestampRX = (DateTime.Now - previousTime_).Milliseconds + (DateTime.Now - previousTime_).Seconds * 1000;
                    previousTime_ = DateTime.Now;
                }
                else
                {
                    // Decoding timestamp without time sync offset
                    result.TimestampTX = (_03b1MuseUtilities.DecodeIntData(buffer[2], buffer[3], buffer[4], buffer[5]) / 10);
                    if (firstPacket_)
                        result.TimestampRX = 0;
                    else
                        result.TimestampRX = (DateTime.Now - previousTime_).Milliseconds + (DateTime.Now - previousTime_).Seconds * 1000;
                    previousTime_ = DateTime.Now;
                }

                // Decoding Gyroscope, HDR Accelerometer and Accelerometer data
                for (int i = 0; i < 3; i++)
                {
                    result.Gyroscope[i] = ((float)_03b1MuseUtilities.DecodeShortData(buffer[i * 2 + 8], buffer[i * 2 + 9]));
                    result.HDRAccelerometer[i] = _03b1MuseUtilities.DecodeIntData(buffer[i * 4 + 16], buffer[i * 4 + 17], buffer[i * 4 + 18], buffer[i * 4 + 19]);
                    result.Accelerometer[i] = _03b1MuseUtilities.DecodeShortData(buffer[i * 2 + 30], buffer[i * 2 + 31]);
                }
            }
            return result;
        }

        /// <summary> Reads quaternion data message. </summary>
        private _03b1MuseMessage ReadQuaternion(byte[] buffer)
        {
            _03b1MuseMessage result = null;

            // Check transmission pattern
            if (buffer[0] == 'Q' && buffer[1] == 'Q')
            {
                result = new _03b1MuseMessage();
                for (int i = 0; i < 4; i++)
                    result.Quaternion[i] = _03b1MuseUtilities.DecodeFloatData(buffer[i * 4 + 2], buffer[i * 4 + 3], buffer[i * 4 + 4], buffer[i * 4 + 5]);
            }
            return result;
        }

        #endregion
    }
}
